import maya.cmds as mc
import maya.mel as mel
import maya.app.renderSetup.model.override as override
import maya.app.renderSetup.model.selector as selector
import maya.app.renderSetup.model.collection as collection
import maya.app.renderSetup.model.renderLayer as renderLayer
import maya.app.renderSetup.model.renderSetup as renderSetup
import json


camAA = 3
diffuse = 3
glossy = 1
refraction = 1
sss = 1
volumeInd = 1
lockSampling = True
transpDepth = 1
transpThres = .990
bucketSize = 135
maxThreads = True

SkinRef = "Human_Anatomy_Rig:Skin_Bind_grp_01locator"
SkelRef = "Human_Anatomy_Rig:Humanoid_Innerworkings_01"
visibilityCtrl = "Human_Anatomy_Rig:Visible_Meshes"


def aiVisible():
    meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True)

    for i in meshes:  
        gloss = i + '.aiVisibleInGlossy'
        diff = i + '.aiVisibleInDiffuse'
        if mc.objExists(gloss):
            mc.setAttr(diff, 0)
            mc.setAttr(gloss, 0)


def ApplyRenderSettings():
    mc.setAttr("defaultArnoldRenderOptions.threads_autodetect", maxThreads)
    mc.setAttr("defaultArnoldRenderOptions.AASamples", camAA)
    mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", diffuse)
    mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", glossy)
    mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", refraction)
    mc.setAttr("defaultArnoldRenderOptions.GISssSamples", sss)
    mc.setAttr("defaultArnoldRenderOptions.GIVolumeSamples", volumeInd)
    mc.setAttr("defaultArnoldRenderOptions.lock_sampling_noise", lockSampling)
    mc.setAttr("defaultArnoldRenderOptions.bucketSize", bucketSize)
    
    mc.setAttr("defaultArnoldRenderOptions.autoTransparencyDepth", transpDepth)
    mc.setAttr ("defaultArnoldRenderOptions.autoTransparencyThreshold", transpThres)
    
    mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", diffuse)
    mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", diffuse)
    mc.setAttr("defaultArnoldRenderOptions.GIGlossyDepth", glossy)
    mc.setAttr("defaultArnoldRenderOptions.GIReflectionDepth", refraction)
    mc.setAttr("defaultArnoldRenderOptions.GIRefractionDepth", refraction)
    mc.setAttr("defaultArnoldRenderOptions.GIVolumeDepth", volumeInd)
    
def CreateDefaultRenderLayers(layerName, collectionName):
    rs = renderSetup.instance()    
    rl = rs.createRenderLayer(layerName)
    c = rl.createCollection(collectionName)

    if layerName == 'Skin_RL': 
        c.getSelector().staticSelection.set([SkinRef, 'Lights:Lights'])
        AddOverrides(c, "Skin", 1)
        AddOverrides(c, "Muscles", 0)
        AddOverrides(c, "Skeleton", 0)
        AddOverrides(c, "Controls", 0)
       
    if layerName == 'Skel_RL': 
        c.getSelector().staticSelection.set([SkelRef, 'Lights:Lights'])
        AddOverrides(c, "Skin", 0)
        AddOverrides(c, "Muscles", 0)
        AddOverrides(c, "Skeleton", 1)
        AddOverrides(c, "Controls", 0)
        AddOverrides(c, "ArticularDisk", 1)

def AddOverrides(ref, AttrName, value):
    oOverride = ref.createOverride(AttrName, override.AbsOverride.kTypeId)
    plug = visibilityCtrl + "." + AttrName
    oOverride.setAttributeName(plug)
    oOverride.finalize(plug)
    oOverride.setAttrValue(value)

def SmoothAllVisibleMeshes():
    meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True) 
    for i in meshes:  
        smooth = i + ".displaySmoothMesh"
        if mc.objExists(smooth):
            mc.setAttr(smooth, 2)
    
aiVisible()
ApplyRenderSettings()
CreateDefaultRenderLayers('Skin_RL', 'skinCollection')
CreateDefaultRenderLayers('Skel_RL', 'skelCollection')
SmoothAllVisibleMeshes()