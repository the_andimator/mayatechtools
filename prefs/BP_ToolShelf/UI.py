###___Component Imports____####
import BP_ToolShelf.ArnoldOpacity as ArnoldOp
import BP_ToolShelf.ApplyRenderSettings as ApplyRendSet
import BP_ToolShelf.ArnoldTransmission as ArnoldTrans
import BP_ToolShelf.EditQuickSelectSets as EditQuickSel
import BP_ToolShelf.EditRenderLayers as EditRendLayer
import BP_ToolShelf.LightLinkMaterials as LightLinkMat
import BP_ToolShelf.RemoveUnusedMaterials as RemoveMat
import BP_ToolShelf.SmoothSceneMeshes as SmoothScene

import BP_ToolShelf as BP
import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
from functools import partial
import maya.app.renderSetup.model.override as override
import maya.app.renderSetup.model.collection as collection
import maya.app.renderSetup.model.renderLayer as renderLayer
import maya.app.renderSetup.model.renderSetup as renderSetup
import json
import os.path as path
import sys


class BPToolUI(object):

    global prepWin
    global toolsetVer
    global renderLayerWin
    renderLayerWin = ''
        
    toolsetVer = "01.00" 
    
    import maya.cmds as mc
    
    def __init__(self):
        try:
            mc.deleteUI(dockable, window = True)
            mc.deleteUI(prepWin, window = True)
        except:
            pass
                
        self.windowName = windowName = "BP_Pipeline_Tools"
        self.dockable = dockable = "BP_Tools_Window"
        
        #Class input variables
        visDif = False
        visSpec = False
        visDiffTrans = False
        visSpecTrans = False
        visOpaque = False
        
        
        CONST_METAL_GOLD = 'MAT_Inlay_Gold_01SG1'
        CONST_METAL_SILVER = 'MAT_Inlay_Silver_01SG'
        CONST_METAL_PORCELAIN = 'MAT_Inlay_Porcelain_01SG'
        CONST_HDRI_MAP = "HDRI_Refl_LightShape"
        
        self.visDif = visDif
        self.visSpec = visSpec
        self.visSpecTrans = visSpecTrans
        self.visDiffTrans = visDiffTrans
        self.visOpaque = visOpaque
        
        self.CONST_METAL_GOLD = CONST_METAL_GOLD
        self.CONST_METAL_SILVER = CONST_METAL_SILVER
        self.CONST_METAL_PORCELAIN = CONST_METAL_PORCELAIN
        self.CONST_HDRI_MAP = CONST_HDRI_MAP
        
        if mc.window(self.windowName, exists = True):
            mc.deleteUI(self.windowName)
            
        if mc.window(self.dockable, exists = True):
            mc.deleteUI(self.dockable)
            
##############################################################################################
#################################___________UI___________#####################################  
##############################################################################################

    def BuildUI(self, windowName):
        
        try:
             mc.deleteUI(self.windowName, window = True)
        except:
            pass
        
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "BP_ToolShelf"
        
        try: 
            mc.deleteUI(WinName)
        except:
            pass
            
        prepWin = mc.window(self.windowName, w =250, h = 300, mnb = True, mxb = True, s = 1, rtf = True)
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 250, h= 350)

        #Create menu items
        menuBarLayout = mc.menuBarLayout()
        mc.menu( label='File' )
        mc.menuItem( label='Stuff will go here eventually..maybe loading the render sequencer or batch file' )

        mc.menu( label='Version', helpMenu=True )
        mc.menuItem( label='Ver:  ' + toolsetVer )
        ###Create Border and Frames####
        mc.separator( style='in', width=605)
        mc.frameLayout(toolsetVer, parent = prepWin, label = "", cll = False, bv = False)
        headerRowColumn = mc.rowColumnLayout("header", nc = 1, cw = [(1,605)], columnOffset = [(1, "both", 2), (2, "both", 2)])
        #Find Header Image
        headerPath = mc.internalVar(userBitmapsDir=True)
        mc.image( image=headerPath + 'icon_bpLogo.png')
  
        
        ########################_____Add Tools UI_____########################
        ###__Arnold Visibility___###
        mc.frameLayout('Arnold Visible in Diffuse and Glossy', parent = prepWin, label = "Arnold Diffuse and Reflective Transmission", cll = True, bv = False)
        AivisTop = mc.rowColumnLayout("aiVistop",  nc = 5, cw = [(1,175), (2,90), (3,80), (4,130), (5,130)])
        name = mc.text("Set Arnold Diff/Gloss Visibility", align = 'left')
        self.visDif = mc.checkBox("visDif", label= 'Diffuse Refl') 
        self.visSpec = mc.checkBox("visSpec", label= 'Spec Refl')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        self.visDiffTrans = mc.checkBox("visDifTrans", label= 'Diffuse Trans')
        self.visSpecTrans = mc.checkBox("visSpecrans", label= 'Spec Trans')
        aiApplySel = mc.button(label = "Apply to Selected", align = "center", c =  lambda *_: self.SetArnoldAttributes(False))
        aiApplyAll = mc.button(label = "Apply to Scene", align = "center",  c =  lambda *_: self.SetArnoldAttributes(True))
        mc.setParent("..")
        
        
        ###__Smooth Scene Meshes___###
        mc.frameLayout('Smooth Scene Meshes', parent = prepWin, label = "Smooth Scene Meshes", cll = True, bv = False)
        SmoothSceneTop = mc.rowColumnLayout("aiVistop", nc = 3, cw = [(1,230), (2,75), (3,300)])
        name = mc.text("Smooth All Polygonal Meshes in scene", align = 'left')
        mc.text(label = "", align = 'left')
        aiSmoothMesh = mc.button(label = "Smooth All Scene Meshes", align = "center", c = lambda *_: self.SmoothMeshes(True))
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        aiUnSmoothMesh = mc.button(label = "De-Smooth All Scene Meshes", align = "center", c = lambda *_: self.SmoothMeshes(False))
        mc.setParent("..")


        ###__Set Arnold Opacity___###
        mc.frameLayout('Set Arnold Opacity',parent = prepWin, label = "Set Arnold Opacity", cll = True, bv = False)
        OpacVis = mc.rowColumnLayout("aiVistop", nc = 6, cw = [(1,175), (2,80), (3,50), (4,300)])
        name = mc.text("Set Arnold Dif/Ref Transmission", align = 'left')
        self.visOpaque = mc.checkBox("visOpaque", label= 'Opaque', align = 'left') 
        mc.text(label = "", align = 'left')
        
        aiApplyAll = mc.button(label = "Apply to Selected", align = "center", c = lambda *args: self.SetArnoldOpacity())
        mc.setParent("..")
        
        
        ####Apply Values to variables
        visOpaque = mc.checkBox(self.visOpaque, q=True, v=True)


        ###__Create and Add objects to render layers___###
        mc.frameLayout('Create Render Layer and Add Selected', parent = prepWin,label = "Create Render Layer and Add Selected", cll = True, bv = False) 
        mc.rowColumnLayout(nc = 2, cw = [(1,275),(2, 302)])
        currentRenderTitle = mc.text(label = "Current Render Layers", align = 'left')
        collectionTitle = mc.text(label = "Collections", align = 'left')
        mc.setParent("..")
        
        
        #create default lists
        mc.rowColumnLayout("addLayer", nc = 3, cw = [(1,250), (2,25), (3,250), (4,5), (5,150), (8,20)])
        sl = pm.iconTextScrollList(allowMultiSelection=True, append = EditRendLayer.EditRenderLayers.FindRenderlayers(self))
        mc.text(label = "", align = 'left')
        slCollections = pm.textScrollList(allowMultiSelection = True, append = self.FindCollections(sl))
        pm.iconTextScrollList(sl, edit = True, allowMultiSelection = True, doubleClickCommand = lambda *args: (self.PrepareWindow(sl)))
        mc.setParent("..")
        #pm.textScrollList(sl, edit = True, allowMultiSelection = True, doubleClickCommand = lambda *args: (self.FindCollections(sl)))
        mc.rowColumnLayout("addLayer2", nc = 2, cw = [(1,225), (2,20), (3,5), (4,5), (5,150), (8,20)])
        renderRenderLayer = mc.button(label = "+ Create a New Render Layer", align = "center", c = lambda *args: self.CreateLayerWindow(sl))
        reficon = headerPath + "icon_refresh.png"
        self.inputRefreshButton = mc.symbolButton(w = 23, h = 23, image = reficon, ebg = True, c = lambda *args: self.UpdateRenderLayers(sl))
        addToRenderRenderLayer = mc.button(label = "Add Selected to Render Layers", align = "center", c = lambda *args: self.AddToRenderLayers(sl) )
        mc.text(label = "", align = 'left')
        removeFromRenderRenderLayer = mc.button(label = "Remove Selected from Render Layers", align = "center", c = lambda *args: self.RemoveFromRenderLayers(sl) )
        mc.text(label = "", align = 'left')
        isolateRL = mc.button(label = "Isolate Single Layer", align = "center", c = lambda *args: self.IsolateRL(sl) )
        mc.setParent("..")
        mc.text(label = "", align = 'left')

        ###__Create Quick Select Sets___###
        CreateQuickSelectSet = mc.frameLayout("Create Quick Select Set", parent = prepWin, label = "Create Quick Select Set", cll = True, bv = False)
        quickSelect = mc.rowColumnLayout("quickSelect", nc =1, cw = [(1,605)])
        mc.setParent("..")
        name = mc.text("Current Quick Select Sets", align = 'left')

        quickSet = mc.rowColumnLayout("quickSet", nc = 1, cw = [(1,605)])
        mc.setParent("..")
        mc.rowColumnLayout("quickSetMenu", nc = 2, cw = [(1,250), (2,20)])
        qss = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindQuickSelect())
        pm.iconTextScrollList(qss, edit = True, doubleClickCommand = lambda *args:(self.SelectQuickSet(qss)))
        mc.setParent("..")
        
        mc.rowColumnLayout("quickSetButton", nc = 2, cw = [(1,225), (2,25)])
        createQuickSet = mc.button(label = "+ Create Quick Select", align = "center", c = lambda *args: self.CreateQuickSelWindow(qss))
        refreshSelect = mc.symbolButton(w = 23, h = 23, image = reficon, ebg = True, c = lambda *args: self.UpdateQuickSelLayers(qss))
        mc.rowColumnLayout("quickSetButton", nc = 1, cw = [(1,225), (2,25)])
        AddSelectedToSet = mc.button(label = "Add Selected to Quick Select", align = "center", c = lambda *args: self.AddToQuickSelectSet(qss))
        RemoveSelectedToSet = mc.button(label = "Remove Selected to Quick Select", align = "center", c = lambda *args: self.RemoveFromQuickSelectSet(qss))
        RenameSelected = mc.button("Rename Selected",label = "Rename Quick Select", align = "center", c = lambda *args: self.RenameQuickSel(qss))
        DeleteSet = mc.button("DeleteSet", label = "Delete Quick Select Set", align = "center", c = lambda *args: self.DeleteQuickSel(qss))
        mc.text(label = "", align = 'left')
        mc.setParent(CreateQuickSelectSet)
        mc.setParent("..")     

        
        ###__Remove Unused Materials___###
        RemoveUnusedMaterials = mc.frameLayout("Remove Unused Materials", parent = prepWin,label = "Remove Unused Materials", cll = True, bv = False)
        mc.rowColumnLayout("caution",  nc = 1, cw = [(1,450)])
        name = mc.text("CAUTION: This might break active shading networks. Save Scene before using.", align = 'left', font = "boldLabelFont")
        mc.setParent(RemoveUnusedMaterials)
        removeMatCol = mc.rowColumnLayout("removeMatCol",  nc = 4, cw = [(1,175), (2,80), (3,50), (4,300)])
        mc.text("Remove Unused Materials", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        
        removeMaterials = mc.button(bgc = [1.0,.2,.2],label = "Remove Materials", align = "center" , c = lambda *args: self.AttemptToRemoveUnusedMat())
        mc.text(label = "", align = 'left')
        mc.setParent(RemoveUnusedMaterials)
       
        ###__Light Link Metal Materials___###
        LightLinkerFrame = mc.frameLayout("LightLinkMetalMaterials", parent = prepWin,label = "Light Link Metal Materials", cll = True, bv = False)
        LightLinkColumn = mc.rowColumnLayout("HDRI_Text",  nc = 2, cw = [(1,100),(2,260)])
        mc.text(label = "HDRI Map Name:", align = 'left', font = "boldLabelFont")
        self.CONST_HDRI_MAP = mc.textField("self.CONST_HDRI_MAP", text = self.CONST_HDRI_MAP)
        mc.setParent("..") 
        
        LightLinkColumn = mc.rowColumnLayout("LightLinkCol",  nc = 1, cw = [(1,605)])

        mc.text("Scene Shader Group List", align = 'left')
        mc.text("Double Click To Rename", align = 'left', font = "smallObliqueLabelFont")
        mc.setParent("..")   
        columnTwo = mc.rowColumnLayout("LightLinkCol2",  nc = 1, cw = [(1,300)])
        llmm = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindMaterialList())
        pm.iconTextScrollList(llmm, edit = True, dcc = lambda *args: self.RenameShaderGroups(llmm))
        columnThree = mc.rowColumnLayout("LightLinkCol3",  nc = 2, cw = [(1,268),(2,32)])
        mc.text(label = "", align = 'left')
        RefreshList = mc.symbolButton(w = 23, h = 25, image = reficon, ebg = True, c = lambda *args: self.GenerateMaterialList(llmm))
        mc.setParent("..") 
        #pm.iconTextScrollList(llmm, edit = True, dcc = lambda *args: self.SelectItemsWithMaterial(llmm))
        
        mc.textField(self.CONST_HDRI_MAP, edit=True)
        
        HDRI_Input = self.CONST_HDRI_MAP
        
        columnFour= mc.rowColumnLayout("LightLinkCol4",  nc = 1, cw = [(1,300)])
        LinkToHDRI = mc.button(label = "Link to HDRI", align = "center" , c = lambda *args: self.CreateHDRILightLinks(llmm, HDRI_Input))
        LinkToDefault = mc.button(label = "Link to Default Lights", align = "center" , c = lambda *args: self.CreateDefaultLightLinks(llmm, HDRI_Input))
        LinkToAll = mc.button(label = "Link to Both", align = "center" , c = lambda *args: self.ConnectAllLightLinks(llmm, HDRI_Input))
        SelectObjectsOf = mc.button(label = "Select Meshes Using Materials", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(llmm))
        mc.setParent(LightLinkerFrame)
        
        
        ###__Renders Settings and Scene Prep___###
        RenderSettingsandPrep = mc.frameLayout("RenderSettingsandPrep", parent = prepWin,label = "Render Settings and Prep", cll = True, bv = False)
        mc.separator( style='in', width=605)
        RenderColumn = mc.rowColumnLayout("RenderColumn",  nc = 2, cw = [(1,150), (2,300)])
        
        mc.text(label = 'Camera(AA)', align = 'left')
        camAA = mc.intSliderGrp("camAA", field=True, value = 3, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'Diffuse', align = 'left')
        diffuse = mc.intSliderGrp("diffuse", field=True,  value = 1, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'Specular', align = 'left')
        specular = mc.intSliderGrp("Specular", field=True,  value = 1, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'Transmission', align = 'left')
        transmission = mc.intSliderGrp("Transmission", field=True,  value = 1, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'SSS', align = 'left')
        SSS = mc.intSliderGrp("sss", field=True,  value = 1, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'VolumeIndirect', align = 'left')
        VolumeIndirect = mc.intSliderGrp("VolumeIndirect", field=True,  value = 1, maxValue=10, fieldMaxValue=25)
        mc.setParent("..") 
        RenderColumn2 = mc.rowColumnLayout("RenderColumn2",  nc = 4, cw = [(1,150), (2,81), (3,150), (4, 81)])
        
        mc.text(label = 'TransparencyDepth', align = 'left')
        TransparencyDepth = mc.intField(value = 1)
        mc.text(label = 'TransparencyThreshold', align = 'center')
        TransparencyThreshold = mc.floatField(value = .990)
        
        mc.text(label = 'BucketSize', align = 'left')
        BucketSize = mc.intField(value = 135)
        mc.setParent("..") 
        
        #######
        RenderColumn3 = mc.rowColumnLayout("RenderColumn3",  nc = 3, cw = [(1,100), (2,100), (3,250)])
        
        lock_sampling = pm.checkBox("LockSampling", label = "Lock Sampling", ed = True, value = True)
        lockSample = pm.checkBox("LockSampling", q = True, v = True)
        threadBox = pm.checkBox("Thread", label = "Max Threads", ed = True, value = True, changeCommand = partial (self.checkbox_toggled))
        thread_slider = pm.intSliderGrp("ThreadSlider",  step = 1, field=True, maxValue = 16, minValue = -16, w = 10, value = 6, enable = False)
        self.threadBox = pm.checkBox("Thread", q = True, value = True)
        self.threadSlider = pm.intSliderGrp("ThreadSlider", q = True, value = True)
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        SelectObjectsOf = mc.button(label = "Apply Settings", align = "center" , c = lambda *args: self.ApplyRenderSettings(camAA, 
                                                                                                                            diffuse, 
                                                                                                                            specular, 
                                                                                                                            transmission, 
                                                                                                                            SSS, 
                                                                                                                            VolumeIndirect, 
                                                                                                                            TransparencyDepth, 
                                                                                                                            TransparencyThreshold, 
                                                                                                                            BucketSize,
                                                                                                                            lockSample))
        mc.separator( style='in', width=605)
        mc.text(label = "", align = 'left')
        
        EndFrame = mc.frameLayout("EndFrame", parent = prepWin,label = "", cll = False, bv = False, bgc = [.25,.25,.25])
        EndFrameColumn = mc.rowColumnLayout("EndFrame",  nc = 1, cw = [(1,605)])
                
        if mc.window(WinName, exists = True):
            mc.deleteUI(WinName)
        dockable = mc.dockControl(WinName, area = 'left', content=prepWin, allowedArea='all', mov = True, s = True )
        mc.showWindow()   
    
    def checkbox_toggled(self, *args):
        slider_enabled = pm.checkBox("Thread", q = True, value = True)
        print slider_enabled
        if slider_enabled:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = False)
        else:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = True)