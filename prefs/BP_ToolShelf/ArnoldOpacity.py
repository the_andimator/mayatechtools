class ArnoldOpacity(object):
    
    def SetArnoldOpacity(self):     
        opaque = mc.checkBox(self.visOpaque, q= True, v= True, changeCommand = True)
        selected = mc.ls(sl= True) 
        if selected != []:
            for i in selected:
                obj = i + ".aiOpaque"
                if mc.objExists(obj):
                    mc.setAttr(obj, opaque)
        else:
            mc.warning("No meshes were found in scene. Unable to apply .aiOpaque setting.")