import maya.cmds as mc
import pymel.core as pm

def setRGBColor(ctrl, color):
    
    rgb = ("R","G","B")
    
    cmds.setAttr(ctrl + ".overrideEnabled",1)
    cmds.setAttr(ctrl + ".overrideRGBColors",1)
    
    for channel, color in zip(rgb, color):
        
        cmds.setAttr(ctrl + ".overrideColor%s" %channel, color)
        
selList = mc.ls(selection = True, transforms = True)
color = (0,0,1)

for i in selList:
    setRGBColor(i, color)