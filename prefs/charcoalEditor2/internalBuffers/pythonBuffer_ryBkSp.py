import maya.cmds as mc
import pymel.core as pm

selList = mc.ls(sl = True, transforms = True)
print mc.ls(sl = True, type = "joint", uuid=True)
print pm.ls(sl = True, type = "transform", uuid = True)

for x in selList:
    print ''
    print mc.ls(selection = True, showType=True )

bindControl = selList[0]
bindJoint = selList[1]

print str(bindControl)

#check if duplicate objects exist in a scene
check = mc.select(bindControl , r = True)
print check


allObj = mc.ls(g=1, shortNames=1, type = "nurbsCurve")
allPolyObjs = mc.listRelatives(allObj,parent=1,f=1)
for item in allPolyObjs:
    print 'my short name = ' + item.split('|')[-1]
    
    
    
    
#Find duplicates
duplicateObjects = []
transformNodes = mc.ls(type = "transform", sn =True)
dup_list = transformNodes

for x in transformNodes:
    shortName_x = x
    splitName_x  = x.split('|')[-1]
    dup_list.remove(x)
    
    for i in dup_list:
        shortName_i = i
        splitName_i  = i.split('|')[-1] 
        if shortName_x == shortName_i or splitName_x == splitName_i:
            duplicateObjects.append(mc.ls(i,x))
        
numDuplicates = len(duplicateObjects)
print "you have %s duplicates in your scene!" % numDuplicates
print "Duplicates:  %s" % duplicateObjects


for each in duplicateObjects:
    mc.select(each, add = True)

        