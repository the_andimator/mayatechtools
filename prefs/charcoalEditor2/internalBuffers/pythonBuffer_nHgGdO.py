import maya.cmds as cmds
import pymel as pm

def getAttName(fullname):
    parts = fullname.split('.')
    return parts[-1]

def copyKeyframes():
    objs = cmds.ls(selection=True)

    if (len(objs) < 2):
        cmds.error("Please select at least two objects")

    sourceObj = objs[0]

    animAttributes = cmds.listAnimatable(sourceObj);
    print "animAttributes:   " + str(animAttributes)

    for attribute in animAttributes:

        numKeyframes = cmds.keyframe(attribute, query=True, keyframeCount=True)

        if (numKeyframes > 0):

            cmds.copyKey(attribute)

            for obj in objs[1:]:
                print "obj:" + str(obj)
                print "attribute:   " + attribute
                cmds.pasteKey(obj, attribute=getAttName(attribute), option="replace")

copyKeyframes()

for key in sorted(os.environ.keys()):
 	print key, os.environ[key]