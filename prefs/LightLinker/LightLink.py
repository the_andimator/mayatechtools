#User Interface
# First of all let me grab the Qt module because it has somethings I want that I don't need to use often
import json
import os
import time
import Qt
from Qt import QtWidgets, QtCore, QtGui
from Qt import QtCompat

# This is the logging module
# It is a much better way of logging output instead of using print statements
import logging
logging.basicConfig()
logger = logging.getLogger('LightingManager')
logger.setLevel(logging.DEBUG)

if Qt.__binding__.startswith('PyQt'):
    logger.debug('Using sip')
    from sip import wrapinstance as wrapInstance
    from Qt.QtCore import pyqtSignal as Signal
elif Qt.__binding__ == 'PySide':
    logger.debug('Using shiboken')
    from shiboken import wrapInstance
    from Qt.QtCore import Signal
else:
    logger.debug('Using shiboken2')
    from shiboken2 import wrapInstance

from maya import OpenMayaUI as omui 
from maya.app.general.mayaMixin import MayaQWidgetBaseMixin
import pymel.core as pm
from pymel import *

#ui_filename = "C:\Users\Andy-BP\Documents\untitled\mainwindow.ui" 
ui_filename =  "C:\Users\Andy-BP\Documents\maya/2017/ui\LightLinker\mainwindow.ui"
base_class = QtCompat.loadUi(ui_filename)


class LightLinkingFunctionality():

    def __init__(self):
        print ''