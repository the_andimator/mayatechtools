import maya.cmds as mc
import pymel.core as pm


#Convert Materials to a specific shader
#


def GetTexturesFromMaterial(sel):
    print ''
    attrlist = mc.listConnections(sel, type="shadingEngine", connections=True, plugs=True)
    print attrlist
    #mat = mc.getAttr('material')

    
    
selectedObj = mc.ls(sl= True)
matList = mc.ls(mat=1)
inConnections = iter(mc.listConnections (matList, type="shadingEngine", connections=True, plugs=True))
assignedMat = mc.ls( mc.listConnections (inConnections), mat=1)
assigned = mc.hyperShade( shaderNetworksSelectMaterialNodes=True )

print ''
print matList
print assignedMat
print inConnections
print assigned


sel = mc.ls(selection=True)
shape = mc.listRelatives (sel, shapes=True )
shadingEngine = mc.listConnections (shape, source=False, destination=True)
material = mc.listConnections (shadingEngine, source=True, destination=False)
attr = shadingEngine[0]+'.surfaceShader'
plug = mc.connectionInfo( attr, sourceFromDestination=True)

print 'plug:        ' + str(plug)
print 'attr:        ' + str(attr)
print "material:        " + str(material)
print 'shadingEngine:       ' + str(shadingEngine)
print 'shape:       '+ str(shape)
print 'sel:     ' + str(sel)

#Assign texture types
#Color (Diffuse)
#Normal
#Metallic
#Roughness
#Emissive
#AO
#Transparency (From Color map alpha, or separate map)

#Select target material
#Select material to convert to (StingRay PBR, Blinn, Phong)

#Grab textures if already connected
#load use selected textures (use string to determine what type of tex they are)


#
