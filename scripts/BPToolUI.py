import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
from functools import partial
import maya.app.renderSetup.model.override as override
import maya.app.renderSetup.model.collection as collection
import maya.app.renderSetup.model.renderLayer as RL
import maya.app.renderSetup.model.renderSetup as renderSetup
import json
import os.path as path
import sys
import RenderPrep as RP
import maya.api.OpenMayaRender  as omr
import maya.api.OpenMayaUI  as omui
import maya.api.OpenMaya  as om



class BPToolUI(object):

    global prepWin
    global toolsetVer
    global renderLayerWin
    renderLayerWin = ''
        
    toolsetVer = "01.20" 
    threadBox = None
    threadSlider = None
    
    def __init__(self):
        try:
            mc.deleteUI(dockable, window = True)
            mc.deleteUI(prepWin, window = True)
        except:
            pass
                
        self.windowName = windowName = "BP_Pipeline_Tools"
        self.dockable = dockable = "BP_Tools_Window"
        
        threadAttr = None
        ThreadCount = None
        
        #Class input variables
        visDif = False
        visSpec = False
        visDiffTrans = False
        visSpecTrans = False
        visOpaque = False
        visPrimary = True
        visCastShadows = True
        
        
        CONST_METAL_GOLD = 'MAT_Inlay_Gold_01SG1'
        CONST_METAL_SILVER = 'MAT_Inlay_Silver_01SG'
        CONST_METAL_PORCELAIN = 'MAT_Inlay_Porcelain_01SG'

        LocateHDRI = pm.ls('HDRI_SkydomeLight.', r = True)

        if LocateHDRI:
            if LocateHDRI[0]:
                CONST_HDRI_MAP = str(LocateHDRI[0])
        else:
            CONST_HDRI_MAP = ''

        self.visDif = visDif
        self.visSpec = visSpec
        self.visSpecTrans = visSpecTrans
        self.visDiffTrans = visDiffTrans
        self.visOpaque = visOpaque
        self.visPrimary = visPrimary
        self.visCastShadows = visCastShadows

        self.CONST_METAL_GOLD = CONST_METAL_GOLD
        self.CONST_METAL_SILVER = CONST_METAL_SILVER
        self.CONST_METAL_PORCELAIN = CONST_METAL_PORCELAIN
        self.CONST_HDRI_MAP = CONST_HDRI_MAP
        self.threadAttr = threadAttr
        self.ThreadCount = ThreadCount

        if mc.window(self.windowName, exists = True):
            mc.deleteUI(self.windowName)
            
        if mc.window(self.dockable, exists = True):
            mc.deleteUI(self.dockable)
            
##############################################################################################
#################################___________UI___________#####################################  
##############################################################################################

    def BuildUI(self, windowName):
        
        try:
             mc.deleteUI(self.windowName, window = True)
        except:
            pass
        
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "BP_ToolShelf"
        
        try: 
            mc.deleteUI(WinName)
        except:
            pass
            
        prepWin = mc.window(self.windowName, w =250, h = 300, mnb = True, mxb = True, s = 1, rtf = True)
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 250, h= 350)

        #Create menu items
        menuBarLayout = mc.menuBarLayout()
        mc.menu( label='File' )
        mc.menuItem( label='Render Sequencer', i = "icon_rendersequence.png", c = lambda *_: OpenRenderSequencer() )
        mc.menuItem( label='Open Reference Editor', i = "workflow_30.png", c = lambda *_: OpenReferenceEditor() )

        mc.menu( label='Version', helpMenu=True )
        mc.menuItem( label='Ver:  ' + toolsetVer )
        
        mc.menu(label = 'Reset', helpMenu = True)
        mc.menuItem(label = "Reset Arnold Nodes", c = lambda*_: ResetArnoldNodes())
        
        ###Create Border and Frames####
        mc.separator( style='in', width=605)
        mc.frameLayout(toolsetVer, parent = prepWin, label = "", cll = False, bv = False)
        headerRowColumn = mc.rowColumnLayout("header", nc = 1, cw = [(1,605)], columnOffset = [(1, "both", 2), (2, "both", 2)])
        #Find Header Image
        headerPath = mc.internalVar(userBitmapsDir=True)
        mc.image( image=headerPath + 'icon_bpLogo.png')
  
        
        ########################_____Add Tools UI_____########################
        ###__Arnold Visibility___###
        mc.frameLayout('Arnold Visible in Diffuse and Glossy', parent = prepWin, label = "Arnold Diffuse and Reflective Transmission", cll = True, bv = False)
        AivisTop = mc.rowColumnLayout("aiVistop",  nc = 5, cw = [(1,175), (2,100), (3,125), (4,103), (5,103)])
        name = mc.text("Set Arnold Diff/Gloss Visibility", align = 'left')
        self.visDif = mc.checkBox("visDif", label= 'Diffuse Refl') 
        self.visSpec = mc.checkBox("visSpec", label= 'Spec Refl')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        self.visDiffTrans = mc.checkBox("visDifTrans", label= 'Diffuse Trans')
        self.visSpecTrans = mc.checkBox("visSpecrans", label= 'Spec Trans')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        self.visPrimary = mc.checkBox("primaryVisibility", label= 'Prim Visibility', v = True)
        self.visCastShadows = mc.checkBox("castsShadows", label= 'Cast Shadows', v = True)
        aiApplySel = mc.button(label = "Apply to Selected", align = "center", c =  lambda *_: self.SetArnoldAttributes(False))
        aiApplyAll = mc.button(label = "Apply to Scene", align = "center",  c =  lambda *_: self.SetArnoldAttributes(True))
        mc.setParent("..")
        
        
        ###__Smooth Scene Meshes___###
        mc.frameLayout('Smooth Scene Meshes', parent = prepWin, label = "Smooth Scene Meshes", cll = True, bv = False)
        SmoothSceneTop = mc.rowColumnLayout("aiVistop", nc = 3, cw = [(1,230), (2,75), (3,300)])
        name = mc.text("Smooth All Polygonal Meshes in scene", align = 'left')
        mc.text(label = "", align = 'left')
        aiSmoothMesh = mc.button(label = "Smooth All Scene Meshes", align = "center", c = lambda *_: self.AttemptToSmoothAll())
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        aiUnSmoothMesh = mc.button(label = "De-Smooth All Scene Meshes", align = "center", c = lambda *_: self.SmoothMeshes(False))
        mc.setParent("..")


        ###__Set Arnold Opacity___###
        mc.frameLayout('Set Arnold Opacity',parent = prepWin, label = "Set Arnold Opacity", cll = True, bv = False)
        OpacVis = mc.rowColumnLayout("aiVistop", nc = 6, cw = [(1,175), (2,80), (3,50), (4,300)])
        name = mc.text("Set Arnold Dif/Ref Transmission", align = 'left')
        self.visOpaque = mc.checkBox("visOpaque", label= 'Opaque', align = 'left') 
        mc.text(label = "", align = 'left')
        
        aiApplyAll = mc.button(label = "Apply to Selected", align = "center", c = lambda *args: self.SetArnoldOpacity())
        mc.setParent("..")
        
        
        ####Apply Values to variables
        visOpaque = mc.checkBox(self.visOpaque, q=True, v=True)


        ###__Create and Add objects to render layers___###
        mc.frameLayout('Create Render Layer and Add Selected', parent = prepWin,label = "Create Render Layer and Add Selected", cll = True, bv = False) 
        mc.rowColumnLayout(nc = 2, cw = [(1,275),(2, 302)])
        currentRenderTitle = mc.text(label = "Current Render Layers", align = 'left')
        #collectionTitle = mc.text(label = "Collections", align = 'left')
        mc.setParent("..")
        
        
        #create default lists
        mc.rowColumnLayout("addLayer", nc = 3, cw = [(1,250), (2,25), (3,250), (4,5), (5,150), (8,20)])
        sl = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindRenderlayers())
        mc.text(label = "", align = 'left')
        #slCollections = pm.textScrollList(allowMultiSelection = True, append = self.FindCollections(sl))
        pm.iconTextScrollList(sl, edit = True, allowMultiSelection = True, doubleClickCommand = lambda *args: (self.PrepareWindow(sl)))
        mc.setParent("..")
        #pm.textScrollList(sl, edit = True, allowMultiSelection = True, doubleClickCommand = lambda *args: (self.FindCollections(sl)))
        mc.rowColumnLayout("addLayer2", nc = 2, cw = [(1,225), (2,20), (3,5), (4,5), (5,150), (8,20)])
        renderRenderLayer = mc.button(label = "+ Create a New Render Layer", align = "center", c = lambda *args: self.CreateLayerWindow(sl))
        reficon = headerPath + "icon_refresh.png"
        self.inputRefreshButton = mc.symbolButton(w = 23, h = 23, image = reficon, ebg = True, c = lambda *args: self.UpdateRenderLayers(sl))
        addToRenderRenderLayer = mc.button(label = "Add Selected to Render Layers", align = "center", c = lambda *args: self.AddToRenderLayers(sl) )
        mc.text(label = "", align = 'left')
        removeFromRenderRenderLayer = mc.button(label = "Remove Selected from Render Layers", align = "center", c = lambda *args: self.RemoveFromRenderLayers(sl) )
        mc.text(label = "", align = 'left')
        isolateRL = mc.button(label = "Isolate Single Layer", align = "center", c = lambda *args: self.IsolateRL(sl) )
        mc.text(label = "", align = 'left')
        OpenSetup = mc.button(label = "Open Render Setup Window", align = 'center')
        mc.button(OpenSetup, edit = True, c = lambda *args: self.OpenRenderSetup(OpenSetup))
        mc.setParent("..")
        
        ###__Create Quick Select Sets___###
        CreateQuickSelectSet = mc.frameLayout("Create Quick Select Set", parent = prepWin, label = "Create Quick Select Set", cll = True, bv = False)
        quickSelect = mc.rowColumnLayout("quickSelect", nc =1, cw = [(1,605)])
        mc.setParent("..")
        name = mc.text("Current Quick Select Sets", align = 'left')

        quickSet = mc.rowColumnLayout("quickSet", nc = 1, cw = [(1,605)])
        mc.setParent("..")
        mc.rowColumnLayout("quickSetMenu", nc = 2, cw = [(1,250), (2,20)])
        qss = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindQuickSelect())
        pm.iconTextScrollList(qss, edit = True, doubleClickCommand = lambda *args:(self.SelectQuickSet(qss)))
        mc.setParent("..")
        
        mc.rowColumnLayout("quickSetButton", nc = 2, cw = [(1,225), (2,25)])
        createQuickSet = mc.button(label = "+ Create Quick Select", align = "center", c = lambda *args: self.CreateQuickSelWindow(qss))
        refreshSelect = mc.symbolButton(w = 23, h = 23, image = reficon, ebg = True, c = lambda *args: self.UpdateQuickSelLayers(qss))
        mc.rowColumnLayout("quickSetButton", nc = 1, cw = [(1,225), (2,25)])
        AddSelectedToSet = mc.button(label = "Add Selected to Quick Select", align = "center", c = lambda *args: self.AddToQuickSelectSet(qss))
        RemoveSelectedToSet = mc.button(label = "Remove Selected to Quick Select", align = "center", c = lambda *args: self.RemoveFromQuickSelectSet(qss))
        RenameSelected = mc.button("Rename Selected",label = "Rename Quick Select", align = "center", c = lambda *args: self.RenameQuickSel(qss))
        DeleteSet = mc.button("DeleteSet", label = "Delete Quick Select Set", align = "center", c = lambda *args: self.DeleteQuickSel(qss))
        mc.text(label = "", align = 'left')
        mc.setParent(CreateQuickSelectSet)
        mc.setParent("..")

        
        ###__Remove Unused Materials___###
        RemoveUnusedMaterials = mc.frameLayout("Remove Unused Materials", parent = prepWin,label = "Remove Unused Materials", cll = True, bv = False)
        mc.rowColumnLayout("caution",  nc = 1, cw = [(1,450)])
        name = mc.text("CAUTION: This might break active shading networks. Save Scene before using.", align = 'left', font = "boldLabelFont")
        mc.setParent(RemoveUnusedMaterials)
        removeMatCol = mc.rowColumnLayout("removeMatCol",  nc = 4, cw = [(1,175), (2,80), (3,50), (4,300)])
        mc.text("Remove Unused Materials", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        
        removeMaterials = mc.button(bgc = [1.0,.2,.2],label = "Remove Materials", align = "center" , c = lambda *args: self.AttemptToRemoveUnusedMat())
        mc.text(label = "", align = 'left')
        mc.setParent(RemoveUnusedMaterials)
       
        
        
        ###__Renders Settings and Scene Prep___###
        RenderSettingsandPrep = mc.frameLayout("RenderSettingsandPrep", parent = prepWin,label = "Render Settings and Prep", cll = True, bv = False)
        mc.separator( style='in', width=605)
        RenderColumn = mc.rowColumnLayout("RenderColumn",  nc = 2, cw = [(1,150), (2,300)])
        
        curCamAA = mc.getAttr("defaultArnoldRenderOptions.AASamples")
        curDif = mc.getAttr("defaultArnoldRenderOptions.GIDiffuseSamples")
        curSpec = mc.getAttr("defaultArnoldRenderOptions.GISpecularSamples")
        curTrans = mc.getAttr("defaultArnoldRenderOptions.GITransmissionSamples")
        curSSS = mc.getAttr("defaultArnoldRenderOptions.GISssSamples")
        curVolIn= mc.getAttr("defaultArnoldRenderOptions.GIVolumeSamples")
        curLockSample= mc.getAttr("defaultArnoldRenderOptions.lock_sampling_noise")
        curBucketSize = mc.getAttr("defaultArnoldRenderOptions.bucketSize")
        curTransDepth = mc.getAttr("defaultArnoldRenderOptions.autoTransparencyDepth")
        curMaxThreads = mc.getAttr("defaultArnoldRenderOptions.threads")
        curMaxThreadsBool = mc.getAttr("defaultArnoldRenderOptions.threads_autodetect")
        
        mc.text(label = 'Camera(AA)', align = 'left')
        camAA = mc.intSliderGrp("camAA", field=True, value = curCamAA, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'Diffuse', align = 'left')
        diffuse = mc.intSliderGrp("diffuse", field=True,  value = curDif, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'Specular', align = 'left')
        specular = mc.intSliderGrp("Specular", field=True,  value = curSpec, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'Transmission', align = 'left')
        transmission = mc.intSliderGrp("Transmission", field=True,  value = curTrans, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'SSS', align = 'left')
        SSS = mc.intSliderGrp("sss", field=True,  value = curSSS, maxValue=10, fieldMaxValue=25)
        
        mc.text(label = 'VolumeIndirect', align = 'left')
        VolumeIndirect = mc.intSliderGrp("VolumeIndirect", field=True,  value = curVolIn, maxValue=10, fieldMaxValue=25)
        mc.setParent("..") 
        RenderColumn2 = mc.rowColumnLayout("RenderColumn2",  nc = 4, cw = [(1,150), (2,81), (3,150), (4, 81)])
        
        mc.text(label = 'Transparency Depth', align = 'left')
        TransparencyDepth = mc.intField(value = curTransDepth)
        mc.text(label = 'Transparency Threshold', align = 'center')
        TransparencyThreshold = mc.floatField(value = curTransDepth)
        
        mc.text(label = 'BucketSize', align = 'left')
        BucketSize = mc.intField(value = curBucketSize)
        mc.setParent("..") 
        
        ##############
        
        RenderColumn3 = mc.rowColumnLayout("RenderColumn3",  nc = 3, cw = [(1,100), (2,100), (3,250)])
        
        lock_sampling = pm.checkBox("LockSampling", label = "Lock Sampling", ed = True, value = curLockSample)
        lockSample = pm.checkBox("LockSampling", q = True, v = True)
        threadBox = pm.checkBox("Thread", label = "Max Threads", ed = True, value = curMaxThreadsBool, changeCommand = partial (self.checkbox_toggled))
        thread_slider = pm.intSliderGrp("ThreadSlider",  step = 1, field=True, maxValue = 16, minValue = -16, w = 10, value = curMaxThreads, enable = False)
        self.threadBox = pm.checkBox("Thread", q = True, value = True)
        self.threadSlider = pm.intSliderGrp("ThreadSlider", q = True, value = True)
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        _fileType = pm.optionMenu(label = "Image Format")
        pm.menuItem(label = "tif")
        pm.menuItem(label = "png")
        pm.menuItem(label = "exr")
        pm.setParent( '..', menu=True )
        
        d = pm.PyNode( 'defaultArnoldDriver' )
        d.ai_translator.set('tif')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text(label = "", align = 'left')
        SelectObjectsOf = mc.button(label = "Apply Settings", align = "center" , c = lambda *args: self.ApplyRenderSettings(camAA, 
                                                                                                                            diffuse, 
                                                                                                                            specular, 
                                                                                                                            transmission, 
                                                                                                                            SSS, 
                                                                                                                            VolumeIndirect, 
                                                                                                                            TransparencyDepth, 
                                                                                                                            TransparencyThreshold, 
                                                                                                                            BucketSize,
                                                                                                                            lockSample,
                                                                                                                            _fileType))
        mc.separator( style='in', width=605)
        mc.text(label = "", align = 'left')
        
        EndFrame = mc.frameLayout("EndFrame", parent = prepWin,label = "", cll = False, bv = False, bgc = [.25,.25,.25])
        EndFrameColumn = mc.rowColumnLayout("EndFrame",  nc = 1, cw = [(1,605)])
                
        if mc.window(WinName, exists = True):
            mc.deleteUI(WinName)
        dockable = mc.dockControl(WinName, area = 'left', content=prepWin, allowedArea='all', mov = True, s = True )
        mc.showWindow()   
    
    def checkbox_toggled(self, *args):
        slider_enabled = pm.checkBox("Thread", q = True, value = True)
        print slider_enabled
        if slider_enabled:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = False)
        else:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = True)
        
##############################################################################################
##########################___________   FUNCTIONALITY___________##############################   
##############################################################################################
    def OpenRenderSetup(self, OpenSetup):
        if not mc.pluginInfo("renderSetup.py", loaded=True, query=True):
            mc.loadPlugin("renderSetup.py")
        import maya.app.renderSetup.views.renderSetup as renSetup
        renSetup.createUI()
        mel.eval('interToUI("collection");')
        mel.eval('interToUI("collection");')
        
        
    def SetArnoldAttributes(self, all):
        #Apply Values to variables
        visDif = mc.checkBox(self.visDif, q= True, v=True, changeCommand = True)
        visSpec = mc.checkBox(self.visSpec, q= True, v= True, changeCommand = True)
        visSpecTrans = mc.checkBox(self.visSpecTrans, q= True, v= True, changeCommand = True)
        visDiffTrans = mc.checkBox(self.visDiffTrans, q= True, v= True, changeCommand = True)
        
        visCastShadows = mc.checkBox(self.visCastShadows, q= True, v= True, changeCommand = True)
        visPrimary = mc.checkBox(self.visPrimary, q= True, v= True, changeCommand = True)
        
        if all == True:
            print "visDif"
            print visDif
            print "visSpec"
            print visSpec
            meshes = mc.ls(geometry = False, shapes = True,  type='geometryShape', showType=True)
            for i in meshes:  

                specAttr = i + '.aiVisibleInSpecularReflection'
                diffAttr = i + '.aiVisibleInDiffuseReflection'
                diffAttrTrans = i + '.aiVisibleInDiffuseTransmission'
                specAttrTrans = i + '.aiVisibleInSpecularTransmission'
                
                visShadowAttrTrans = i + '.castsShadows'
                visPrimAttrTrans = i + '.primaryVisibility'
                
                if mc.objExists(specAttr):
                    mc.setAttr(diffAttr, visDif)
                    mc.setAttr(specAttr, visSpec)
                    mc.setAttr(diffAttrTrans, visDiffTrans)
                    mc.setAttr(specAttrTrans, visSpecTrans)
                    mc.setAttr(visPrimAttrTrans, visPrimary)
                    mc.setAttr(visShadowAttrTrans, visCastShadows)
                    
                       
        elif all == False:

            selected = mc.ls(sl= True)
            getShape = mc.listRelatives(selected, shapes=True)
            if getShape != None:
                for i in getShape:

                    specAttr = i + '.aiVisibleInSpecularReflection'
                    diffAttr = i + '.aiVisibleInDiffuseReflection'
                    diffAttrTrans = i + '.aiVisibleInDiffuseTransmission'
                    specAttrTrans = i + '.aiVisibleInSpecularTransmission'
                    
                    visShadowAttrTrans = i + '.castsShadows'
                    visPrimAttrTrans = i + '.primaryVisibility'
                
                    if mc.objExists(specAttr):
                        mc.setAttr(diffAttr, visDif)
                        mc.setAttr(specAttr, visSpec)
                        mc.setAttr(diffAttrTrans, visDiffTrans)
                        mc.setAttr(specAttrTrans, visSpecTrans)
                        mc.setAttr(visPrimAttrTrans, visPrimary)
                        mc.setAttr(visShadowAttrTrans, visCastShadows)
            else:
                mc.warning("No meshes are selected. Unable to apply changes.")

        
    def SmoothMeshes(self, action):
        meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True) 
        for i in meshes:
            smooth = i + ".displaySmoothMesh"
            if mc.objExists(smooth):
                if action == True:
                    mc.setAttr(smooth, 2)
                if action == False:
                    mc.setAttr(smooth, 0)
        
    def SetArnoldOpacity(self):     
        opaque = mc.checkBox(self.visOpaque, q= True, v= True, changeCommand = True)
        selected = mc.ls(sl= True) 
        if selected != []:
            for i in selected:
                obj = i + ".aiOpaque"
                if mc.objExists(obj):
                    mc.setAttr(obj, opaque)
        else:
            mc.warning("No meshes were found in scene. Unable to apply .aiOpaque setting.")
            
    def AddToRenderLayers(self, selection):
        col1 = collection.Collection()
        #Get render layer selection from GUI selection
        renderLayerSel = selection.getSelectItem()
        if renderLayerSel != None:
            renderLayerSele = ''.join(renderLayerSel)
            import re
            rlSl = renderLayerSele[3:]

            rs = renderSetup.instance()
            rl = rs.getRenderLayer(rlSl)
            clName = rl.getBack()

            if clName == None:
                c1 = rl.createCollection("RL_Built_from_script")
            
            #print firstCollection
            clName = rl.getBack()
            print clName
            #print rl.GetCollections()
            selected = mc.ls(sl=True)
            if selected != []:
                for i in selected:
                    print i
                    clName.getSelector().staticSelection.add(selected)
            else:
                mc.warning("No objects selected. Please selected something.")
        else:
            mc.warning("No layers are selected. Please select a render layer")

    def RemoveFromRenderLayers(self, selection):
        col1 = collection.Collection()
        #Get render layer selection from GUI selection
        renderLayerSel = selection.getSelectItem()
        if renderLayerSel != None:
            renderLayerSele = ''.join(renderLayerSel)
            import re
            
            rlSl = renderLayerSele[3:]
            #rlSl = re.sub('rs_', '', rlSl)
            
            print rlSl #store render layer into variable
            print "____________^^^^"
            rs = renderSetup.instance()
            rl = rs.getRenderLayer(rlSl)
            clName = rl.getBack()

            if clName == None:
                print ''
                #c1 = rl.createCollection("RL_Built_from_script")
            else:
                selected = mc.ls(sl=True)
                if selected != []:
                    for i in selected:
                        print i
                        clName.getSelector().staticSelection.remove(selected) 
                else:
                    mc.warning("No objects selected. Please selected something.")
        else:
            mc.warning("No layers are selected. Please select a render layer")   
                      
    def IsolateRL(self, selection):
        renderLayerSel = selection.getSelectItem()
        renderLayerSele = ''.join(renderLayerSel)
        import re
        rlSl = renderLayerSele[3:]
        
        rs = renderSetup.instance()
        test = rs.getVisibleRenderLayer()
        rl = rs.getRenderLayer(rlSl)
        defaultRL = rs.getDefaultRenderLayer()
        
        if test == rl:
            rs.switchToLayer(defaultRL)
        else:
            rs.switchToLayer(rl)
        
    def AttemptToRemoveUnusedMat(self):
        result = mc.confirmDialog(
		title='Remove Unused Materials in Scene',
		message='                                          Are you sure you want to remove unused materials?',
		messageAlign = 'center',
		button=["Yes. I'm prepared to potentially break my scene.", 'Nope. Definitely Not. Take me back to safety, Captain'],
		defaultButton="Nope. Definitely Not. Take me back to safety, Captain",
		cancelButton='Nope. Definitely Not. Take me back to safety, Captain',
		dismissString='Nope. Definitely Not. Take me back to safety, Captain',
		bgc = [1.0,.4,.4])

        if result == "Yes. I'm prepared to potentially break my scene.":
            mel.eval('MLdeleteUnused')
            
    def AttemptToSmoothAll(self):
        result = mc.confirmDialog(
		title='Smooth All Objects',
		message='Attempting to smooth all objects may cause scene crashes. It is recommended that you save your scene first',
		messageAlign = 'center',
		button=["Smooth Scene without saving", 'Do not smooth. I need to save my changes first'],
		defaultButton="Do not smooth. I need to save my changes first",
		cancelButton='Do not smooth. I need to save my changes first',
		dismissString='Do not smooth. I need to save my changes first',
		bgc = [1.0,.4,.4])

        if result == "Smooth Scene without saving":
            self.SmoothMeshes(True)
        
    def FindQuickSelect(self):
        qSet = []
        setObj = mc.ls(exactType = "objectSet")
        for set in setObj:
            if mc.sets(set, q = True, text = True) == "gCharacterSet":
                qSet.append(set)
        return qSet
    
    def RenameQuickSel(self, qss):
        if qss.getSelectItem() != None:
            oldName = "".join(qss.getSelectItem())
            result = mc.promptDialog(
            title='',
            message='Rename Set',
            text = oldName,
            button=["Rename", 'Cancel'],
            defaultButton="Create",
            cancelButton='Cancel',
            dismissString='Cancel')

            if result == "Rename":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.rename(oldName, newName)
                self.UpdateQuickSelLayers(qss)
        else:
            mc.warning("Nothing is selected, unable to rename quick select set.")  
               
    def DeleteQuickSel(self, qss):
        if qss.getSelectItem() != None:
            setToDel = "".join(qss.getSelectItem())
            result = mc.confirmDialog(
            title='',
            message='Are you sure you want to delete "' + setToDel + '"',
            button=["Yes. Get it outta here.", 'Nevermind'],
            defaultButton="Nevermind",
            cancelButton='Nevermind',
            dismissString='Nevermind')

            if result == "Yes. Get it outta here.":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.delete(setToDel)
                self.UpdateQuickSelLayers(qss)
        else:
            mc.warning("Nothing is selected, unable to delete quick select set.")
            
    def SelectQuickSet(self, qss):
        set = "".join(qss.getSelectItem())
        mc.select(set, r = True)
        
    def CreateQuickSelect(self, qsName):
        mc.sets(name = qsName, text = "gCharacterSet" )
        
    def CreateQuickSelWindow(self, sl):
        result = mc.promptDialog(
		title='',
		message='Add Set',
		text = 'newSet_01',
		button=["Create Set using Selected", 'Cancel'],
		defaultButton="Create",
		cancelButton='Cancel',
		dismissString='Cancel')

        if result == "Create Set using Selected":
            newLayer = mc.promptDialog(query=True, text=True)
            self.CreateQuickSelect(newLayer)
            self.UpdateQuickSelLayers(sl)
            
    def AddToQuickSelectSet(self, qss):
        if qss.getSelectItem() != None:
            quickSet = "".join(qss.getSelectItem())
            selected = mc.ls(sl= True)
            
            print selected
            
            if selected == [] or selected == None:
                mc.warning("No meshes were selected, nothing was added to " + quickSet)
            else:
                for i in selected:
                    mc.sets(selected, add = quickSet)
        #mc.sets(edit = True, forceElement = True, addElement = selected)
        else:
            mc.warning("Nothing is selected, unable to add to quick select set.")
            
    def RemoveFromQuickSelectSet(self, qss):
        if qss.getSelectItem() != None:
            quickSet = "".join(qss.getSelectItem())
            selected = mc.ls(sl= True)
            
            print selected
            
            if selected == [] or selected == None:
                mc.warning("No meshes were selected, nothing was added to " + quickSet)
            else:
                for i in selected:
                    mc.sets(selected, rm = quickSet)
        #mc.sets(edit = True, forceElement = True, addElement = selected)
        else:
            mc.warning("Nothing is selected, unable to add to quick select set.")    
                          
    def UpdateQuickSelLayers(self, sl):
        list = self.FindQuickSelect()
        #sl = pm.textScrollList()
        sl.removeAll()
        pm.iconTextScrollList(sl, edit = True, allowMultiSelection=True)
        for x in list:
            sl.append(x)
           
    def FindRenderlayers(self):
        render_layer_list = mc.ls(type="renderLayer")
        defaultRenderLayer = "defaultRenderLayer"
        refDefaultRenderLayer = ":defaultRenderLayer"
        renderList = []
        print render_layer_list
        for i in render_layer_list:
            if i == defaultRenderLayer:
                continue
            elif defaultRenderLayer in i: 
                continue
            elif refDefaultRenderLayer in i: 
                continue
            else:
                renderList.append(i)
        
        return renderList
    
    def FindCollections(self, sl):
        test = pm.iconTextScrollList(sl, q = True, allowMultiSelection=True)
        
        rs = renderSetup.instance()
        getLayers = rs.getRenderLayers()
        collections1 = RL.collection.collections
        collections = RL.RenderLayer()
        test = collections.collectionHighest

        appendTo = []
        for i in getLayers:
            appendTo.append(i)
            print i
            
        print appendTo
        #if selected == None, return no render layers
        
        #renderLayerSel = omr.MRenderItemList.indexOf(selection)
        
        #if renderLayerSel != None and renderLayerSel != "":
        #    rlSl = ''.join(renderLayerSel)
        #    import re
        #    rlSl = re.sub('rs_', '', rlSl)
        #    rs = renderSetup.instance()
        #    rl = rs.getRenderLayer(rlSl)
        #    clName = rl.getBack()
        #    print ''
        #else:
        #  mc.warning("There is nothing selected")

        
    def CreateLayerWindow(self, sl):
        result = mc.promptDialog(
		title='',
		message='Add Layer',
		text = 'new render layer',
		button=["Create Layer", 'Cancel'],
		defaultButton="Rename",
		cancelButton='Cancel',
		dismissString='Cancel')

        if result == "Create Layer":
            newLayer = mc.promptDialog(query=True, text=True)
            self.CreateRenderLayers(sl, newLayer)
            self.UpdateRenderLayers(sl)
            
    def CreateRenderLayers(self, sl, layerName):
        layerExists = False
        render_layer_list = mc.ls(type="renderLayer")
        folderRL = layerName
        collectionName = layerName + "_collection"
        if "rs_" in layerName:
            print"'rs_' found in layer already. Nice."
            newLayerName = layerName
        else:
            newLayerName = "rs_" + layerName
            print ("'rs_' not found in render layer, added rs_ to front of string")
            print ("new string =    " + layerName)
            
        for i in render_layer_list:
            if i == newLayerName:
                layerExists = True
                break
        
        if layerExists == False: 
            rs = renderSetup.instance()
            rl = rs.createRenderLayer(layerName)
            #c = rl.createCollection(collectionName)
        
        
    def UpdateRenderLayers(self, sl):
        render_layer_list = mc.ls(type="renderLayer")
        defaultRenderLayer = "defaultRenderLayer"
        renderList = []
        curSel= sl.getSelectItem()
        print curSel
        renderList = self.FindRenderlayers()
        sl.removeAll()
        pm.iconTextScrollList(sl, edit = True, allowMultiSelection=True)
        for x in renderList:
            sl.append(x)
        #sl = sl.replace("||", "|")
        #mc.iconTextScrollList(sl, append = True, si = True)
        
    def PrepareWindow(self, sl):
        oldVal = ""
        oldVal = "".join(sl.getSelectItem())
        result = mc.promptDialog(
		title='',
		message='Rename layer',
		text = oldVal,
		button=["Rename", 'Cancel'],
		defaultButton="Rename",
		cancelButton='Cancel',
		dismissString='Cancel')

        if result == "Rename":
            newVal = mc.promptDialog(query=True, text=True)
            self.RenameLayer(sl, newVal, oldVal)
            self.UpdateRenderLayers(sl)
       
    def RenameLayer(self, sl, newVal, oldVal):
        rs = renderSetup.instance()
        # Get the list of all render layers
        render_layer_list = mc.ls(type="renderLayer")
        if "rs_" in newVal:
            print"'rs_' found in layer already. Nice."
        else:
            #newLayerName = "rs_" + newVal
            print ("'rs_' not found in render layer, added rs_ to front of string")
        self.UpdateRenderLayers(sl)
        import os.path as path
        projectRoot = mc.internalVar(userWorkspaceDir=True)
        src = path.dirname(path.dirname(projectRoot))
        src = src + "/RSTemplates/"
        sceneName = pm.sceneName()
        sceneName = path.basename(sceneName)
        sceneName = path.splitext(sceneName)[0]
        fullpath = src + sceneName + ".json"
        self.exportRenderSetup(fullpath, "DidThisWork")
        #Change text inside document
        self.FindandReplaceTextinJson(newVal, oldVal, fullpath)
        #reimport updated render layer
        self.importRenderSetup(fullpath)
       
    def FindandReplaceTextinJson(self, newVal, oldVal, path):

        if newVal[0] == "r":
            if newVal[1] == "s":
                if newVal[2] == "_":
                    newVal = newVal[3:]
                    print newVal
                    
        if oldVal[0] == "r":
            if oldVal[1] == "s":
                if oldVal[2] == "_":
                    fixedVal = oldVal[3:]
                    print fixedVal
                    
        import fileinput
        print oldVal
        fixedVal = '"' + fixedVal + '"'
        newVal = '"' + newVal + '"'
        print "__________________________"
        print newVal
        print fixedVal
        print path
        
        print "THESE ARE THE NEW VALUES"
        with open(path, 'r+') as f:
            content = f.read()
            f.seek(0)
            f.truncate()
            f.write(content.replace(fixedVal, newVal))

    def importRenderSetup(self, filename):
        with open(filename, "r") as file:
            renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)
     
    def exportRenderSetup(self, filename , note = None):
        with open(filename, "w+") as file:
            json.dump(renderSetup.instance().encode(note), fp=file, indent=2, sort_keys=True)
            
        
    def ApplyRenderSettings(self, camAA, diffuse, specular, transmission, SSS, VolumeIndirect, TransparencyDepth, TransparencyThreshold, BucketSize, lockSample, imageFormat):
        #check to make sure that arnold is imported already
        #if mc.pluginInfo( 'mtoa.mll', q = True, loaded = True):
        #    print "Arnold plugin already loaded"
        #else:
        #    mc.loadPlugin( 'mtoa.mll' )
        imgFormat = pm.optionMenu(imageFormat, q = True, value = True)
        #d = pm.PyNode( 'defaultArnoldDriver' )
        #d.ai_translator.set(imgFormat)


        slider_enabled = pm.checkBox("Thread", q = True, value = True)

        
        _camAA = pm.intSliderGrp("camAA", q = True, value = True)
        _diffuse = pm.intSliderGrp('diffuse', q = True, value = True)
        _specular = pm.intSliderGrp('Specular', q = True, value = True)
        _transmission = pm.intSliderGrp('Transmission', q = True, value = True)
        _SSS = pm.intSliderGrp('sss', q = True, value = True)
        _VolumeIndirect = pm.intSliderGrp('VolumeIndirect', q = True, value = True)
        
        _TransparencyDepth = pm.intField(TransparencyDepth, q = True, value = True)
        _TransparencyThreshold = pm.floatField(TransparencyThreshold, q = True, value = True)
        _BucketSize = pm.intField(BucketSize, q = True, value = True)
        _maxThreads = pm.intSliderGrp("ThreadSlider", q = True, value = True)
        _lockSample = pm.checkBox("LockSampling", q = True, value = True)
        
        mc.setAttr("defaultArnoldRenderOptions.threads" , _maxThreads)
        mc.setAttr("defaultArnoldRenderOptions.threads_autodetect", slider_enabled)

        mc.setAttr('defaultArnoldDriver.ai_translator', imgFormat, type='string')
        mc.setAttr("defaultArnoldDriver.tiffFormat ", 0)
        pm.mel.eval("setMayaSoftwareFrameExt(3, 0)")
        
        mc.setAttr("defaultArnoldRenderOptions.AASamples", _camAA)
        mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", _diffuse)
        mc.setAttr("defaultArnoldRenderOptions.GISpecularSamples", _specular)
        mc.setAttr("defaultArnoldRenderOptions.GITransmissionSamples", _transmission)
        mc.setAttr("defaultArnoldRenderOptions.GISssSamples", _SSS)
        mc.setAttr("defaultArnoldRenderOptions.GIVolumeSamples", _VolumeIndirect)
        mc.setAttr("defaultArnoldRenderOptions.lock_sampling_noise", _lockSample)
        mc.setAttr("defaultArnoldRenderOptions.bucketSize", _BucketSize)
        
        mc.setAttr("defaultArnoldRenderOptions.autoTransparencyDepth", _TransparencyDepth)
        
        mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", _diffuse)
        mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", _diffuse)
        mc.setAttr("defaultArnoldRenderOptions.GISpecularDepth", _specular)
        mc.setAttr("defaultArnoldRenderOptions.GITransmissionDepth", _transmission)
        mc.setAttr("defaultArnoldRenderOptions.GIVolumeDepth", _VolumeIndirect)
        mc.setAttr("defaultResolution.width", 1920)
        mc.setAttr("defaultResolution.height", 1080)
        mc.setAttr("defaultResolution.dotsPerInch", 300)
            
        
def OpenRenderSequencer():
    import Render_Sequencer as MI
    reload (MI)
    build = MI.SequenceSetup(1)
    build.PrepareWindow()

def OpenReferenceEditor():
    mc.ReferenceEditor()
    
def ResetArnoldNodes():
    from mtoa.core import createOptions
    createOptions()
    mc.warning("Arnold Nodes have been recreated. Settings changed to default values.")