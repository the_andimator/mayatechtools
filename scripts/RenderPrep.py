import maya.cmds as mc
import maya.mel as mel
import maya.app.renderSetup.model.override as override
import maya.app.renderSetup.model.collection as collection
import maya.app.renderSetup.model.renderLayer as renderLayer
import maya.app.renderSetup.model.renderSetup as renderSetup

camAA = 3
diffuse = 3
glossy = 1
refraction = 1
sss = 1
volumeInd = 1
lockSampling = True
transpDepth = 1
transpThres = .990
bucketSize = 135
maxThreads = True

def aiVisible():
    meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True)
    
    for i in meshes:  
        spec = i + '.aiVisibleInSpecularTransmission'
        diff = i + '.aiVisibleInDiffuseTransmission'
        diffR = i + '.aiVisibleInDiffuseReflection'
        specR =i + '.aiVisibleInSpecularReflection'
        if mc.objExists(spec):
            mc.setAttr(diff, 0)
            mc.setAttr(spec, 0)
            mc.setAttr(diffR, 0)
            mc.setAttr(specR, 0)


def ApplyRenderSettings():
    mc.setAttr('defaultArnoldDriver.ai_translator', 'tif', type='string')
    mc.setAttr("defaultArnoldDriver.tiffFormat ", 0)

    mc.setAttr("defaultArnoldRenderOptions.threads_autodetect", maxThreads)
    mc.setAttr("defaultArnoldRenderOptions.AASamples", camAA)
    mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", diffuse)
    mc.setAttr("defaultArnoldRenderOptions.GISpecularSamples", glossy)
    mc.setAttr("defaultArnoldRenderOptions.GITransmissionSamples", refraction)
    mc.setAttr("defaultArnoldRenderOptions.GISssSamples", sss)
    mc.setAttr("defaultArnoldRenderOptions.GIVolumeSamples", volumeInd)
    mc.setAttr("defaultArnoldRenderOptions.lock_sampling_noise", lockSampling)
    mc.setAttr("defaultArnoldRenderOptions.bucketSize", bucketSize)
    
    mc.setAttr("defaultArnoldRenderOptions.autoTransparencyDepth", transpDepth)
    
    mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", diffuse)
    mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", diffuse)
    mc.setAttr("defaultArnoldRenderOptions.GISpecularDepth", glossy)
    mc.setAttr("defaultArnoldRenderOptions.GITransmissionDepth", refraction)
    mc.setAttr("defaultArnoldRenderOptions.GIVolumeDepth", volumeInd)
    mc.setAttr("defaultResolution.width", 1920)
    mc.setAttr("defaultResolution.height", 1080)
    mc.setAttr("defaultResolution.dotsPerInch", 300)
    
def CreateDefaultRenderLayers(layerName, collectionName):
    SkinRef = "Humanoid_Exterior_01"
    SkelRef = "Skeleton_Mesh"
    ArtDisk = "ArticularDisk:Articular_Disk_Grp"
    MuscRef = "Muscles_Mesh"
    visibilityCtrl = "Visible_Meshes"
    TeethRef = ""
    ToolsRef = ""
    Lights = "Lights:Lights"
    
    
    #confirm list of assets
    SkinRef = ConfirmExistence(SkinRef)
    SkelRef = ConfirmExistence(SkelRef)
    ArtDisk = ConfirmExistence(ArtDisk)
    MuscRef = ConfirmExistence(MuscRef)
    visibilityCtrl = ConfirmExistence(visibilityCtrl)
    Lights = ConfirmExistence(Lights)
    
    if mc.objExists(Lights):
        print "Lights exist in scene"
    else:
        mc.directionalLight(n = "Lights:Lights", intensity = 1)
        mc.warning("Unable to find correct Lights in scene, created a default light as placeholder")
        mc.warning("Be sure to check your lighting and make sure it is correct.")
        
    # Get the list of all render layers
    render_layer_list = mc.ls(type="renderLayer")
    folderRL = layerName
    layerExists = False
    
    if "rs_" in layerName:
        print"'rs_' found in layer already. Nice."
    else:
        newLayerName = "rs_" + layerName
        print ("'rs_' not found in render layer, added rs_ to front of string")
        print ("new string =    " + layerName)
        
    for i in render_layer_list: 
        if i == newLayerName:
            layerExists = True
            break
    
    if layerExists == False: 
        rs = renderSetup.instance()
        rl = rs.createRenderLayer(layerName)
        c = rl.createCollection(collectionName)
        
        if layerName == 'Skin_RL':
            c.getSelector().staticSelection.set([SkinRef, Lights])      
            ApplyOverridesToLayers(visibilityCtrl, c,1,0,0,0,0)
           
        if layerName == 'Skel_RL': 
            c.getSelector().staticSelection.set([SkelRef, Lights, ArtDisk])
            ApplyOverridesToLayers(visibilityCtrl, c,0,0,1,0,1)

        if layerName == 'Muscle_RL':
            c.getSelector().staticSelection.set([SkelRef, Lights, ArtDisk, MuscRef])
            ApplyOverridesToLayers(visibilityCtrl, c,0,1,1,0,1)
            
        if layerName == 'Teeth_RL':
            c.getSelector().staticmethod.set([TeethRef, Lights])
            ApplyOverridesToLayers(visibilityCtrl, c,0,0,0,0,0)
        
        if layerName == 'Tools_RL':
            c.getSelector().staticmethod.set([ToolsRef, Lights])
            ApplyOverridesToLayers(visibilityCtrl, c,0,0,0,0,0)

    else:
        mc.warning("Render Layer %s already exists, skipping." % (layerName))
        
def ApplyOverridesToLayers(visibilityCtrl, c, skin, muscle, skeleton, controls, artDisk):
    AddOverrides(visibilityCtrl, c, "Skin", skin)
    AddOverrides(visibilityCtrl, c, "Muscles", muscle)
    AddOverrides(visibilityCtrl, c, "Skeleton", skeleton)
    AddOverrides(visibilityCtrl, c, "Controls", controls)
    AddOverrides(visibilityCtrl, c, "ArticularDisk", artDisk)


def ConfirmExistence(obj):
    if mc.objExists(obj):
        return obj
    else:
       newObj = FindNamespace(obj)

       if mc.objExists(newObj):
            return newObj
       else:
           return obj

           
def FindNamespace(obj):
    if mc.objExists(obj):
        return obj
    else:
       #list namespaces, iterate until objexists
       curNmspce = mc.namespaceInfo(lon = True)
       newObj = ""
       for i in curNmspce:
           newObj = i + ":" + obj
           if mc.objExists(newObj):
               print ("Found newObj:   %s" % (newObj))
               return newObj
           else:
               mc.warning("No obj found named %s. Returning original input value of %s" % (newObj, obj))
               return obj

def Error(obj):
    mc.warning("No object found named %s, unable to add to render layer" % (obj))

            
def AddOverrides(visibilityCtrl, ref, AttrName, value):
    if mc.objExists(visibilityCtrl):
        oOverride = ref.createOverride(AttrName, override.AbsOverride.kTypeId)
        plug = visibilityCtrl + "." + AttrName
        oOverride.setAttributeName(plug)
        oOverride.finalize(plug)
        oOverride.setAttrValue(value)
    else:
        mc.warning("%s does not exist, please make sure the body rig is imported. Skipping AddOverrides()" % (visibilityCtrl))

def SmoothAllVisibleMeshes():
    meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True) 
    for i in meshes:  
        smooth = i + ".displaySmoothMesh"
        if mc.objExists(smooth):
            mc.setAttr(smooth, 2)
    
#aiVisible()
#ApplyRenderSettings()
#CreateDefaultRenderLayers('Skin_RL', 'skinCollection')
#CreateDefaultRenderLayers('Skel_RL', 'skelCollection')
#SmoothAllVisibleMeshes()

#create prep menu window
    #create render layers
    #create apply render settings (list/save settings)
    #smooth/unsmooth all meshes
    #turn off aivisibility ( selection or scene)
    
    