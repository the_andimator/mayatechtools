import maya.cmds as mc
import os.path
from os.path import expanduser
import maya.mel as mel
import pymel.core as pm
import sys as sys
from functools import partial as ftool
import shutil as shutil
from os import listdir
import os

from Qt import QtWidgets, QtCore, QtGui
from Qt.QtWidgets import QWidget, QApplication
from Qt import QtCompat
from Qt.QtCore import QMimeData
from Qt.QtGui import QCursor 
import Qt
import sip

import filecmp
import UpdateLibrary as customUI

def maya_main_window():
    main_window_ptr = omui.MQtUtil.mainWindow()
    return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, parent = None):
        super(MainWindow, self).__init__()
        self.setWindowFlags(QtCore.Qt.Tool)
        global main_widget
        global OLL_window
        global WindRef
        WindRef = self
        main_widget = self.ui =  customUI.Ui_UpdateLibrary()
        main_widget.setupUi(self)
        self.setObjectName('UpdateMVTLib')
        self.setWindowTitle("UpdateMVTLib")
        self.setWindowFlags(QtCore.Qt.BypassGraphicsProxyWidget|
                            QtCore.Qt.WindowCloseButtonHint|
                            QtCore.Qt.Dialog)
        self.form_widget = UpdateMVTLibrary(self)
        self.form_widget.AssignToUI()

class UpdateMVTLibrary(QtWidgets.QMainWindow):
    #class variables
    windowName = "UpdateLibrary"

    def __init__(self, parent):
        try:
			mc.deleteUI(windowName, window = True)
        except:
			pass
        #path variables
        testAssets = "3D_MVT_ASSETS"
        proMax = os.path.join("Z:/", "ASSETS/", testAssets)
        assetLibrary = "MVT_Assets_Library"
        backupLibrary = '_mvt_library_backup'
        backupLoc = proMax + '/' + backupLibrary

        USER = os.environ['HOME']
        fullMayaPath = "%s\maya\projects/%s" % (USER, assetLibrary)

        uifile = "\maya/2018/ui\UpdateLibrary.py"
        ui_filename =  os.path.expanduser('~') + uifile

        foundLocalPath = False
        foundProPath = False

        self.proMax = os.path.join(proMax, assetLibrary)
        self.fullMayaPath = fullMayaPath
        self.backupLoc = backupLoc
        self.completed = completed = 0
        
        self.localUnique = localUnique = []
        self.localUniqueNice = localUniqueNice = []
        self.promaxUnique = promaxUnique = []
        self.promaxUniqueNice = promaxUniqueNice = []
        self.localChanges = localChanges = []
        self.localChangesNice = localChangesNice = []
        self.localSwatches = localSwatches = []

        #print (r"Z:/ASSETS/3D_MVT_ASSETS")
        print (proMax)
        
        print os.access(proMax, os.F_OK)
        print os.access(proMax, os.R_OK)
        print os.access(proMax, os.W_OK)
        print os.access(proMax, os.X_OK)

        
        #os.chmod(proMax, 0444)
        
        if os.path.exists(proMax):
            self.foundProPath = True
        else:
            mc.warning("ProMax path is not found. Make sure that you are connected to ProMax")
            
        main_widget.label.setText("Not Started")
            
    def AssignToUI(self):
        main_widget.progressBar.setMinimum(0)
        main_widget.progressBar.setMaximum(100)
        main_widget.progressBar.setValue(0)
        main_widget.pullLatest.clicked.connect(lambda *_: self.PullFromMVT())
        main_widget.pushLatest.clicked.connect(lambda *_: self.PushToMVT())

    #Progress Bar Functions
    def ResetProgress(self, message):
        main_widget.label.setText(message + "Not Started")
        self.completed = 0
        main_widget.progressBar.setValue(self.completed)

    def StartTransferProgress(self, val):
        self.completed += val
        main_widget.progressBar.setValue(self.completed)
        
    def ChangeText(self, message, replace):
        if replace == False:
            main_widget.label.setText(message + "In Progress")
        else:
            main_widget.label.setText(message)
            
    def CompleteBar(self, message):
        main_widget.label.setText(message + "Complete")
    
        
    def PullFromMVT(self):
        self.ChangeText("Starting up...", True)

        if main_widget.newBackup.isChecked() == True:
            self.CreateBackupFolder(self.backupLoc)
        
        if main_widget.rebuild.isChecked() == True:
            if os.path.exists(self.proMax):
                #mc.warning("Path exists")
                #mc.warning(self.proMax)
                self.ChangeText("Deleting previous local library...", True)
                self.DeleteDirectory(self.fullMayaPath)
                
                message = "Rebuilding Library from Promax:     "
                self.Copy(self.proMax, self.fullMayaPath, message, "Pull")
            #If pulling
            #   Confirm directories exist
            #   Iterate through folders. If iterator finds files with same name, copy file from source and replace at destination
            #   If iterator comes across file or folder that is not in destination folders, copy from source to destination
            print ''
        else:
            #find unique files on local computer
            message = "Updating Library from Promax:     "
            self.FindUniqueFiles()
            
            if len(self.promaxUniqueNice) != 0:
                #send list of asset dir to new function to be sorted
                self.Copy(self.promaxUnique, self.fullMayaPath, message, "Pull")
            if len(self.localSwatches) > 0:
                    #get swatches directory for all Promax folders, store in list
                    #self.proMax             ##C:/Users/Andy-BP/Documents/maya/projects/MVT_Assets_Library/
                    promaxDir = os.listdir(self.proMax)
                    promaxSwatches = []
                    
                    for x in promaxDir:
                        #mc.warning(x)
                        #mc.warning(os.path.join(self.proMax, x))
                        newPath = os.path.join(self.proMax, x)
                        promaxSwatches.append(os.path.join(newPath, ".mayaSwatches"))
                    
                    
                    #mc.warning("promaxDir:    %s" % promaxDir)
                    #mc.warning("promaxSwatches:    %s" % promaxSwatches)
                    #mc.warning("maya path:    %s" % self.fullMayaPath)

                    #parent = os.path.abspath(os.path.join(sourceSwatches, os.pardir)) #C:/Users/Andy-BP/Documents/maya/projects/MVT_Assets_Library/Teeth/ OR Z:\ASSETS\3D MVT ASSETS\MVT_Assets_Library\Teeth\
                    #parentfolderName = os.path.basename(parent) #Teeth/

                    #os.listdir(self.local)
                    
                    #get local path that ends in MVT_Assets_Library
                    
                    self.UpdateSwatches(promaxSwatches, self.fullMayaPath, message, "Pull")
                    
                    self.ResetProgress("")
                    self.ChangeText("Complete!", True)
                    
            else:
                self.ChangeText("Local library is already up to date! ", True)
        
    def PushToMVT(self):
        import CopyToLocation as copy
        message = message = "Pushing from MVT:     "
        #self.CopyToLoc()
        
        self.FindUniqueFiles()
        mc.warning(self.localChangesNice)
        mc.warning(self.proMax)
        mc.warning(self.localSwatches)
        
        self.ChangeText("Processing...", False)
        
        if len(self.localUniqueNice) > 0 or len(self.localChangesNice) > 0 or len(self.localSwatches) > 0:
            #Create dialog window, display new assets, ask if user wants to push assets to mvt
            result = mc.confirmDialog(
            title="Found Unique Files",
            icon = "warning",
            message="New Assets to Push to Promax: \n%s\n\nEdited assets to push to Promax:\n%s\n\n\nSwatches To Push:\n%s\n\n" % (self.localUniqueNice, self.localChangesNice, self.localSwatches),
            messageAlign = 'left',
            button=['Push Changes', "Cancel"],
            defaultButton="Cancel",
            cancelButton='Cancel',
            dismissString='Cancel',
            bgc = [.25,.2,.2])

            if result == 'Push Changes':
                message = "Sending updates to Promax:     "
                if len(self.localUnique) > 0:
                    self.Copy(self.localUnique, self.proMax, message, "Push")
                if len(self.localChanges) > 0:
                    self.Copy(self.localChanges, self.proMax, message, "Push")
                message = "Updating Swatches.."
                if len(self.localSwatches) > 0:
                    #mc.warning("self.localSwatches:    \n%s" % (self.localSwatches))
                    #mc.warning("self.proMax:    \n%s" % (self.proMax))
                    self.UpdateSwatches(self.localSwatches, self.proMax, message, "Push")
                    
            self.ResetProgress("")
            self.ChangeText("Complete!", True)
        else:
            mc.warning ("Local library matches Promax. No Unique assets.")
            self.ResetProgress("")
            self.ChangeText("Local library matches Promax. No Unique assets.", True)
            
    def CreateJSON(self):
        #if favs.json is being updated:
        #   find favs.json in directory
        #   open up file
        #   replace username in file with local comp username
        #   check that the directories in json file exist
        print ''

    def CreateBackupFolder(self, d):
        import datetime
        now = datetime.datetime.now()
        curDate = now.strftime("%m%d%y  %H%M")
        print curDate
        
        dst = d + "/" + curDate
        #Create Backup Folder
        message = "MVT backup:     "
        #confirm backup folder exists
        self.Copy(self.proMax, dst, message, None)
        
        print ''
            
    def DeleteDirectory(self, src):
        mc.warning("Deleting directory at:  %s" % src)
        if os.path.isdir(src):
            shutil.rmtree(src)
            
    def Copy(self, src, dst, message, direction):
        
        self.ResetProgress(message)
        self.ChangeText(message, False)
        #get list of names from source

        #determine if names are paths or files
        if len(src) > 0:
            if os.path.isfile(src[0]):
                #find increment values and # of assets
                allAssets = len(src)
                incrValue = 100 / float(allAssets * 1.00)
                
                #copy individual file
                for assetPath in src:
                    if os.path.exists(assetPath):
                        #Get parent directory path for src asset
                        absPath = os.path.abspath(os.path.join(assetPath, os.pardir))
                        #Get folder name for asset location (folder name from mvt asset path)
                        parentfolderName = os.path.basename(absPath)
                        #Get asset name
                        uniqueAssetName = os.path.basename(assetPath)
                        if direction == "Pull":
                            #Create path to local mvt library
                            localibPath = os.path.join(self.fullMayaPath, parentfolderName)
                        if direction == "Push":
                            #Create path to promax mvt library
                            localibPath = os.path.join(self.proMax, parentfolderName)
                            
                        #mc.warning("absPath:    %s" % absPath)
                        #mc.warning("parentfolderName:    %s" % parentfolderName)
                        #mc.warning("uniqueAssetName:    %s" % uniqueAssetName)
                        #mc.warning("localibPath:    %s" % localibPath)

                        #Confirm path exists
                        if os.path.exists(localibPath):
                            #confirm location on mvt
                            finalPath = os.path.join(localibPath, uniqueAssetName)
                            mc.warning("Final Path:    %s" % finalPath)
                            
                            if os.path.isfile(assetPath):
                                if os.path.isdir(dst):
                                    shutil.copy2(assetPath, finalPath)
                                    
                            if os.path.isfile(finalPath):
                                self.ChangeText("%s copied successfully!" % uniqueAssetName, True)
                                
                            self.StartTransferProgress(incrValue)
            else:
                #copy tree directory
                #create directory at destination
                os.makedirs(dst)
                    
                names = os.listdir(src)

                #find increment values and # of assets
                allAssets = len(names)
                incrValue = 100 / float(allAssets * 1.00)
        
                for name in names:
                    srcname = os.path.join(src, name)
                    dstname = os.path.join(dst, name)

                    if os.path.islink(srcname):
                        linkto = os.readlink(srcname)
                        os.symlink(linkto, dstname)
                    elif os.path.isdir(srcname):
                        shutil.copytree(srcname, dstname, False, None)

                    self.StartTransferProgress(incrValue)
                
        self.CompleteBar("Task ")
        
    def UpdateSwatches(self, src, dst, message, direction):
        self.ResetProgress(message)
        self.ChangeText(message, False)
        
        #find increment values and # of assets
        allAssets = len(src)
        incrValue = 100 / float(allAssets * 1.00)

        #list destination folder basenames
        dstFolders = os.listdir(dst) 
        
        for sourceSwatches in src:
            #Get parent folder of swatches
            parent = os.path.abspath(os.path.join(sourceSwatches, os.pardir)) #C:/Users/Andy-BP/Documents/maya/projects/MVT_Assets_Library/Teeth/ OR Z:\ASSETS\3D MVT ASSETS\MVT_Assets_Library\Teeth\
            parentfolderName = os.path.basename(parent) #Teeth/
            
            #get swatches folder
            swatchesFolder = os.path.basename(sourceSwatches) #.mayaSwatches/

            #Create dst path
            if direction == "Push":
                dstFolder = os.path.join(self.proMax, os.path.join(parentfolderName, swatchesFolder)) #Z:\ASSETS\3D MVT ASSETS\MVT_Assets_Library\ ( Teeth\ + .mayaSwatches\ ) OR C:/Users/Andy-BP/Documents/maya/projects/MVT_Assets_Library/ ( Teeth\ + .mayaSwatches\ ) 
            elif direction == "Pull":
                dstFolder = os.path.join(self.fullMayaPath, os.path.join(parentfolderName, swatchesFolder))
            #check if paths are valid
            if os.path.isdir(dstFolder) != False:
                shutil.rmtree(dstFolder)
            
            shutil.copytree(sourceSwatches, dstFolder, False, None)
            self.StartTransferProgress(incrValue)



    def FindUniqueFiles(self):
        top_mvt = os.listdir(self.proMax) #promax to list (of lists)
        top_local = os.listdir(self.fullMayaPath) #local list (of lists)
        self.localChangesNice[:] = [] #clear nice list so duplicate items don't pile up
        self.localSwatches[:] = [] #clear nice list so duplicate items don't pile up
        unique_to_local = []
        unique_to_promax = []
        updated_local_assets = []
        
        for local, promax in zip(top_mvt, top_local):
            local_temp = os.listdir(self.fullMayaPath + "/" + local)
            mvt_temp = os.listdir(self.proMax + "/" + promax)
            #mc.warning(mvt_temp)
            #mc.warning(local_temp)
            
            createLocalList = [x for x in local_temp if x not in mvt_temp]
            createPromaxList = [x for x in mvt_temp if x not in local_temp]
            
            updatedLocal = [x for x in local_temp if x in mvt_temp]
            updatedPromax = [x for x in mvt_temp if x in local_temp]
            
            if createLocalList != []:
                for i in createLocalList:
                    itemPath = self.fullMayaPath + "/" + local + "/" + i
                    unique_to_local.append(itemPath)
                    self.localUniqueNice.append(i)

            if createPromaxList != []:
                for i in createPromaxList:
                    promaxitemPath = self.proMax + "/" + promax + "/" + i
                    unique_to_promax.append(promaxitemPath)
                    self.promaxUniqueNice.append(i)

            for localEdit, promaxEdit in zip(updatedLocal, updatedPromax):
                #create path to item
                locPath = itemPath = self.fullMayaPath + "/" + local + "/" + localEdit
                proPath = promaxitemPath = self.proMax + "/" + promax + "/" + promaxEdit

                if filecmp.cmp(locPath, proPath) == False:
                    if os.path.isfile(locPath):
                        updated_local_assets.append(locPath)
                        self.localChangesNice.append(localEdit)
                    else:
                        #This stores folder paths (specifically used for the .swatches folder)
                        self.localSwatches.append(locPath)

        if  len(updated_local_assets) != 0:
            mc.warning("Found local changes that should be pushed!\nPath: %s\nObject Name: %s" % (updated_local_assets, self.localChangesNice))

        self.localChanges = updated_local_assets
        self.promaxUnique = unique_to_promax
        self.localUnique = unique_to_local
        #mc.warning("unique_to_local  %s" % unique_to_local)
        #mc.warning("unique_to_promax  %s" % unique_to_promax)

 