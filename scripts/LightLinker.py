import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
from functools import partial
import maya.app.renderSetup.model.override as override
import maya.app.renderSetup.model.collection as collection
import maya.app.renderSetup.model.renderLayer as RL
import maya.app.renderSetup.model.renderSetup as renderSetup
import json
import sys
import RenderPrep as RP
import maya.api.OpenMayaRender  as omr
import maya.api.OpenMayaUI  as omui
import maya.api.OpenMaya  as om



class LightLinkMenu(object):

    global prepWin
    global toolsetVer
    global renderLayerWin
    renderLayerWin = ''
        
    toolsetVer = "01.00" 
    
    def __init__(self):
        try:
            mc.deleteUI(dockable, window = True)
            mc.deleteUI(prepWin, window = True)
        except:
            pass
                
        self.windowName = windowName = "Light_Linker"
        self.dockable = dockable = "Light_Linker"
        
        CONST_HDRI_MAP = "HDRI_Refl_LightShape"
        self.CONST_HDRI_MAP = CONST_HDRI_MAP    
            
        CONST_Default_MAP = "Default_LightShape"
        self.CONST_Default_MAP = CONST_Default_MAP
        
        if mc.window(self.windowName, exists = True):
            mc.deleteUI(self.windowName)
            
        if mc.window(self.dockable, exists = True):
            mc.deleteUI(self.dockable)
            
##############################################################################################
#################################___________UI___________#####################################  
##############################################################################################

    def BuildUI(self, windowName):
        
        try:
            mc.deleteUI(self.windowName, window = True)
        except:
            pass
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "BP_ToolShelf"
        try: 
            mc.deleteUI(WinName)
        except:
            pass   
        prepWin = mc.window(self.windowName, w =750, h = 450, mnb = True, mxb = True, s = 1, rtf = True)
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 750, h= 450)

        #Create menu items
        menuBarLayout = mc.menuBarLayout()
        mc.menu( label='File' )
        mc.menuItem( label='BP ToolKit', i = "icon_bpLogo_small.png", c = lambda *_: OpenRenderSequencer() )
        mc.menuItem( label='Render Sequencer', i = "icon_bpLogo_LL.png", c = lambda *_: OpenRenderSequencer() )
        mc.menuItem( label='Open Reference Editor', i = "workflow_30.png", c = lambda *_: OpenReferenceEditor() )
        mc.menu( label='Version', helpMenu=True )
        mc.menuItem( label='Ver:  ' + toolsetVer )

        ###Create Border and Frames####
        mc.separator( style='in', width=750)
        headerRowColumn = mc.rowColumnLayout("header", nc = 1, cw = [(1,750)], columnOffset = [(1, "both", 2), (2, "both", 2)])

        #Find Header Image
        headerPath = mc.internalVar(userBitmapsDir=True)
        mc.image( image=headerPath + 'icon_bpLogo_LL.png')

        mc.frameLayout('Light Link Sets',parent = prepWin, label = "Light Link Sets", cll = True, bv = False)
        mc.rowColumnLayout("LightLinkMaster",  nc = 2, cw = [(1,375),(2,375)])
        ###############################################################
        ##############________HDRI List________#############
        ###############################################################
        ###__Light Link Metal Materials___###
        mc.rowColumnLayout("HDRI_Text",  nc = 2, cw = [(1,40),(2,260)])
        mc.text(label = "HDRI:", align = 'left', font = "boldLabelFont")
        self.CONST_HDRI_MAP = mc.textField("self.CONST_HDRI_MAP", text = self.CONST_HDRI_MAP) 
        mc.text(label = "", align = 'left')
        hdriScroll = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindMaterialList(), parent = "HDRI_Text" )
        pm.iconTextScrollList(hdriScroll, edit = True, dcc = lambda *args: self.RenameShaderGroups(hdriScroll))
        mc.textField(self.CONST_HDRI_MAP, edit=True)
        HDRI_Input = self.CONST_HDRI_MAP
        mc.text(label = "", align = 'left')
        columnFour= mc.rowColumnLayout("HDRI_LL",  nc = 5, cw = [(1,101),(2,53), (3,33), (4,33), (5,33), (6,33), (7,33)])
        hdriAdd = mc.button(label = "Select All", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(hdriScroll))
        mc.text(label = "", align = 'left')
        hdriAdd = mc.button(label = "Add", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(hdriScroll))
        hdriRemove = mc.button(label = "Remove", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(hdriScroll))
        reficon = headerPath + "icon_refresh.png"
        RefreshList = mc.symbolButton(w = 23, h = 25, image = reficon, ebg = True, c = lambda *args: self.GenerateMaterialList(hdriScroll))
        mc.setParent("LightLinkMaster")
        
        mc.rowColumnLayout("Default", nc = 2, cw = [(1,40),(2,260)])
        mc.text(label = "Default:", align = 'left', font = "boldLabelFont")
        self.CONST_Default_MAP = mc.textField("self.CONST_Default_MAP", text = self.CONST_Default_MAP) 
        mc.text(label = "", align = 'left')
        hdriScroll = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindMaterialList(), parent = "Default" )
        pm.iconTextScrollList(hdriScroll, edit = True, dcc = lambda *args: self.RenameShaderGroups(hdriScroll))
        mc.textField(self.CONST_Default_MAP, edit=True)
        HDRI_Input = self.CONST_Default_MAP
        mc.text(label = "", align = 'left')
        columnFour= mc.rowColumnLayout("HDRI_LL",  nc = 5, cw = [(1,101),(2,53), (3,33), (4,33), (5,33), (6,33), (7,33)])
        hdriAdd = mc.button(label = "Select All", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(hdriScroll))
        mc.text(label = "", align = 'left')
        hdriAdd = mc.button(label = "Add", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(hdriScroll))
        hdriRemove = mc.button(label = "Remove", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(hdriScroll))
        reficon = headerPath + "icon_refresh.png"
        RefreshList = mc.symbolButton(w = 23, h = 25, image = reficon, ebg = True, c = lambda *args: self.GenerateMaterialList(hdriScroll))
        mc.text(label = "", align = 'left')
        mc.setParent("LightLinkMaster")
        mc.setParent("..")

        mc.frameLayout('Unused',parent = prepWin, label = "Unused", cll = True, bv = False)
        mc.rowColumnLayout("UnassignedObj",  nc = 2, cw = [(1,450),(2,375)])
        
        ###############################################################
        ##############________SceneShaderGroupList________#############
        ###############################################################
        ###__Light Link Metal Materials___###
        mc.rowColumnLayout("LightLinkCol",  nc = 2, cw = [(1,40),(2,650)])
        mc.text(label = "", align = 'left')
        mc.text("Scene Shader Group List", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text("Double Click To Rename", align = 'left', font = "smallObliqueLabelFont")
        mc.text(label = "", align = 'left')

        #layout = pm.horizontalLayout(ratios=[5,0], spacing=10)
        mc.rowColumnLayout("LightLinkCol2",  nc = 2, cw = [(1,350),(2,400)])
        llmm = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindMaterialList(), parent = "LightLinkCol2")
        pm.iconTextScrollList(llmm, edit = True, dcc = lambda *args: self.RenameShaderGroups(llmm))

        mc.text(label = "", align = 'left')
        columnFive = mc.rowColumnLayout("LightLinkCol4",  nc = 1, cw = [(1,350)], parent = "LightLinkCol2")
        SelectObjectsOf = mc.button(label = "Select Meshes Using Materials", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(llmm))
        reficon = headerPath + "icon_refresh.png"
        RefreshList = mc.symbolButton(w = 23, h = 25, image = reficon, ebg = True, c = lambda *args: self.GenerateMaterialList(llmm))        
        
        mc.rowColumnLayout("LightLinkCol44",  nc = 2, cw = [(1,40),(2,650)])
        mc.text(label = "", align = 'left')
        mc.text("Scene Shader Group List", align = 'left')
        mc.text(label = "", align = 'left')
        mc.text("Double Click To Rename", align = 'left', font = "smallObliqueLabelFont")
        mc.text(label = "", align = 'left')

        #layout = pm.horizontalLayout(ratios=[5,0], spacing=10)
        mc.rowColumnLayout("LightLinkCol3",  nc = 2, cw = [(1,350),(2,400)])
        llmm = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindMaterialList(), parent = "LightLinkCol3")
        pm.iconTextScrollList(llmm, edit = True, dcc = lambda *args: self.RenameShaderGroups(llmm))

        mc.text(label = "", align = 'left')
        columnFive = mc.rowColumnLayout("LightLinkCol46",  nc = 1, cw = [(1,350)], parent = "LightLinkCol2")
        SelectObjectsOf = mc.button(label = "Select Meshes Using Materials", align = "center" , c = lambda *args: self.SelectItemsWithMaterial(llmm))
        reficon = headerPath + "icon_refresh.png"
        RefreshList = mc.symbolButton(w = 23, h = 25, image = reficon, ebg = True, c = lambda *args: self.GenerateMaterialList(llmm))
        mc.setParent("..")

        
        
        

        
        if mc.window(WinName, exists = True):
            mc.deleteUI(WinName)
        mc.showWindow()

##############################################################################################
##########################___________   FUNCTIONALITY___________##############################   
##############################################################################################     
    def FindMaterialList(self):
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)   
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')
        optimizedGrp = []
        refresh = shadingGrps
        
        if shadingGrps != None:
            for x in shadingGrps:
                if optimizedGrp != []:
                    if x not in optimizedGrp:
                        print ("%s appended to optimized list" % (x))
                        optimizedGrp.append(x)  
                else:
                    optimizedGrp.append(x)
      
        return optimizedGrp
            
    def GenerateMaterialList(self, llmm):
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)    
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')
        #return shadingGrps
        llmm.removeAll()
        pm.iconTextScrollList(llmm, edit = True, allowMultiSelection=True)
        
        optimizedGrp = []
        refresh = shadingGrps

        if shadingGrps != None:
            for x in shadingGrps:
                if optimizedGrp != []:
                    if x not in optimizedGrp:
                        optimizedGrp.append(x)
                        llmm.append(x)  
                else:
                    optimizedGrp.append(x)
                    llmm.append(x)
        else:
            mc.warning("No shading groups in scene that are attached to objects. Ignoring shading group refresh.")
                
    def CreateHDRILightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        #HDRI = self.FindLights("HDRI")
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        
        selectedMat = materials.getSelectItem()
        hdrObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        if hdrObj != None:
            for sel in hdrObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 0, light = hdr, object = sel)
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 1, light = lights, object = sel)

        
        
    def CreateDefaultLightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        
        selectedMat = materials.getSelectItem()
        defObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        
        
        if defObj != None:
            for sel in defObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 1, light = hdr, object = sel)
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 0, light = lights, object = sel)
        else:
            mc.warning("No lights were found in scene. Unable to Light Link")

    def ConnectAllLightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        #HDRI = self.FindLights("HDRI")
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        selectedMat = materials.getSelectItem()
        defObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        if defObj != None:
            for sel in defObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 0, light = hdr, object = sel)
                
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 0, light = lights, object = sel)
        else:
            mc.warning("No lights were found in scene. Unable to Light Link")        
                
    def FindLights(self, sendBack):
        print ''
        #create array of all lights from scene
        grabAllLights = mc.ls(type = 'light', dag = True)
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)
        
        #Returnable Variables
        sceneLights = []
        HDRI = []
        AllLights = []
        
        metallicShaders = []
        nonMetallicShaders = []
        nonMetalObjects = []
   
        #sort lights into separate arrays (HDRI vs everything else)
        for i in grabAllLights:
            if i != self.CONST_HDRI_MAP:
                sceneLights.append(i)
                AllLights.append(i)

        for x in grabAllGeometry:
            if x == self.CONST_HDRI_MAP:
                HDRI.append(x)
                AllLights.append(x)

        grabAllGeometry = mc.ls(type = 'geometry', dag = True)    
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')

        #Make list of metallic shaders in specific material
        if shadingGrps != None:
            for mat in shadingGrps:
                    nonMetallicShaders.append(mat)
        else:
            mc.warning("No HDRI lights were found in scene. Unable to Link HDRI")
        #Make list of objects that have specific material connections

        metalObjects = mc.listConnections(metallicShaders, t = 'mesh', scn = True)
        
        nonMetalObjects = []
        nonMetalObjects = mc.listConnections(nonMetallicShaders, t = 'mesh', scn = True)  

        if sendBack == "HDRI":
            print ''
            return HDRI
        if sendBack == "Default":
            print ''
            return sceneLights
        if sendBack == "All":
            print ''
            return AllLights
            
    def SelectItemsWithMaterial(self, materials):      
        #create array of all lights from scene
        HDRI = []
        metallicShaders = []
        nonMetallicShaders = []
        nonMetalObjects = []
        
        mat =  materials.getSelectItem()
        
        grabAllGeometry = mc.listConnections(mat, type='mesh', scn = True)
        print grabAllGeometry
        mc.select(grabAllGeometry)
    
    def RenameShaderGroups(self, rsg):
        if rsg.getSelectItem() != None:
            oldName = "".join(rsg.getSelectItem())
            result = mc.promptDialog(
            title='',
            message='Rename Shader Group',
            text = oldName,
            button=["Rename", "Generate from Material",'Cancel'],
            defaultButton="Create",
            cancelButton='Cancel',
            dismissString='Cancel')

            if result == "Rename":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.rename(oldName, newName)
                self.GenerateMaterialList(rsg)
                
            if result == "Generate from Material":
                newName = self.GenerateNameForMat(oldName)
                mc.rename(oldName, newName)
                self.GenerateMaterialList(rsg)
        else:
            mc.warning("Nothing is selected, unable to rename quick select set.")  