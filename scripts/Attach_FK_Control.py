import maya.cmds as mc
import pymel.core as pm

class Attach_FK_Control():

    #step 1: select ctrl
    #step 2: select joint
    #step 3: run script
    
    global prepWin
    global renderLayerWin
    
    renderLayerWin = ''
    
    def __init__(self):
        try:
            mc.deleteUI(windowName, window = True)
        except:
            pass
            
        OrientControl = "orientControl"
        PointControl ="pointControl"
        searchField = ""
        replaceField = ""
        
        #Class variables
        self.OrientControl = OrientControl
        self.PointControl = PointControl
        self.searchField = searchField
        self.replaceField = replaceField
                
            
    def BuildUI(self, windowName):
        
        WinName = windowName
        print WinName
        
        try:
             mc.deleteUI(WinName, window = True)
        except:
            pass


        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        
        dWin = mc.window(windowName, title = windowName, wh=(325,256), sizeable = False, rtf = True)
        mc.columnLayout(cat=('both', 0), rowSpacing=5, columnWidth=275, cal = "left", co = ("left",100))
        mc.text(label = "Search and replace:")

        self.searchField = mc.textFieldGrp("search", label='Search for:', pht='control name' , text='Ctrl_')
        self.replaceField = mc.textFieldGrp("replace", label='Replace with:', pht='group name', text='Grp_', w = 256)
        mc.checkBox(self.OrientControl, label = "Orient Constrain", v = True)
        mc.checkBox(self.PointControl, label = "Point Constrain", v = False)
        pm.button(label = "Orient Control", command = lambda *args: self.CreateFK())
        mc.showWindow(dWin)


    def CreateFK(self):
        selList = mc.ls(sl = True, transforms = True)
        
        if len(selList) != 2:
            print 'invald number of selections. Check selected items and try again.'
        else:
            bindControl = mc.filterExpand( sm=9 )[0]
            bindJoint = mc.ls(sl = True, type = "joint")
            
            #check if duplicate objects exist in a scene
            check = mc.ls(bindControl)
            if len(check) > 1:
                mc.error("multiple objects with the same name")
                return
            
            groupRef = self.renamingNewGrp(bindControl)
            self.moveToJoint(groupRef, bindControl, bindJoint)

    def renamingNewGrp(self, oldName):
        #store name in string variable and replace text
        #selList = mc.ls(selection = True)
        print("THIS IS oldName:   " + oldName)
        print("THIS IS self.searchField.lower():   " + pm.textFieldGrp(self.searchField, q=1, text=1))
        print("THIS IS self.replaceField.lower():   " + pm.textFieldGrp(self.replaceField, q=1, text=1))
        
        #search and replace names
        newName = oldName
        newName = oldName.replace(pm.textFieldGrp(self.searchField, q=1, text=1), pm.textFieldGrp(self.replaceField, q=1, text=1))
        newName = newName.replace("|", '')
        print("THIS IS newName:   " + newName)
        #create empty group and name
        groupRef = mc.group(em = True, n = newName)
        print("THIS IS GROUP:   " + groupRef)
        return groupRef
        
        #self.moveToJoint(groupRef, selList[0], selList[1])
    

    def moveToJoint(self, groupRef, ctrl, joint):
        #parent grp to joint
        mc.parent(groupRef, joint)
        #move grp to joint position and zero out transforms
        self.zeroOutTranslate(groupRef)
        self.zeroOutRotate(groupRef)
        
        #unparent from joint
        mc.Unparent(groupRef, joint)
        
        mc.parent(ctrl, groupRef)
        #self.zeroOutTranslate(ctrl)
        
        #Freeze transforms
        mc.FreezeTransformations(ctrl)
        
        orient = mc.checkBox(self.OrientControl, q= True, v= True, changeCommand = True)
        point = mc.checkBox(self.PointControl, q= True, v= True, changeCommand = True)      
        
        if orient == True:
            pm.orientConstraint(ctrl, joint, mo = False)
        if point == True:
            pm.pointConstraint(ctrl, joint, mo = False)

       
    def zeroOutTranslate(self, groupRef):
        name = groupRef.replace("|", "")
        print name
        pm.setAttr(str(name) +".translateX", 0)
        mc.setAttr(name +".translateY", 0)
        mc.setAttr(name +".translateZ", 0)
        
    def zeroOutRotate(self, groupRef):
        name = groupRef.replace("|", "")
        mc.setAttr(name+".rotateX", 0)
        mc.setAttr(name+".rotateY", 0)
        mc.setAttr(name+".rotateZ", 0)
        
        
_repeat_function = None
_args = None
_kwargs = None

def repeat_command():

    if _repeat_function is not None:
        _repeat_function(*_args, **_kwargs)

    def wrap(function):
        def wrapper(*args, **kwargs):

            global _repeat_function
            global _args
            global _kwargs

            _repeat_function = function
            _args = args
            _kwargs = kwargs

            commandToRepeat = ('python("%s.repeat_command()")' % __name__)

            ret = function(*args, **kwargs)

            pmc.repeatLast(addCommand=commandToRepeat, addCommandLabel=function.__name__)

            return ret
        return wrapper
    return wrap


'''
To Create a new shelf button, use this block of code:
    
import Attach_FK_Control as FK
reload (FK)

WinName = "Attach_FK_Control"

try:
	mc.deleteUI(WinName, window = True)
except:
	pass

build = FK.Attach_FK_Control()
build.BuildUI(WinName)

'''
