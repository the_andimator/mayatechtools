import maya.cmds as mc

def SetOpaque():
    sel = mc.ls(sl=True, type = 'mesh', dag=True, long=True)
    print sel
    for i in sel:
        print i + ": Opaque set to false."
        mc.setAttr(i +'.aiVisibleInDiffuse', 0)
        mc.setAttr(i +'.aiVisibleInGlossy', 0)
        