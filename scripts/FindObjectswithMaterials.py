import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm

    
class FindObjectswithMaterials(object):
    

    
    def BuildUI(self, windowName):
        
        toolsetVer = ".001"
        headerPath = mc.internalVar(userBitmapsDir=True)
        reficon = headerPath + "icon_refresh.png"
        
        try:
             mc.deleteUI(windowName, window = True)
        except:
            pass
        
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "Material_Light_Linker"
        
        try: 
            mc.deleteUI(WinName)
        except:
            pass
            
        prepWin = mc.window(windowName, w =750, h = 600, mnb = True, mxb = True, s = 1, rtf = True)
        
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)
            
        #Create menu items
        menuBarLayout = mc.menuBarLayout()
        mc.menu( label='File' )
        mc.menuItem( label='Refresh', i = "icon_rendersequence.png", c = lambda *_: OpenRenderSequencer() )
        mc.menuItem( label='Open Reference Editor', i = "workflow_30.png", c = lambda *_: OpenReferenceEditor() )

        mc.menu( label='Version', helpMenu=True )
        mc.menuItem( label='Ver:  ' + toolsetVer )
        
        #Find Header Image
        ###Create Border and Frames####

        mc.frameLayout(toolsetVer, parent = prepWin, label = "", cll = False, bv = False)
        headerPath = mc.internalVar(userBitmapsDir=True)
        mc.image( image=headerPath + 'icon_bpLogo.png')
        mc.separator( style='in', width=750)
  
        #mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 750, h= 600)
        
        #Create Light Groups lists
        
        #HDRI
        
        #Normal Lights

        #Create menu items
        menuBarLayout = mc.menuBarLayout()
        ###__Light Link Metal Materials___###
       # LightLinkerFrame = mc.frameLayout("LightLinkMetalMaterials", parent = prepWin,label = "Light Link Metal Materials", cll = False, bv = False)
       ## LightLinkColumn = mc.rowColumnLayout("HDRI_Text",  nc = 2, cw = [(1,100),(2,260)])
       # mc.text(label = "HDRI Map Name:", align = 'left', font = "boldLabelFont")
       # self.CONST_HDRI_MAP = mc.textField("self.CONST_HDRI_MAP", text = "")
       # mc.setParent("..")
        
        #LightLinkColumn = mc.rowColumnLayout("LightLinkCol",  nc = 1, cw = [(1,300)])

        #mc.text("Scene Object List:", align = 'left')
       # mc.text("Double Click To Rename", align = 'left', font = "smallObliqueLabelFont")
        #mc.setParent("..")   
        
        LightLinkingGroup = mc.rowColumnLayout("LightLinkingGroup",  nc = 10, cw = [(1,40),(2,130),(3,50),(4,130),(5,100),(6,100),(7,100),(8,100),(9,100),(10,100)], h = 425)
        mc.text(l="")
        mc.text(l="HDRI", align = 'right')
        mc.text(l="")
        mc.text(l="Default", align = 'right')
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        llmm = pm.iconTextScrollList(allowMultiSelection=True, append = self.FindMaterialList(), h = 300)
        mc.text(l="")
        pm.iconTextScrollList(allowMultiSelection=True, append = self.FindMaterialList(), h = 300)
        mc.setParent("..")
        
        mc.rowColumnLayout("LightLinkingButtons",  nc = 6, cw = [(1,40),(2,130),(3,50),(4,50),(5,50),(6,50)], h = 425)
        mc.text(l="")
        mc.button(label = "Apply to Selected", align = "center", c =  lambda *_: self.SetArnoldAttributes(False))
        
        #mc.setParent("..") 
        #pm.iconTextScrollList(llmm, edit = True, dcc = lambda *args: self.SelectItemsWithMaterial(llmm))
        
        #mc.textField(self.CONST_HDRI_MAP, edit=True)
        #mc.setParent(LightLinkerFrame)
        
        mc.showWindow()
    
    
    def FindMaterialList(self):
        grabAllGeometry = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=False)
      
        return grabAllGeometry
            
    def GenerateMaterialList(self, llmm):
        grabAllGeometry = mc.ls(type = 'geometry', dag = True, excludeType = 'shape')    
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')
        #return shadingGrps
        llmm.removeAll()
        pm.iconTextScrollList(llmm, edit = True, allowMultiSelection=True)
        
        optimizedGrp = []
        refresh = shadingGrps

        if shadingGrps != None:
            for x in shadingGrps:
                if optimizedGrp != []:
                    if x not in optimizedGrp:
                        optimizedGrp.append(x)
                        llmm.append(x)  
                else:
                    optimizedGrp.append(x)
                    llmm.append(x)
        else:
            mc.warning("No shading groups in scene that are attached to objects. Ignoring shading group refresh.")
                
    def CreateHDRILightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        #HDRI = self.FindLights("HDRI")
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        
        selectedMat = materials.getSelectItem()
        hdrObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        if hdrObj != None:
            for sel in hdrObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 0, light = hdr, object = sel)
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 1, light = lights, object = sel)
            #self.RefreshHDRIshading()

        
        
    def CreateDefaultLightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        
        selectedMat = materials.getSelectItem()
        defObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        
        
        if defObj != None:
            for sel in defObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 1, light = hdr, object = sel)
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 0, light = lights, object = sel)
            #self.RefreshHDRIshading()
        else:
            mc.warning("No lights were found in scene. Unable to Light Link")

    def ConnectAllLightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        #HDRI = self.FindLights("HDRI")
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        selectedMat = materials.getSelectItem()
        defObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        if defObj != None:
            for sel in defObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 0, light = hdr, object = sel)
                
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 0, light = lights, object = sel)
                    
            #self.RefreshHDRIshading()
        else:
            mc.warning("No lights were found in scene. Unable to Light Link")   
    
    def RefreshHDRIshading(self):
        print DMO.highlightingIsActive
        try:
            mc.warning('Tried DMO')
            DMO
        except NameError:
            import DisplayMetalObjects_singular as DMO
            reload (DMO)
            mc.warning('Except Name Error')
        else:
            mc.warning('Should be hiring')
            if DMO.highlightingIsActive == False:
                DMO.highlightingIsActive = True
                build = DMO.DisplayMetalObjects(True)
                build.DecisionMaker()
            else:
                DMO.highlightingIsActive = False
                build = DMO.DisplayMetalObjects(False)
                build.DecisionMaker()
                
		            
    def FindLights(self, sendBack):
        print ''
        #create array of all lights from scene
        grabAllLights = mc.ls(type = 'light', dag = True)
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)
        
        #Returnable Variables
        sceneLights = []
        HDRI = []
        AllLights = []
        
        metallicShaders = []
        nonMetallicShaders = []
        nonMetalObjects = []
   
        #sort lights into separate arrays (HDRI vs everything else)
        for i in grabAllLights:
            if i != self.CONST_HDRI_MAP:
                sceneLights.append(i)
                AllLights.append(i)

        for x in grabAllGeometry:
            if x == self.CONST_HDRI_MAP:
                HDRI.append(x)
                AllLights.append(x)

        grabAllGeometry = mc.ls(type = 'geometry', dag = True)    
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')

        #Make list of metallic shaders in specific material
        if shadingGrps != None:
            for mat in shadingGrps:
                if mat == self.CONST_METAL_GOLD or mat == self.CONST_METAL_SILVER or mat == self.CONST_METAL_PORCELAIN:
                    metallicShaders.append(mat)
                else:
                    nonMetallicShaders.append(mat)
        else:
            mc.warning("No HDRI lights were found in scene. Unable to Link HDRI")
        #Make list of objects that have specific material connections

        metalObjects = mc.listConnections(metallicShaders, t = 'mesh', scn = True)
        
        nonMetalObjects = []
        nonMetalObjects = mc.listConnections(nonMetallicShaders, t = 'mesh', scn = True)  

        if sendBack == "HDRI":
            print ''
            return HDRI
        if sendBack == "Default":
            print ''
            return sceneLights
        if sendBack == "All":
            print ''
            return AllLights
            
    def SelectItemsWithMaterial(self, materials):      
        #create array of all lights from scene
        HDRI = []
        metallicShaders = []
        nonMetallicShaders = []
        nonMetalObjects = []
        
        mat =  materials.getSelectItem()
        
        grabAllGeometry = mc.listConnections(mat, type='mesh', scn = True)
        print grabAllGeometry
        mc.select(grabAllGeometry)
    
    def RenameShaderGroups(self, rsg):
        if rsg.getSelectItem() != None:
            oldName = "".join(rsg.getSelectItem())
            result = mc.promptDialog(
            title='',
            message='Rename Shader Group',
            text = oldName,
            button=["Rename", "Generate from Material",'Cancel'],
            defaultButton="Create",
            cancelButton='Cancel',
            dismissString='Cancel')

            if result == "Rename":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.rename(oldName, newName)
                self.GenerateMaterialList(rsg)
                
            if result == "Generate from Material":
                newName = self.GenerateNameForMat(oldName)
                mc.rename(oldName, newName)
                self.GenerateMaterialList(rsg)
        else:
            mc.warning("Nothing is selected, unable to rename quick select set.")  
            
    def GenerateNameForMat(self, oldName):
        MAT = mc.listConnections(oldName)
        for x in MAT:
            searchFor = x + '.outColor'
            if mc.objExists(searchFor):
                newName = x + "SG"

        print newName
        return newName
        
    #Find materials (not shader groups) in scene
        #Create material list (one for each material)

    #Link to HDRI
    
    #Link to Default

    #Link to Both
    
    