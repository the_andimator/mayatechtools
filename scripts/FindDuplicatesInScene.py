import maya.cmds as mc

def FindDups():
    #Find duplicates
    duplicateObjects = []
    transformNodes = mc.ls(type = "transform", sn =True)
    dup_list = transformNodes

    for x in transformNodes:
        shortName_x = x
        splitName_x  = x.split('|')[-1]
        dup_list.remove(x)
        
        for i in dup_list:
            shortName_i = i
            splitName_i  = i.split('|')[-1] 
            if shortName_x == shortName_i or splitName_x == splitName_i:
                duplicateObjects.append(mc.ls(x, i))

    mc.select(clear= True)
    for each in duplicateObjects:
        mc.select(each, add = True)
        
    numDuplicates = len(mc.ls(sl=True))
    print "you have %s duplicates in your scene!" % numDuplicates
    print "Duplicates:  %s" % duplicateObjects
