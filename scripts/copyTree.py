import shutil
import os.path
import os
import maya.cmds as mc

def file_copied():
    print "File copied!"

# Usage example:  copytree(src, dst, cb=file_copied)
def copytree(src, dst, symlinks=False, ignore=None, cb=None):
    names = os.listdir(src)
    if ignore is not None:
        ignored_names = ignore(src, names)
    else:
        ignored_names = set()

    os.makedirs(dst)
    errors = []
    
    mc.warning(names)

    incrValue = 100/len(names)
    mc.warning(incrValue)
    for name in names:
        if name in ignored_names:
            continue
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)

        if symlinks and os.path.islink(srcname):
            linkto = os.readlink(srcname)
            os.symlink(linkto, dstname)
        elif os.path.isdir(srcname):
            copytree(srcname, dstname, symlinks, ignore)
        else:
            # Will raise a SpecialFileError for unsupported file types
            shutil.copy2(srcname, dstname)
            if cb is not None:
                cb()
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        #except Error, err:
        #    errors.extend(err.args[0])
        #except EnvironmentError, why:
         #   errors.append((srcname, dstname, str(why)))
    try:
        shutil.copystat(src, dst)
    except OSError, why:
        if WindowsError is not None and isinstance(why, WindowsError):
            # Copying file access times may fail on Windows
            pass
        else:
            errors.extend((src, dst, str(why)))
    #if errors:
     #   raise Error, errors