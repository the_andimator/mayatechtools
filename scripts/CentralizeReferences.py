import maya.cmds as mc
import pymel.core as pm
import os
import os.path as path
import sys

class CentralizeAssets(object):
    global renderLayerWin
    renderLayerWin = ''

    def __init__(self):
        user = path.abspath(path.join(os.getcwd()))
        conda = "Anaconda2\envs\Python27\Lib\site-packages"
        pythonPath = os.path.join(user, conda)
        
        sys.path.append(pythonPath)
        
        self.windowName = windowName = "CentralizeAssets"
        self.numTextures = numTextures = []
        self.numLights = numLights = []
        self.numMisc = numMisc = []
        self.numReferences = numReferences = []
        self.icon = icon = mc.internalVar(upd = True) + "/icons/folder.png"
        
        self.imageDirectory = imageDirectory = str(mc.workspace(q=True, rd = True)) + "sourceimages"
        self.lightsDirectory = assetsDirectory = str(mc.workspace(q=True, rd = True)) + "sourceimages"
        self.referenceDirectory = referenceDirectory = str(mc.workspace(q=True, rd = True)) + "assets"
        self.miscDirectory = miscDirectory = str(mc.workspace(q=True, rd = True)) + "sourceimages"

        try:
            pm.deleteUI(self.windowName, window = True)
        except:
            pass

        if mc.window("CentralizeAssets", exists = True):
            pm.deleteUI("CentralizeAssets")

        #calls function that grabs all texture references in scene and categorizes them into appropriate arrays
        self.GetTexturesReferences()
        self.BuildWindow()

    def BuildWindow(self):
        
        prepWin = mc.window(self.windowName, w = 300, h = 400, mnb = True, mxb = True, s = 1)
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        winHeight = 400

        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            pm.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 850, h= winHeight)

        #Create menu items
        menuBarLayout = mc.menuBarLayout()
        mc.menu( label='File' )
        mc.menuItem( label='Close UI', i = "sortByDate.bmp", c = lambda *_: CloseUI(self.windowName))
        mc.menuItem( label='Autofill all fields with directory', i = "icon_rendersequence.png", c = lambda *_: self.AutofillDirectory())
        mc.menuItem( label='Resolve Paths', i = "icon_rendersequence.png", c = lambda *_: self.ConnectReferencesToLocal())
        
        ###Create Project Directory Options
        top = mc.separator("top",style='in', width=850)
        pm.text(al = "left", label = "")
        pm.rowColumnLayout( "title", numberOfColumns=3, columnWidth = [(1,150), (2,665), (3,35)])
        pm.text(al = "center", label = "Project Directory:")
        self.projectDirectoryText = pm.textField(text = str(mc.workspace(q=True, rd = True)) )
        self.ProjectDirectory = pm.symbolButton(w = 20, h = 20, image = self.icon, ebg = True, c = lambda *_: self.BrowseFilePath(3, None, self.projectDirectoryText))
        mc.setParent("..")
        pm.text(al = "left", label = "")

        #Create header elements
        pm.rowColumnLayout("header", numberOfColumns=8, columnWidth = [(1,100), (2,75), (3,40), (4,50), (5,40), (6,75), (7,40), (8,150)])
        pm.text(al = "left", label = "")
        pm.text(al = "left", label = "Type")
        pm.text(al = "left", label = "")
        pm.text(al = "left", label = "#")
        pm.text(al = "left", label = "")
        pm.text(al = "left", label = "View")
        pm.text(al = "left", label = "")
        pm.text(al = "left", label = "Copy to this folder")
        mc.setParent("..")
        
        bottom = mc.separator("bottom",style='in', width=925)
        pm.rowColumnLayout("ImageLayout", numberOfColumns=7, columnWidth = [(1,60), (2,110), (3,100), (4,100), (5,50), (6,395), (7,35)])
        
        self.CreateMenuOptions("Images")
        self.CreateMenuOptions("Lights")
        self.CreateMenuOptions("References")
        self.CreateMenuOptions("Misc")
        mc.setParent("..")
        
        pm.text(label="")
        pm.rowColumnLayout("buttonLayout", numberOfColumns=3, columnWidth = [(1,150), (2,600), (3,150)])
        pm.text(label="")
        pm.button(al = "center", label = "Move References to local folders", command = lambda *_:self.CopyReferencesToProject())
        pm.text(label="")

        mc.showWindow()
        
    def CreateMenuOptions(self, label):
        pm.text(al = "center", label = "")
        pm.text(al = "center", label = label)
        pm.text(al = "center", label = self.GetCount(label.lower()))
        
        if label == 'Images':        
            pm.button(al = "center", label = "View Objects", command = lambda *_: self.ViewObjects("image"))
            pm.text(al = "center", label = "")
            self.inputSourceDirectory = pm.textField(text = str(self.imageDirectory))
            self.referenceBrowseButton = pm.symbolButton(w = 20, h = 20, image = self.icon, ebg = True, c = lambda *_: self.BrowseImages(3, None, self.inputSourceDirectory, label ))  
        if label == 'Lights':
            pm.button(al = "center", label = "View Objects", command = lambda *_: self.ViewObjects("lights"))
            pm.text(al = "center", label = "")
            self.inputLightsDirectory = pm.textField(text = str(self.lightsDirectory))
            self.referenceBrowseButton = pm.symbolButton(w = 20, h = 20, image = self.icon, ebg = True, c = lambda *_: self.BrowseLights(3, None, self.inputLightsDirectory, label ))  
        if label == 'References':
            pm.button(al = "center", label = "View Objects", command = lambda *_: self.ViewObjects("references"))
            pm.text(al = "center", label = "")
            self.inputRefsDirectory = pm.textField(text = str(self.referenceDirectory))
            self.referenceBrowseButton = pm.symbolButton(w = 20, h = 20, image = self.icon, ebg = True, c = lambda *_: self.BrowseReferences(3, None, self.inputRefsDirectory, label ))  
        if label == 'Misc':
            pm.button(al = "center", label = "View Objects", command = lambda *_: self.ViewObjects("misc"))
            pm.text(al = "center", label = "")
            self.inputMiscDirectory = pm.textField(text = str(self.miscDirectory))
            self.referenceBrowseButton = pm.symbolButton(w = 20, h = 20, image = self.icon, ebg = True, c = lambda *_: self.BrowseMisc(3, None, self.inputMiscDirectory))         
        
    def GetTexturesReferences(self):

        self.numTextures = numTextures = []
        self.numLights = numLights = []
        self.numMisc = numMisc = []
        self.numReferences = numReferences = []

        # Get list of all textures in scene (including references)
        activeTextures = mc.ls( textures=True, lights = True, )
        
        # For each file node..
        for f in activeTextures:
            checkType = mc.nodeType(f)
            if checkType == "file":
                test = mc.ls(tex=True)
                self.numTextures.append(f)
                    
        ref = pm.listReferences(recursive = True)
        for x in ref:
            self.numReferences.append(x)

        Lights = mc.ls(type="aiPhotometricLight")
        for l in Lights:
            self.numLights.append(l)
            print mc.getAttr(l + ".aiFilename")
                    # Test image directory 


    def BuildWindowForList(self, type, list):

        try:
            pm.deleteUI(str(type + "_win"), window = True)
        except:
            pass

        if mc.window(str(type + "_win"), exists = True):
            pm.deleteUI(str(type + "_win"))
            
        prepWin = mc.window(type + "_win", w = 300, h = 400, mnb = True, mxb = True, s = 1)
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        winHeight = 400
        
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            pm.deleteUI(self.baseColumnLayout)

        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 400, h= winHeight)
        
        ###Create Project Directory Options
        pm.text(al = "left", label = "")
        top = mc.separator(type + "_top",style='in', width=400)

        #Create header elements
        pm.rowColumnLayout(type + "_header", numberOfColumns=3, columnWidth = [(1,100), (2,200), (3,100)])
        pm.text(al = "left", label = "")
        pm.text(al = "center", label = "Object Name")
        pm.text(al = "left", label = "")
        mc.setParent("..")
        top = mc.separator(type + "_mid",style='in', width=400)
        
        pm.rowColumnLayout(type + "_items", numberOfColumns=1, columnWidth = [(1,400)])
        
        for x in list:
            pm.text(al = "center", label = x)
        
        mc.showWindow()

    ###################____BROWSE FILE PATH____#######################
    def BrowseFilePath(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.projectDirectoryText, edit = True, text = returnPath)
        self.projectDirectoryText = returnPath
        return returnPath

    def BrowseImages(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputSourceDirectory, edit = True, text = returnPath)
        self.inputSourceDirectory = returnPath
        return returnPath
        
    def BrowseLights(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputLightsDirectory, edit = True, text = returnPath)
        self.inputLightsDirectory = returnPath
        return returnPath
        
    def BrowseReferences(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputRefsDirectory, edit = True, text = returnPath)
        self.inputRefsDirectory = returnPath
        return returnPath
        
    def BrowseMisc(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputMiscDirectory, edit = True, text = returnPath)
        self.inputMiscDirectory = returnPath
        return returnPath


    def AutofillDirectory(self):
        autofillWindowName = "Autofill_Directory"
        setupWin = mc.window(autofillWindowName, title= autofillWindowName, w =440, h = 80, mnb = True, mxb = True, s = 0, rtf = True)
        mainLayout = mc.columnLayout(w = 440, h= 80)
        ###_Image_Directory_###
        directoryText = str(mc.workspace(q=True, rd = True)) + "images"
        
        rowColumnLayout = mc.rowColumnLayout(nc = 4, cw = [(1,85), (2,315), (3,30), (4,20)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4)])
        mc.text(label = "File Directory : ", align = "right") 
        inputDirectory = mc.textField(pht='C:\Users\Joe_Danger\Documents\maya\projects\default', w = 285 , text = directoryText) 
        icon = mc.internalVar(upd = True) + "/icons/folder.png"
        inputBrowseButton = mc.symbolButton(w = 20, h = 20, image = icon, ebg = True, c = lambda *_: self.BrowseFilePath(3, None, self.inputDirectory)) 

        mc.setParent("..")
        mc.text (label = "") 
        mc.text (label = "") 
        autoButtons = mc.rowColumnLayout(nc = 4, cw = [(1,110), (2,100), (3,20), (4,100)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4)])
        mc.text (label = "") 
        mc.button(label = "Apply", command = lambda *_: self.ApplyGlobalFileDirectory())
        mc.text (label = "") 
        mc.button(label = "Cancel", command = lambda *_: self.DeleteUI())
        mc.showWindow()
        
    def GetCount(self, returnType):
        if returnType == "images":
            return len(self.numTextures)
        elif returnType == 'lights':
            return len(self.numLights)
        elif returnType == 'references':
            return len(self.numReferences)
        elif returnType == 'misc':
            return len(self.numMisc)
        else:
            mc.warning("Invalid string sent to GetCount(). Returning Nonetype.")
            return 0

    def CheckExists(file):
        if os.path.exists(file) == False:
            return False
        else:
            return True
     
    def ViewObjects(self, type):
        print type
        
        #create window
        if type == 'image':
            if len(self.numTextures) == 0:
                print "No %s items to view" % (type)
                return
                
            self.BuildWindowForList(type, self.numTextures)
            
            for x in self.numTextures:
                print x

        elif type == 'lights':
            if len(self.numLights) == 0:
                print "No %s items to view" % (type)
                return
                
            self.BuildWindowForList(type, self.numLights)
            for x in self.numLights:
                print x

        elif type == 'references':
            if len(self.numReferences) == 0:
                print "No %s items to view" % (type)
                return
            #creates temp array so window shows simple names istead of directories
            nodeNames = []
            for x in self.numReferences:
                nodeNames.append(x.refNode)
                print x
                
            self.BuildWindowForList(type, nodeNames)

        elif type == 'misc':
            if len(self.numMisc) == 0:
                print "No %s items to view" % (type)
                return
                
            self.BuildWindowForList(type, self.numMisc)
            for x in self.numMisc:
                print x

        print str( 'Window should be open now for %s' %(type) )


    def ApplyGlobalFileDirectory(self):
        print "Directory shown successfully. Implement directoy being applied to UI."
        self.DeleteUI()
           
    def DeleteUI(self):
        if(mc.window("Autofill_Directory", exists=True)):
            mc.deleteUI("Autofill_Directory")

    def CopyReferencesToProject(self):
        #Load Copy Script
        import CopyToLocation as Copy
        reload (Copy)
        
        imageFileDirectory = []
        lightFileDirectory = []
        refFileDirectory = []
        miscfileDirectory = []
        #verify that object exists, get path, store in variable
        #iterate through textures
        if len(self.numTextures) > 0:
            #Get directories of objects
            for tex in self.numTextures:
                temp = mc.getAttr(tex + '.fileTextureName')
                #verify directories
                print temp 
                if os.path.isfile(temp):
                    imageFileDirectory.append(temp)
                    
                print "   '%s' has been added to imageFileDirectory. File Path:     %s" % (tex, temp)

        #iterate through lights
        if len(self.numLights) > 0:
            for tex in self.numLights:
                temp = mc.getAttr(tex + '.aiFilename')
                #verify directories
                if os.path.isfile(temp):
                    lightFileDirectory.append(temp)
                    
                print "   '%s' has been added to lightFileDirectory. File Path:     %s" % (tex, temp)

        #iterate through references      
        if len(self.numReferences) > 0:
            for tex in self.numReferences:
                #convert fileref node to string
                temp = str(tex)
                #verify directories
                if os.path.isfile(temp):
                    refFileDirectory.append(temp)
                    
                print "   '%s' has been added to refFileDirectory. File Path:     %s" % (tex.refNode, tex)

        #iterate through misc
        if len(self.numMisc) > 0:
            print ''

        # This function can be reused to copy files from one directory to another. Must input full paths in order for it to work.
        imageDir = mc.textField(self.inputSourceDirectory, q=True, text = True)
        lightDir = mc.textField(self.inputLightsDirectory, q=True, text = True)
        refDir = mc.textField(self.inputRefsDirectory, q=True, text = True)
        miscDir = mc.textField(self.inputMiscDirectory, q=True, text = True)
        
        print imageDir  
        print lightDir  
        print refDir  
        print ""  
        print ""  
        print ""  
        print ""  
        print imageFileDirectory  
        print lightFileDirectory  
        print refFileDirectory  
        
        
        type = 1
              
        #run through each item and copy directories
        if os.path.isdir(imageDir):
            for i in imageFileDirectory:
                Copy.CopyToLoc(i,imageDir, type, None) ###  0 == copytree :::  1 == copy individually
        else:
            mc.warning("Invalid image directory! dir:   %s" % (imageDir))
            
        if os.path.isdir(lightDir):            
            for l in lightFileDirectory:
                Copy.CopyToLoc(l, lightDir, type, None) ###  0 == copytree :::  1 == copy individually
        else:
            mc.warning("Invalid light directory! dir:   %s" % (lightDir))      
              
        if os.path.isdir(refDir):            
            for r in refFileDirectory:
                Copy.CopyToLoc(r, refDir, type, None) ###  0 == copytree :::  1 == copy individually
        else:
            mc.warning("Invalid ref directory! dir:   %s" % (refDir))            

        self.ConnectReferencesToLocal()
        
    def ConnectReferencesToLocal(self):

        #Sort by images, lights, references, etc
        for x in self.numTextures:
            print ''
            filename = x
            print "Texture Name:    %s" % (filename)

            #get string of local project file path
            curPath = mc.getAttr(filename + '.fileTextureName')
            baseName = os.path.basename(curPath)    
            localPath = os.path.join(self.imageDirectory, baseName)

            print "Cur Path:    %s" % (curPath)
            print "Local Path:    %s" % (localPath)
            print "Base Name:    %s" % (baseName)
            print ''

            pm.setAttr(filename + '.fileTextureName', localPath)


        for r in self.numReferences:
            print ''
            refName = r
            print "Reference Name:    %s" % (filename)            
            curPath = mc.referenceQuery(refName, filename = True)
            print ''

            
        for l in self.numLights:
            print ''
            #get string of filename
            filename = l
            print "Light Name:    %s" % (filename)

            #get string of local project file path
            curPath = mc.getAttr(filename + '.aiFilename')
            baseName = os.path.basename(curPath)    
            localPath = os.path.join(self.lightsDirectory, baseName) #self.imageDirectory

            print "Cur Path:    %s" % (curPath)
            print "Local Path:    %s" % (localPath)
            print "Base Name:    %s" % (baseName)
            print ''

            pm.setAttr(filename + '.aiFilename', localPath)
            
        for r in self.numMisc:
            print ''
            
        mc.FilePathEditor()
        #Go through each item in list

        #get string of filename

        #get string of local project file path

        #get maya node and image name property

        #apply local path + filename to imagename property in maya node

        #apply path to object node

        print ''

def CloseUI(windowName):
    uiName = str("mc.deleteUI('%s', window = True)" % (windowName))
    pm.evalDeferred(uiName)
        

        
#Script will look at all references in scene

#Compare reference directories to set project

#If refs are not inside project directoy,
    #script will copy the files in to the images/assets folders
    #
    