import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import sys as sys
from functools import partial as ftool
import shutil as shutil
from os import listdir
import os


########################################
#########__SAVE_THIS_TO_SHELF_##########
########################################
#
#import Render_Sequencer as MI
#reload (MI)
#build = MI.SequenceSetup(1)
#build.PrepareWindow()
#
########################################
########################################
class SequenceSetup(object):
    
    def __init__(self, amount):
        try:
            mc.deleteUI(setupWin, window = True)
        except:
            pass
        self.amount = amount
        self.windowName = windowName = "Creating Sequencer"
       
        if mc.window(self.windowName, exists = True):
            mc.deleteUI(self.windowName)
        self.setupWin = mc.window(self.windowName, title= self.windowName, w =250, h = 85, mnb = True, mxb = True, s = 1, rtf = True)
        mainLayout = mc.columnLayout(w = 250, h= 50)
        
    def PrepareWindow(self):
        
        result = mc.promptDialog(
		title='Creating Sequence...',
		message='# of Sequences:',
		button=["Honey Batcher don't give a Fu..", 'Cancel'],
		defaultButton="Honey Batcher don't give a Fu..",
		cancelButton='Cancel',
		dismissString='Cancel')

        if result == "Honey Batcher don't give a Fu..":
            text = mc.promptDialog(query=True, text=True)
            self.BuildItBetter(int(text))

    def BuildItBetter(self, amount):
        self.amount = amount
        if amount == "":
            mc.warning("No value given. Defaulting to 1 sequence")
        pm.mel.eval("setMayaSoftwareFrameExt(3, 0)")
        pm.setAttr("defaultRenderGlobals.extensionPadding", 4)
        SequencerWindow = self.CreateWindowInstance()     
        SequencerWindow.BuildUI()
        #overides = number of sequences, camera name (iterates # on subsequent sequences), start frame, end frame, skip existing (boolean), directory, prefix 
        #img_<Scene>_<RenderLayer>
        print self.amount

    
    def CreateWindowInstance(self):
        return Window(self.amount, 'camera', 1, 2, 0, '', 'img_<Scene>_<Camera>_<RenderLayer>_', "<None>", "Scene One")       

        
class Window(object):
    
    global mainWin
    
#######################____Initialize Variables____#######################

    def __init__(self, numSequences, name, start, end, skip, directory, prefix, rendLayer, sceneAssignment):
        try:
            mc.deleteUI(mainWin, window = True)
        except:
            pass
        
        self.numSequences = numSequences
        self.name = name
        self.start = start
        self.end = end
        self.skip = skip
        self.directory = directory
        self.prefix = prefix
        self.rendLayer = rendLayer
        self.sceneAssignment = sceneAssignment
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        self.sequenceInputs = sequenceInputs = []
        self.sequenceStorage = sequenceStorage = []
        self.numSequences = numSequences
        self.sizeable = sizeable = 1
        self.windowName = windowName = "Render_Sequencer"
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        
        if mc.window("Render_Sequencer", exists = True):
            mc.deleteUI("Render_Sequencer")
        
        
    def BuildUI(self):
        mainWin = mc.window("Render_Sequencer", title= self.windowName, w =738, h = 550, mnb = True, mxb = True, s=self.sizeable, rtf = True)
        
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)

        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 738, h= 350)
        
        for x in range(self.numSequences):
            tempName = "Camera_Scene_"#self.name
            
            name = tempName + "0" + str(x+1) #Camera_Scene_01
            self.sequenceStorage.append (self.MakeNew(name, self.start, self.end, self.skip, self.prefix, self.rendLayer, self.sceneAssignment))
            self.sequenceStorage[x].BuildSequenceUI(self.sequenceStorage[x].name, x, self.sequenceStorage[x].start, self.sequenceStorage[x].end, self.sequenceStorage[x].skip, self.sequenceStorage[x].directory ,self.sequenceStorage[x].prefix, self.sequenceStorage[x].rendLayer, mainWin)

        mc.button(label = "Start Hackish Batch Render", command = lambda *_: self.ApplyCallBack(self.numSequences), w = 738)

        mc.showWindow()
        
    def MakeNew(self, name, start, end, skip, prefix, rendLayer, sceneAssignment):
        return SequenceUI(name, self.numSequences, start, end, skip, "", prefix, rendLayer, sceneAssignment)
        
#######################____APPLY GUI VALUES TO LISTS____#######################
    def ApplyCallBack(self, numSequences):
        seq = 0
        seqAttr = 0
        verified = []
        result = True
        
        #Changes renderAllSequenceCameras to True in render sequencer options. This is very important, disabling this option breaks camera switching when doing renders.
        mel.eval('optionVar -intValue "renderSequenceAllCameras" 1;')

        
        for x in range(numSequences):
            name = mc.textField(self.sequenceStorage[x].name, q=True, text = True)
            start = mc.intField(self.sequenceStorage[x].start, q=True, v = True)
            end = mc.intField(self.sequenceStorage[x].end, q=True, v = True)
            skip = mc.checkBox(self.sequenceStorage[x].inputSkip, q=True, v = True)
            directory = mc.textField(self.sequenceStorage[x].inputDirectory, q=True, text = True)
            prefix = mc.textField(self.sequenceStorage[x].inputPrefix, q=True, text = True)
            rendLayer = mc.textField(self.sequenceStorage[x].inputRendLayer, q = True, text = True)
            sceneAssignment = mc.textField(self.sequenceStorage[x].inputSceneAssignment, q = True, text = True)
            

            verified.append(self.VerifyInputs(numSequences, name, rendLayer, start, end, directory, prefix, sceneAssignment))

            for v in verified:
                if v == False:
                    result = False
                else:
                    print v
            
            if result == True:
                #Disables all other cams from being renderable
                self.CheckDirectory(directory, rendLayer, prefix, sceneAssignment)
                self.sequenceStorage[x].SetCamAsRenderable(name, 1) #Changes camera to renderable state
                mc.warning("enabled %s before rendering" %(mc.textField(self.sequenceStorage[x].name, q=True, text = True)))
                  
                self.sequenceStorage[x].LookThroughCam(name) #Looks through active camera

#################__Send Sequences to be Rendered__########
                self.sequenceStorage[x].UpdateRenderGlobals(start, end, prefix, skip) #Changes render globals to match start/end frames, image prefix, and whether to skip existing frames
                self.sequenceStorage[x].SetDirectory(directory) #changes render directory
                self.sequenceStorage[x].SetRenderLayer(rendLayer)
                
                ##Disable persp as renderable camera"
                mc.setAttr("perspShape.renderable", 0)
                mc.setAttr("perspShape.visibility", 0)
                mc.setAttr("persp.visibility", 0)
                mel.eval("getCurrentCamera()")

                if(x == 0):
                    mc.RenderSequence( s=start, e=end, RenderAllCameras=False) #Starts Sequence Render
                else:
                    mc.RenderSequence( s=start, e=end)
                
                self.sequenceStorage[x].SetCamAsRenderable(name, 0)
                mc.setAttr("%s.visibility" % name, 0)
                
                projectRoot = mc.workspace(q=1,rd=1)
                src = projectRoot + "images/tmp/"
                dst = directory
                print "SCENE :::   " + sceneAssignment
                
                copyTree(src, dst, sceneAssignment, name, rendLayer, False, None)
                mc.pause (sec = 2)
                mc.refresh()
                    
            elif result == False:
                mc.warning("")
                mc.warning("")
                mc.warning("")
                mc.warning("Errors found in sequencer input fields. Double check to make sure everything is correct")
                mc.warning("")
                mc.warning("")
                mc.warning("")
                break

    
    #This checks inputs of all sequences to make sure they are valid
    def VerifyInputs(self, num, name, rendLayer, start, end, directory, prefix, sceneAssignment):
        verified = []
        passFail = True
        
        for x in range(num):
            name = mc.textField(self.sequenceStorage[x].name, q=True, text = True)
            start = mc.intField(self.sequenceStorage[x].start, q=True, v = True)
            end = mc.intField(self.sequenceStorage[x].end, q=True, v = True)
            directory = mc.textField(self.sequenceStorage[x].inputDirectory, q=True, text = True)
            prefix = mc.textField(self.sequenceStorage[x].inputPrefix, q=True, text = True)
            rendLayer = mc.textField(self.sequenceStorage[x].inputRendLayer, q = True, text = True)
            sceneAssignment = mc.textField(self.sequenceStorage[x].inputSceneAssignment, q = True, text = True)
            
            #Check to make sure that the cameras exist
            if mc.objExists(name):
                print ("verified " + name + " exists")
            else:
                mc.warning(name + " could not be verified, aborting render process")
                passFail = False

            #Check to see if frames already exist, copy over to tmp dir if true
            #
            #
            #
            #       Arnold does not currently support skip existing frames. A different method is needed.
            #       Check for existing frames, find gap in number, change start frame to that number
            #
            #
            #self.CheckDirectory(directory, rendLayer, prefix, sceneAssignment)

            #Check to make sure the render layers exist
            if self.CheckRenderLayer(rendLayer) == False:
                passFail = False
                mc.warning("Render Layer doesn't exist. Aborting render process")

            #Check to make sure that start/end frames are int values
            if start > end:
                passFail = False
                mc.warning("start frame is larger than end frame. Aborting render process")
                
            #Check file directory for existing folders and files
            verified.append(passFail)

            for check in verified:
                if check == False:
                    passFail = False

        return passFail

    def CheckDirectory(self, directory, renderLayer, prefix, sceneAssignment):
        #Get folder name
        
        # Get the list of all render layers. Find render layer to append to find folder.
        render_layer_list = mc.ls(type="renderLayer")
        folderRL = renderLayer

        #Get Target directory
        #Append folder name to target directory
        targetDir = directory + "/" + sceneAssignment + "/" + folderRL
        
        #Get tmp directory
        projectRoot = mc.workspace(q=1,rd=1)
        tmpRoot = projectRoot + "images/tmp/"
        destination = tmpRoot + folderRL
        

        #confirm if it exists
        #if it exists, copy target to tmp directory
        mc.warning (targetDir + "  TARGET DIR")
        mc.warning (destination + "  DESTINATION DIR")
        mc.warning (tmpRoot + "  tmpRoot DIR")
        if os.path.isdir(targetDir):
            print ("Destination folder exists: %s" % targetDir)
            DeleteTemp(destination) #purges any files currently in temp. This shouldn't backfire at all..
            
            if os.path.isdir(destination):
                print "Do nothing"
            else:
                CopyExisting(targetDir, destination)
            #copyTree(targetDir, destination, sceneAssignment, name, rendLayer, False, None)
        else:
            mc.warning("UNABLE TO FIND THIS DIRECTORY:  " + targetDir)

            desktopPass = "\Desktop"
            folderName = "\StoredRenders"
            homepath = os.environ['HOMEPATH']
            backupDestination = homepath + desktopPass + folderName
            #print backupDestination

            try:
                os.path.exists(backupDestination)
                print "path exists, awesome"
            except:
                #os.makedirs(backupDestination)
                CopyExisting(targetDir, backupDestination)
                mc.warning("Unable to find originally directory. Images have been copied to the desktop.")
                mc.warning('new location:     ' + backupDestination)

            
    def CheckRenderLayer(self, activeLayer):
        mc.warning("CUR RENDER LAYER:  " + activeLayer)
        foundLayer = False
        defaultRenderLayer = "defaultRenderLayer"
        render_layer_list = mc.ls(type="renderLayer")
        if "rs_" in activeLayer:
            print("")
        else:
            activeLayer = "rs_" + activeLayer
            
        mc.warning("CUR RENDER LAYER:  " + activeLayer)
        
        for i in render_layer_list:  
            if i == activeLayer:
                foundLayer = True
                break
            else:
                foundLayer = False
            
        return foundLayer
        
class SequenceUI():
    
    def __init__(self, name, numSequences, start, end, skip, directory, prefix, rendLayer, sceneAssignment):
        self.name = name
        self.start = start
        self.end = end
        self.skip = skip
        self.directory = directory
        self.prefix = prefix
        self.rendLayer = rendLayer
        self.sceneAssignment = sceneAssignment
        
        SequenceTitle = None
        UpperRowName = None
        LowerRowName = None
        inputName = None
        seqCount = int(numSequences)
        
        self.sequences = sequences = []
        self.SequenceTitle = SequenceTitle
        SequenceTitle = "Sequence %s"%(seqCount+1)
        self.UpperRowName = UpperRowName
        UpperRowName = "RowColumnTop %s"%(seqCount+1)
        self.LowerRowName =  LowerRowName
        LowerRowName = "RowColumnTop %s"%(seqCount+1)
        x = int(numSequences)
        self.inputName = inputName
        
        appendedName = name
        tempSequence = []
        tempSequence.append(appendedName)
        tempSequence.append(self.start)
        tempSequence.append(self.end)
        tempSequence.append(self.skip)
        tempSequence.append(self.directory)
        tempSequence.append(self.prefix)

        self.sequences.append(tempSequence)

        ##Input Fields
        iName = ""
        iStart = 0
        iEnd = 10
        iSkip = False
        iDir = ""
        iPrefix = ""
        
        self.iName = iName
        self.iStart = iStart
        self.iEnd = iEnd
        self.iSkip = iSkip
        self.iDir = iDir
        self.iPrefix = iPrefix
        
###################____Create UI based on # of sequences____#######################
    def BuildSequenceUI(self, name, numSequences, start, end, skip, direct, prefix, rendLayer, mainWin):
        seqCount = int(numSequences)
        directoryText = None
        self.SequenceTitle = "Sequence %s"%(seqCount+1)
        self.UpperRowName = "RowColumnTop %s"%(seqCount+1)
        self.LowerRowName = "RowColumnTop %s"%(seqCount+1)
        self.directoryText = directoryText
        x = int(seqCount)
        self.inputName = ""
        
        ###Create Border and Frames####
        instanceFrameLayout = mc.frameLayout(self.SequenceTitle, parent = mainWin, label = self.SequenceTitle, cll = True, bv = True)
        instancerowColumnLayout = mc.rowColumnLayout(self.UpperRowName, nc = 9, cw = [(1,93), (2,250), (3,30), (4,50), (5,30), (6,50), (7,80), (8,50), (9,100)], columnOffset = [(1, "both", 2), (2, "both", 2), (3, "both", 0), (4, "both",1), (5, "both", 5), (6, "both", 1), (7, "both", 2), (8, "both", 2), (9, "both", 2)])
        
        ###_Camera_###
        mc.text(label = "Camera Name : ", align = "right") 
        self.name = mc.textField(text = self.name)

        ###_Start_###
        mc.text(label = "Start : ", align = "left") 
        self.start = mc.intField(v = self.start)
  
        ###_End_###
        mc.text(label = "End : ", align = "left") 
        self.end = mc.intField( v = self.end)
        mc.text (label = "") 
        mc.text (label = "")
        
        ###_Skip_###
        self.inputSkip = mc.checkBox(label= 'Skip Existing') 
        mc.setParent("..")

        ###_Image_Directory_###
        self.directoryText = str(mc.workspace(q=True, rd = True)) + "images"
        
        rowColumnLayout = mc.rowColumnLayout(nc = 6, cw = [(1,93), (2,400), (3,10), (4,20), (4,50)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4)])
        mc.text(label = "File Directory : ", align = "right") 
        self.inputDirectory = mc.textField(pht='C:\Users\Joe_Danger\Documents\maya\projects\default', w = 400 , text = self.directoryText) 
        icon = mc.internalVar(upd = True) + "/icons/folder.png"
        self.inputBrowseButton = mc.symbolButton(w = 20, h = 20, image = icon, ebg = True, c = lambda *_: self.BrowseFilePath(3, None, self.inputDirectory)) 
        mc.text (label = "") 
        
        ###_Prefix_###
        self.prefix = "Scene_One" + "_"
        mc.text(label = "Prefix : ", align = "right") 
        self.inputPrefix = mc.textField(w = 135, text = self.prefix)
        mc.setParent("..")
        
        ##_Render_Layer_###
        rowColumnLayout = mc.rowColumnLayout(nc = 6, cw = [(1,93), (2,300), (3,108), (4,20), (4,50)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4)])
        mc.text(label = "Render Layer : ", align = "right") 
        self.inputRendLayer = mc.textField(pht='render layer name', w = 300 , text = self.rendLayer) 
        mc.text (label = "")
        mc.text (label = "")
        
        ##_Scene Assignment_##
        mc.text(label = "Scene : ", align = "right") 
        self.inputSceneAssignment = mc.textField(text = "Scene One", w = 135)
        mc.setParent("..")
        mc.setParent("..")

        ###_Attach commands to GUI_###
        mc.textField(self.name, edit=True)
        mc.intField(self.start, edit=True)
        mc.intField(self.end, edit=True)
        mc.textField(self.inputDirectory, edit=True)
        mc.textField(self.inputPrefix, edit=True)
        mc.textField(self.inputRendLayer, edit=True)
        mc.textField(self.inputSceneAssignment, edit=True)

###################____RENDER____########################
    # Update render settings before starting another sequence
    def UpdateRenderGlobals(self, start, end, prefix, skip):
        mc.setAttr("defaultRenderGlobals.startFrame", start)
        mc.setAttr("defaultRenderGlobals.endFrame", end)
        mc.setAttr("defaultRenderGlobals.imageFilePrefix", prefix, type = "string")
        mc.setAttr("defaultRenderGlobals.skipExistingFrames", skip)

    # Sets cameras as renderable/non-renderable
    def SetCamAsRenderable(self, cam, enable):
        if mc.objExists(cam):
            mc.setAttr("%s.renderable" % cam, enable)
        else:
            mc.warning("SetCamAsRenderable() -- Camera could not be found. Please check spelling of camera component")

    def DisableCams(self, cam):
        if mc.objExists(cam):
            mc.setAttr("%s.renderable" % cam, 0)
        else:
            mc.warning("SetCamAsRenderable() -- Camera could not be found. Please check spelling of camera component")


    # Looks through specified camera for render
    def LookThroughCam(self, cam):
        if mc.objExists(cam):
            mc.lookThru(cam)
            #mc.lookThru(cam, q=True )
            mel.eval("getCurrentCamera()")
        else:
            mc.warning("LookThroughCam() -- Unable to look through camera that doesn't exist. Please check spelling of camera component")

    # Must run before starting RenderSequence
    def GetCurrent():
        mel.eval("getCurrentCamera()")

    def SetDirectory(self, directory):   
        self.directory = self.inputDirectory
        
    def SetRenderLayer(self, activeLayer):

        foundLayer = False
        defaultRenderLayer = "defaultRenderLayer"
        # Get the list of all render layers
        render_layer_list = mc.ls(type="renderLayer")
        
        if "rs_" in activeLayer:
            print("rs_ found in render layer")
        else:
            activeLayer = "rs_" + activeLayer
            mc.warning("'rs_' not found in render layer, added rs_ to front of string")
            mc.warning ("new string =    " + activeLayer)
            
        for i in render_layer_list:    
            if i == activeLayer:
                mc.warning("Render Layer found:  " + activeLayer)
                mc.editRenderLayerGlobals (currentRenderLayer = activeLayer)
                foundLayer = True
                return
            else:
                foundLayer = False
                
        if foundLayer == False:
            for x in render_layer_list:
                if x == defaultRenderLayer:
                    mc.editRenderLayerGlobals (currentRenderLayer = defaultRenderLayer)


###################____BROWSE FILE PATH____#######################     

    def BrowseFilePath(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputDirectory, edit = True, text = returnPath)
        self.directoryText = returnPath
        print ("Directory found :   " + str(self.inputDirectory))
        return returnPath


###################____"STORE"_FRAMES_FROM_RENDER_TO_SPEFICIC_FOLDER____#######################  
def copyTree(src, dst, folderName, name, rendLayer, symlinks=False, ignore=None):
    # Print "folder/scene name" + folderName
    # Print "name:   " + name
    # Print "render layer:    " + rendLayer
    # Find render layer
    foundLayer = False
    
    # Get the list of all render layers
    render_layer_list = mc.ls(type="renderLayer")
    folderRL = rendLayer
    
    if "rs_" in rendLayer:
        print"'rs_' found in layer already. Nice."
    else:
        rendLayer = "rs_" + rendLayer
        mc.warning("'rs_' not found in render layer, added rs_ to front of string")
        mc.warning ("new string =    " + rendLayer)
        
    for i in render_layer_list:    
        if i == rendLayer:
            foundLayer = True
            break
    else:
        print 'render layer match has not been found'

    if foundLayer == False:
        rendLayer = "defaultRenderLayer"
        print "render layer was not found. Defaulted to defaultRenderLayer. rendLayer = " + rendLayer

    # Create new folder in dst path, named after assigned scene
    if folderName != "" and folderName != None:    
        dst = dst + "/" + folderName
    else:
        folderName = "Default_Scene"
        dst = dst + "/" + folderName

    # Check to see if path already exists, otherwise create a new path. This is based off of the Scene input field
    if os.path.isdir(dst):
        print ("Huzzah! Destination folder already exists. Congrats.")
    else:
        mc.sysFile(dst, makeDir=True )
        print ("Huzzah! Created a folder for you. Congrats.")
        
    # Check to see if sequence used a render layer
    # Did not use a render layer
    if rendLayer == "defaultRenderLayer":

        for parentDir in os.listdir(src):
            s = os.path.join(src, parentDir)
            d = os.path.join(dst, parentDir) # Joints src and scene folder together
            recursivePath = s
            savedPath = d

            if os.path.isfile(s):
                for i in os.listdir(src):
                    CopyToLoc(s,d,1)
                
            else:
                for x in os.listdir(recursivePath):
                    s = os.path.join(recursivePath, x)
                    d = os.path.join(savedPath, x)
                    # Move images from parent directory
                    if os.path.isfile(s):
                        if os.path.isdir(savedPath):
                            CopyToLoc(s,d,1)
                        else:
                            mc.sysFile(savedPath, makeDir=True )
                            CopyToLoc(s,d,1)
                    else:
                        mc.warning("No files found in path: %s. Unable to copy. " % s)

    # Did use a render layer     
    else:             
        match = folderRL
        curParentDirectory = ""
        print "Match:  " + match
        
        # If a render layer is used : 
        # This finds the base-level directory
        for parentDir in os.listdir(src):
            mc.warning("parentDir - line 459 " + parentDir)
            mc.warning("parentDir - line 459 " + src)
            if parentDir == match: # Does the parent directory match the render layer name?
                curParentDirectory = os.path.join(src, parentDir)
                print "curParentDirectory:    " + curParentDirectory
                s = os.path.join(src, parentDir)
                d = os.path.join(dst, parentDir)


                # Check that folder does not already exist in destination directory
                if os.path.isdir(d): # Proceed to copy files individually
                    # Finds children in specified folder, used when copytree will fail
                    if curParentDirectory != "":
                        # Search list for correct parent directory
                        for childDir in os.listdir(curParentDirectory):
                            print "found child:    " + childDir
                            s = os.path.join(curParentDirectory, childDir)
                            print "Source:        " + s
                            print "Destination:   " + d
                            CopyToLoc(s, d, 1, childDir)
                        break
                else:                #use copyTree instead
                    CopyToLoc(s,d, 0, "")
                    break


# This function can be reused to copy files from one directory to another. Must input full paths in order for it to work.
def CopyToLoc(s,d, type, name): ###  0 == copytree :::  1 == copy individually

    if type == 0:
        print""
        shutil.copytree(s, d, symlinks=False, ignore=None)
        if os.path.exists(s):
            if os.path.isdir(d):
                mc.warning ("Confirmed that %s has been successfully copied into target directory. Proceeding to delete tmp file." % s)
                DeleteTemp(s)  # Deletes entire temp folder after it has been successfully copied to other directory
                print "Successfully Copied 1 Sequence to new directory :::  %s" % d
            else:
                mc.warning("%s does not exist. Cannot remove something that does not exist. What am I, a magician?" % d)
        
    elif type == 1:
        # Move images from parent directory
        print "SRC ::::  " + s
        print "DST ::::  " + d
        
        if os.path.exists(s):
            shutil.copy2(s,d)
            if os.path.isfile(d+ "/" + name):
                print ("ConfirmeDestination folder existsd that %s has been successfully copied into target directory. Proceeding to delete tmp file." % s)
                DeleteTemp(s)  # Deletes file in temp folder after it has been successfully copied to other directory
                print "Successfully Copied 1 Sequence to new directory :::  %s" % d
            else:
                mc.warning ("No file found at destination. Aborting delete. Check tmp folder for rendered files.")
                mc.warning ("isFile(d):     %s" % d)
        else:
            mc.warning("%s does not exist. Cannot remove something that does not exist. What am I, a magician?" % d)
    
def DeleteTemp(path):
    if os.path.isfile(path):
        os.remove(path)  # remove the file
    elif os.path.isdir(path):
        shutil.rmtree(path)  # remove dir and all contains
    else:
        mc.warning("file {} is not a file or dir.".format(path))
        
def CopyExisting(s,d):
    mc.warning( "CopyExisting()   SRC ::  " + s)
    mc.warning( "CopyExisting()   DST ::  " + d)
    shutil.copytree(s, d, symlinks=False, ignore=None)
    if os.path.exists(s):
        if os.path.isdir(d):
            mc.warning ("Confirmed that %s has been successfully copied into target directory. Officially ready for rendering with skipping frames active." % s)
            print "Successfully Copied 1 Sequence folder to new directory :::  %s" % d
        else:
            mc.warning("%s does not exist. Cannot remove something that does not exist. No frames are being skipped during rendering" % d)


#ToDo : Find a way to store values between windows closing. Check 'remember values' box in window to reload those same preferences. Save/read from pref file (?)

###################___Reading_information_from_file_placeholder___######
#        for file in files:
#            if file.rpartition(".")[2] == "mb":
#                
#                #main loop
#                fileName = inputDirectory + "/" + file
#                mc.file(fileName, open = True, force = True, ignoreVersion = True, prompt = False)
#                
#                #do stuff
#                print 'reading the txt  files down here'
#
#   text file should read as follows:
#       amount of sequences 
#           -- per sequence
#       cam name, start, end, skip, image directory, prefix