import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import sys as sys
from functools import partial as ftool
import shutil as shutil
from os import listdir
import os

class StoreSequences(object):
    #constant string for naming conventions
    def AddNewSequence():
        #Creates new sequence group
        seqGroup = CreateSeqGroup()
        if seqGroup != None:
            #Adds attributes
            CreateSequenceAttributes()
            #Add to container
            mc.container(addNode=[seqGroup])

    def CreateSeqGroup():
        newName = FindName()
        if newName == None:
            mc.warning('newName == Nonetype')
        else:
            #Create/Name node
            test = mc.createNode( 'transform', n = newName)
            
        return newName
            
    def FindName():
        curSequences = mc.ls("Sequence_*")
        i = 1
        print curSequences
        for seq in curSequences:
            if seq == "Sequence_" + str(i):
                i = i + 1
                
            elif seq != "Sequence_" + str(i):
                newName = "Sequence_" + str(i)
                print "found new name"
                return newName
                
            if i == len(curSequences):
                i = i + 1
                newName = "Sequence_" + str(i)
                print newName
                print "append new name to end"
                return newName


    def CreateContainer():
        #This should not add the selected nodes, instead create an array using a select name* type function
        #Creates Node
        mc.container(name = "Scene_Sequence_Timing", includeShapes = True, includeTransform = True, force = True, addNode = mc.ls(selection = True))
        #Enable Black Box


    def StoreSequenceInstance():
        #Find sequences in scene and store inside array
        #Maybe find the name of the asset node and try to select children
        print ''


    def RenameNode():
        seqCount = int(numSequences)
        SequenceTitle = "Sequence %s"%(seqCount+1)
        name = "Sequence_1"
        selected = mc.ls(sl= True)
        mc.rename(selected, name)
        print name
        mc.select(name)


    def CreateSequenceAttributes():
        #Attributes need to have unique names, it would be easiest to simply append a number based on the sequence name (_1, _2, _3)
        mc.addAttr(ln = 'CameraName', dt = 'string', s = True)
        mc.addAttr(ln = 'Start', at = 'byte', dv = 0, s = True)
        mc.addAttr(ln = 'End', at = 'byte', dv = 0, s = True)
        mc.addAttr(ln = 'RenderLayer', dt = 'string', s = True)
        mc.addAttr(ln = 'Prefix', dt = 'string', s = True)
        mc.addAttr(ln = 'Scene', dt = 'string', s = True)
        mc.addAttr(ln = 'FileDirectory', dt = 'string', s = True)
        mc.addAttr(ln = 'SkipExisting', attributeType = 'bool', s = True)


    def RenumberSequences():
        print ''

    def DeleteSpecificSequence():
        print ''
                
                
                
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
