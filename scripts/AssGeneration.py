import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import sys as sys
import shutil as shutil
from os import listdir
import os
import maya.OpenMaya as OpenMaya
from functools import partial

class OpenUI(object):

    def __init__(self, SequenceList):
        self.SequenceList = SequenceList

        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "Confirm_batch_file_settings"
        try: 
            mc.deleteUI(WinName)
        except:
            pass

        self.renderNodes = mc.container("Scene_Sequence_Timing",q = True, nl = True)
        prepWin = mc.window(WinName, w =550, h = 175, mnb = True, mxb = True, s = 1)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 550, h= 175)

        #Get filename of obj
        self.fileLocation = mc.file(query=True,sn=True) 
        temp = mc.file(query=True,sn=True, shn = True) 
        self.fileName = temp.replace(".ma", ".bat")
        self.fileName = temp.replace(".mb", ".bat")
        ###__Renders Settings and Scene Prep___###
        RenderSettingsandPrep = mc.frameLayout("RenderSettingsandPrep", parent = prepWin,label = "Render Settings and Prep", cll = False, bv = False)
        mc.separator( style='in', width=550)

        mc.text(label = " Be Sure to Check Your Render Settings Before Exporting .bat and .ass Files", align = 'left', bgc = (1,1,1))

        fileColumn = mc.rowColumnLayout("filenameRow", nc = 3, cw = [(1,100), (2,15), (3,350)])
        mc.text(label = "Batch File Name:", align = 'right')
        mc.text(label = "", align = 'left')
        mc.textField("filename", pht = "e.g. CrownLengthening_S1-S6.bat, Scene7.bat, etc")
        mc.setParent("..")

        fileColumn = mc.rowColumnLayout("filetoexport", nc = 3, cw = [(1,100), (2,15), (3,350)])
        mc.text(label = "Path to Scene File:", align = 'right')
        mc.text(label = "", align = 'left')
        mc.textField(text = self.fileLocation)
        mc.setParent("..")

        directoryColumn = mc.rowColumnLayout("directory", nc = 3, cw = [(1,115), (2,350), (3,25)])
        mc.text(label = "     Export Directory:", align = 'left')
        exportdirectoryText = str(mc.workspace(q=True, rd = True)) + "images"
        self.inputDirectory = mc.textField(pht='C:\Users\Joe_Danger\Documents\maya\projects\default', w = 350 , text = exportdirectoryText) 
        icon = mc.internalVar(upd = True) + "/icons/folder.png"
        exinputBrowseButton = mc.symbolButton(w = 20, h = 20, image = icon, ebg = True, c = lambda *_: self.BrowseFilePath(3, None, self.inputDirectory)) 
        mc.setParent("..")

        createAssColumn = mc.rowColumnLayout("Ass", nc = 3, cw = [(1,100), (2,410), (3,25)])
        mc.text("")
        curLockSample = True
        create_ass = pm.checkBox("CreateAss", label = "Create .ass (non-functional)", ed = True, value = curLockSample)
        createass = pm.checkBox("CreateAss", q = True, v = True)
        mc.setParent("..")

        GenerateFiles = mc.button(label = "Create Batch File", align = "center" , c = lambda *args: self.GenerateFiles())
        mc.showWindow()


    def checkbox_toggled(self, *args):
        slider_enabled = pm.checkBox("Thread", q = True, value = True)
        print slider_enabled
        if slider_enabled:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = False)
        else:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = True)

###################____BROWSE FILE PATH____#######################     
    def BrowseFilePath(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputDirectory, edit = True, text = returnPath)
        print ("Directory found :   " + str(self.inputDirectory))
        if returnPath != None:
            return returnPath

    def CreateString(self, fileDir, outputDir, sequence):
        #sequenceString = "kick -i %s -o %s -of %s" % (fileDir, outputDir, format)
        sequenceString = "Render -r arnold -rd %s %s %s" % (outputDir, sequence, fileDir)
        return sequenceString

    def GenerateFiles(self):
        directory = mc.textField(self.inputDirectory, q = True, text = True)
        assFile = pm.checkBox("CreateAss", q = True, v = True)

        if assFile == True:
            self.SaveAssFile()

        FinalList = []
        for i in self.SequenceList:
            FinalList.append(self.CreateString(self.fileLocation, mc.textField(self.inputDirectory, q = True, text = True), i))
        
        mc.warning("Current Scene:      %s" % (mc.file(q= True, sn = True)))
        mc.warning("Directory:      %s" % (directory))
        mc.warning("Final List:      %s" % (FinalList))
        
        self.WriteFile(directory, FinalList)
        newFile = os.path.join(directory, mc.textField("filename", q= True, text = True))
        
        if os.path.isfile(newFile):
            mc.deleteUI("Confirm_batch_file_settings")
            print("Batch file created successfully")
        else:
            mc.warning("Error when creating file")
            

    def WriteFile(self, directory, createString):
        output = open(directory + '/' + mc.textField("filename", q= True, text = True), 'w')
        for item in createString:
            output.write("%s\n" % item)
        output.write("%s\n" % "pause")

    def SaveAssFile(self):
        print ''
        #Find directory
        #Export entire scene




