import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import sys as sys
from functools import partial as ftool
import shutil as shutil
from os import listdir
import os



###################____"STORE"_FRAMES_FROM_RENDER_TO_SPEFICIC_FOLDER____#######################  
def copyTree(src, dst, folderName, name, rendLayer, symlinks=False, ignore=None):
    # Print "folder/scene name" + folderName
    # Print "name:   " + name
    # Print "render layer:    " + rendLayer
    # Find render layer
    foundLayer = False
    
    # Get the list of all render layers
    render_layer_list = mc.ls(type="renderLayer")
    folderRL = rendLayer
    
    if "rs_" in rendLayer:
        print"'rs_' found in layer already. Nice."
    else:
        rendLayer = "rs_" + rendLayer
        mc.warning("'rs_' not found in render layer, added rs_ to front of string")
        mc.warning ("new string =    " + rendLayer)
        
    for i in render_layer_list:
        if i == rendLayer:
            foundLayer = True
            break
    else:
        print 'render layer match has not been found'

    if foundLayer == False:
        rendLayer = "defaultRenderLayer"
        print "render layer was not found. Defaulted to defaultRenderLayer. rendLayer = " + rendLayer

    # Create new folder in dst path, named after assigned scene
    if folderName != "" and folderName != None:    
        dst = dst + "/" + folderName
    else:
        folderName = "Default_Scene"
        dst = dst + "/" + folderName

    # Check to see if path already exists, otherwise create a new path. This is based off of the Scene input field
    if os.path.isdir(dst):
        print ("Huzzah! Destination folder already exists. Congrats.")
    else:
        mc.sysFile(dst, makeDir=True )
        print ("Huzzah! Created a folder for you. Congrats.")
        
    # Check to see if sequence used a render layer
    # Did not use a render layer
    if rendLayer == "defaultRenderLayer":

        for parentDir in os.listdir(src):
            s = os.path.join(src, parentDir)
            d = os.path.join(dst, parentDir) # Joints src and scene folder together
            recursivePath = s
            savedPath = d

            if os.path.isfile(s):
                for i in os.listdir(src):
                    CopyToLoc(s,d,1)
                
            else:
                for x in os.listdir(recursivePath):
                    s = os.path.join(recursivePath, x)
                    d = os.path.join(savedPath, x)
                    # Move images from parent directory
                    if os.path.isfile(s):
                        if os.path.isdir(savedPath):
                            CopyToLoc(s,d,1)
                        else:
                            mc.sysFile(savedPath, makeDir=True )
                            CopyToLoc(s,d,1)
                    else:
                        mc.warning("No files found in path: %s. Unable to copy. " % s)

    # Did use a render layer     
    else:     
        match = folderRL
        curParentDirectory = ""
        parentDirectory = os.path.abspath(os.path.join(src, os.pardir))
        print "Match:  " + match
        mc.warning("src:    " + src)
        mc.warning("folderRL:   " + str(folderRL))
        mc.warning("parent dir:   " + str(os.path.abspath(os.path.join(src, os.pardir))))
        # If a render layer is used : 
        # This finds the base-level directory
        
        #if os.path.exists(src):
         #   existPath = src
        #elif os.path.exists(parentDirectory):
        #    existPath = parentDirectory
        #else:
         #   existPath = src
        existPath = parentDirectory
        
        if os.path.exists(existPath) and os.listdir(existPath) != []:
            for parentDir in os.listdir(existPath):
                mc.warning("parentDir - line 105 " + parentDir)
                mc.warning("parentDir - line 106 " + existPath)
                if parentDir == match: # Does the parent directory match the render layer name?
                    curParentDirectory = os.path.join(existPath, parentDir)
                    print "curParentDirectory:    " + curParentDirectory
                    s = os.path.join(existPath, parentDir)
                    d = os.path.join(dst, parentDir)


                    # Check that folder does not already exist in destination directory
                    if os.path.isdir(d): # Proceed to copy files individually
                        # Finds children in specified folder, used when copytree will fail
                        if curParentDirectory != "":
                            # Search list for correct parent directory
                            for childDir in os.listdir(curParentDirectory):
                                print "found child:    " + childDir
                                s = os.path.join(curParentDirectory, childDir)
                                print "Source:        " + s
                                print "Destination:   " + d
                                CopyToLoc(s, d, 1, childDir)
                            break
                    else:                #use copyTree instead
                        CopyToLoc(s,d, 0, "")
                        break
        else:
            mc.warning("Path %s does not exist or does not have any relevant children. Skipping copy to location function." % (src))

# This function can be reused to copy files from one directory to another. Must input full paths in order for it to work.
def CopyToLoc(s,d, type, name): ###  0 == copytree :::  1 == copy individually

    checkPath = os.path.dirname(s)    
    if(checkPath == d):  
        print ''
        mc.warning("Found attempt to copy same file to itself. Skipping.\r       Source: %s\r       Destination: %s" %(s,d))
    else:
        if type == 0:
            print""
            try:
                shutil.copytree(s, d, symlinks=False, ignore=None)
            except IOError, e:
                print "Unable to copy file. %s" % e
                
            if os.path.exists(s):
                
                mc.warning("d:    %s" % (d))
                mc.warning("s:    %s" % (s))
                
                if os.path.isdir(d):
                    mc.warning ("Confirmed that %s has been successfully copied into target directory. Proceeding to delete tmp file." % s)
                    DeleteTemp(d)  # Deletes entire temp folder after it has been successfully copied to other directory
                    print "Successfully Copied 1 Sequence to new directory :::  %s" % d
                else:
                    mc.warning("%s does not exist.\n Cannot remove something that does not exist. What am I, a magician?" % d)
            
        elif type == 1:
            # Move images from parent directory
            print "\rSRC:  %s\rDST: %s\r" % (s,d)

            if os.path.exists(s):
                
                try:
                    shutil.copy2(s,d)
                except IOError, e:
                    print "Unable to copy file. %s" % e
                    
                mc.warning("d:    %s" % (d))
                mc.warning("s:    %s" % (s))
                
                if name != None:
                    mc.warning("joined path:    %s" % (os.path.join(d, name)))
                    if os.path.isfile(os.path.join(d, name)):
                        print ("ConfirmeDestination folder existsd that %s has been successfully copied into target directory. Proceeding to delete tmp file." % s)
                        DeleteTemp(os.path.abspath(os.path.join(s, os.pardir)))  # Deletes file in temp folder after it has been successfully copied to other directory
                        print "Successfully Copied 1 Sequence to new directory :::  %s" % d
                    else:
                        mc.warning ("No file found at destination. Aborting delete. Check tmp folder for rendered files.")
                        mc.warning ("isFile(d):     %s" % d)
            else:
                mc.warning("\n\n%s does not exist.\nCannot remove something that does not exist. What am I, a magician?\n\n" % d)
    
def DeleteTemp(tempPath):
    projectRoot = mc.workspace(q=1,rd=1)
    tempFolder = projectRoot + "images/tmp/"
    
    print "path:   :" + str(tempPath)
    print "tempFolder:   :" + str(tempFolder)
    
    import os.path as path
    import sys
    from os.path import expanduser
    user = (os.path.abspath(os.path.join(expanduser("~"), '..' )))
    conda = "Anaconda2\envs\Python27\Lib\site-packages"
    pythonPath = os.path.join(user, conda)
    sys.path.append(pythonPath)

    print user
    print conda
    print pythonPath

    from send2trash import send2trash
    #delete temp
    
    if os.path.isfile(tempPath):
        #os.remove(path)  # remove the file and send to trash
        send2trash(tempPath)
    elif os.path.isdir(tempPath):
        #shutil.rmtree(path)  # remove dir and all contains
        send2trash(tempPath)
    else:
        mc.warning("file {} is not a file or dir.".format(tempPath))
        
    if os.path.isfile(tempFolder):
        #os.remove(tempFolder)  # remove the file
        send2trash(tempFolder)
    elif os.path.isdir(tempFolder):
        #shutil.rmtree(tempFolder)  # remove dir and all contains
        send2trash(tempFolder)
    else:
        mc.warning("file {} is not a file or dir.".format(tempFolder))
            
def CopyExisting(s,d):
    mc.warning( "CopyExisting()   SRC ::  " + s)
    mc.warning( "CopyExisting()   DST ::  " + d)
    try:
        shutil.copytree(s, d, symlinks=False, ignore=None)
    except IOError, e:
        print "Unable to copy file. %s" % e    
    
    if os.path.exists(s):
        if os.path.isdir(d):
            mc.warning ("Confirmed that %s has been successfully copied into target directory. Officially ready for rendering with skipping frames active." % s)
            print "Successfully Copied 1 Sequence folder to new directory :::  %s" % d
        else:
            mc.warning("%s does not exist. Cannot remove something that does not exist. No frames are being skipped during rendering" % d)
