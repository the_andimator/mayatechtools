import maya.cmds as mc

selList = mc.ls(selection = True, transforms = True)
rigJoint = selList[0]
skinJoint = selList[1]

def GetLists(rigJoint, skinJoint):
    rigList = mc.listRelatives(rigJoint, ad = True, type = 'joint')
    skinList = mc.listRelatives(skinJoint, ad = True, type = 'joint')
    
    length = "Rig List Length  {0}    :     Skin List Length  {1}".format(len(rigList), len(skinList))
    print length
    
    index = 0
    for x in rigList:
        if index >= len(skinList):
            statement = "RIG:   {0}     SKIN:   None".format(x)
        else:
            statement = "RIG:   {0}     SKIN:   {1}".format(x, skinList[index])

        print statement
        
        ConstrainJoints(x, skinList[index])
        index += 1

def ConstrainJoints(rig, skin):
    mc.pointConstraint(rig, skin, mo = False)
    mc.orientConstraint(rig, skin, mo = False)


def MatchJoints(rig, skin):
        
    #object name.rpartition(':')[2]
    rig = name.rpartitionf(':')[2]
    print rig
    
    # using list comprehension  
    # to get string with substring  
    matchedName = [i for i in skin if rig in i] 
      
    # printing result  
    print ("All strings with given substring are : " + str(res)) 
    
    return matchedName
    
    #return the matched joints in a string array
    
    
GetLists(rigJoint, skinJoint)


selList = mc.ls(selection = True, transforms = True)
jointList = mc.listRelatives(selList[0], ad = True, type = 'joint')

def GetSelectedJointHierarchy(jointList):
    print 
    joint_list = mc.listRelatives(rigJoint, ad = True, type = 'joint')
    mc.select(jointList, add = True)


GetSelectedJointHierarchy(jointList)
#pointConstraint;
#orientConstraint;