import maya.cmds as mc

selList = mc.ls(selection = True, transforms = True)

rigJoint = selList[0]
skinJoint = selList[1]

def MatchJoints(rig, skin):
        
    #object name.rpartition(':')[2]
    jointName = rig.rpartition('SKIN_')[2]
    print "JOINT NAME:     " + str(jointName)

    mylst = map(lambda each:each.rpartition(":")[2], skin)
    print mylst
    
    # using list comprehension  
    # to get string with substring  
    
    matchedName = [i for i in mylst if str(jointName) in i] 
    
    return matchedName
    
    #return the matched joints in a string array

skinList = mc.listRelatives(skinJoint, ad = True, type = 'joint')


print str(MatchJoints(rigJoint, skinList))


