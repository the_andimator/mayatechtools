import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import sys as sys
from functools import partial as ftool
import shutil as shutil
from os import listdir
import os
import re

class StoreSequences(object):

    def AddNewSequence(self):
        mc.select(d = True)
        #Creates new sequence group
        if mc.objExists("Scene_Sequence_Timing"):
            seqGroup = self.CreateSeqGroup()
            if seqGroup != None:
                #Adds attributes
                self.CreateSequenceAttributes()
                #Add to container
                self.PublishNodes(seqGroup)
        else:
            self.CreateContainer()
            seqGroup = self.CreateSeqGroup()
            if seqGroup != None:
                #Adds attributes
                self.CreateSequenceAttributes()
                #Add to container
                self.PublishNodes(seqGroup)
            
    def CreateSeqGroup(self):
        newName = self.FindName()
        if newName == None:
            mc.warning('newName == Nonetype')
        else:
            #Create/Name node
            test = mc.createNode( 'transform', n = newName)
        return newName
  
            
    def FindName(self):
        curSequences = mc.ls("Sequence_*")
        i = 0
        print curSequences
        if curSequences == []:
            return 'Sequence_1'
        else:
            for seq in curSequences:
                if seq == "Sequence_" + str(i+1):
                    i = i + 1
                    print "first if"    
                elif seq != "Sequence_" + str(i+1):
                    newName = "Sequence_" + str(i+1)
                    print "found new name"
                    return newName
                if i == len(curSequences):
                    i = i + 1
                    newName = "Sequence_" + str(i)
                    print newName
                    print "append new name to end"
                    return newName


    def PublishNodes(self, seq):
        self.LockNode("Scene_Sequence_Timing", False)
        
        number = self.find_numbers(seq)
        #Add sequence to container
        mc.container("Scene_Sequence_Timing",edit = True, addNode=[seq])
        mc.select("Scene_Sequence_Timing")
        #Publish specific nodes to container
        mc.container(edit = True, pb = (seq + ".CameraName" , "CameraName" + number))
        mc.container(edit = True, pb = (seq + ".Start" , "Start"+ number))
        mc.container(edit = True, pb = (seq + ".End" , "End"+ number))
        mc.container(edit = True, pb = (seq + ".RenderLayer" , "RenderLayer"+ number))
        mc.container(edit = True, pb = (seq + ".Prefix" , "Prefix"+ number))
        mc.container(edit = True, pb = (seq + ".Scene" , "Scene"+ number))
        mc.container(edit = True, pb = (seq + ".FileDirectory" , "FileDirectory"+ number))
        mc.container(edit = True, pb = (seq + ".SkipExisting" , "SkipExisting"+ number))
        mc.container(edit = True, pb = (seq + ".RenderStatsDir" , "RenderStatsDir"+ number))

        self.LockNode("Scene_Sequence_Timing", True)

      
    def CreateContainer(self):
            mc.container(name = "Scene_Sequence_Timing", includeShapes = True, includeTransform = True, force = True, addNode = mc.ls(selection = True))
            #Enable Black Box
            mc.setAttr("Scene_Sequence_Timing.blackBox", 1)
            self.LockNode("Scene_Sequence_Timing", True)
                
    def RebuildContainer(self):
        if mc.objExists("Scene_Sequence_Timing"):
            result = mc.confirmDialog(
            title='Sequence Timing Node Exists',
            messageAlign = 'center',
            message='Sequence timing node already exists, are you sure you want to replace this? You will lose all stored values.',
            button=["Create New Node", 'Cancel'],
            defaultButton="Create New Node",
            cancelButton='Cancel',
            dismissString='Cancel',
            bgc = [1.0,.4,.4])

            if result == "Create New Node":
                self.LockNode("Scene_Sequence_Timing", False)
                mc.delete("Scene_Sequence_Timing")
                #This should not add the selected nodes, instead create an array using a select name* type function
                #Creates Node
                mc.container(name = "Scene_Sequence_Timing", includeShapes = True, includeTransform = True, force = True, addNode = mc.ls(selection = True))
                #Enable Black Box
                mc.setAttr("Scene_Sequence_Timing.blackBox", 1)
                seqGroup = self.CreateSeqGroup()
                if seqGroup != None:
                    #Adds attributes
                    self.CreateSequenceAttributes()
                    #Add to container
                    self.PublishNodes(seqGroup)
        else:
            mc.container(name = "Scene_Sequence_Timing", includeShapes = True, includeTransform = True, force = True, addNode = mc.ls(selection = True))
            mc.setAttr("Scene_Sequence_Timing.blackBox", 1)

    def RenameNode(self):
        seqCount = int(numSequences)
        SequenceTitle = "Sequence %s"%(seqCount+1)
        name = "Sequence_1"
        selected = mc.ls(sl= True)
        mc.rename(selected, name)
        print name
        mc.select(name)
        
    def AddRenderButton():
        print ''
        mc.addAttr( shortName='ff', longName='forcefield', dataType='matrix', multi=True )

    def CreateSequenceAttributes(self):
        #Attributes need to have unique names, it would be easiest to simply append a number based on the sequence name (_1, _2, _3)
        mc.addAttr(ln = 'CameraName', dt = 'string', s = True)
        mc.addAttr(ln = 'Start', at = 'short', dv = 0, hsn = 0, min = 0, smx = 5000, hnv = True, hxv = False, s = True)
        mc.addAttr(ln = 'End', at = 'short', dv = 0, hsn = 0, min = 0, smx = 5000, hnv = True, hxv = False, s = True)
        mc.addAttr(ln = 'RenderLayer', dt = 'string', s = True)
        mc.addAttr(ln = 'Prefix', dt = 'string', s = True)
        mc.addAttr(ln = 'Scene', dt = 'string', s = True)
        mc.addAttr(ln = 'FileDirectory', dt = 'string', s = True)
        mc.addAttr(ln = 'SkipExisting', attributeType = 'bool', s = True)
        mc.addAttr(ln = 'RenderStatsDir', nn = "Render Stats Directory", dt = 'string', s = True, r = True)
        #mc.setAttr('AvgRenderTime', 2, "hours", "minutes", type = 'stringArray')


    def RenumberSequences(self):
        print ''

    def DeleteSpecificSequence(self):
        print ''
        
    def LockNode(self, obj, set):
        if mc.objExists(obj):
            mc.lockNode(obj, lock = set)
        else:
            print "Sequence node does not exist"
            
    def DeleteNode(self):
        
        if mc.objExists("Scene_Sequence_Timing"):
            result = mc.confirmDialog(
            title='Attempt to Delete Sequence Node',
            messageAlign = 'center',
            message='Deleting this node will remove all sequence information from scene. Are you sure?.',
            button=["Delete", 'Cancel'],
            defaultButton="Delete",
            cancelButton='Cancel',
            dismissString='Cancel',
            bgc = [1.0,.4,.4])

            if result == "Delete":
                mc.Delete("Scene_Sequence_Timing")

    def StoreSequenceInstance(self):
        #Find sequences in scene and store inside array
        #Maybe find the name of the asset node and try to select children
        print ''
 
    def find_numbers(self, input_str):            
        rem = input_str.replace("_", " ")
        newNum = (re.findall('\d+', rem ))
        print newNum
        return newNum[0]

    def GenerateBatchFile(self):
        print ''
        
    def DisableBlackBox(self):
        if mc.objExists("Scene_Sequence_Timing"):
            val = mc.getAttr("Scene_Sequence_Timing.blackBox")
            
            if val == 1:
                mc.setAttr("Scene_Sequence_Timing.blackBox", 0)
            else:
                mc.setAttr("Scene_Sequence_Timing.blackBox", 1)
        else:
            print 'Sequence node does not exist'

#Scene_Sequence_Timing Edits
    #Add folder button to File Directory option in nodes
    #Find way to add icon to outliner
    #Create a render layer list in attribute editor

#Render Window
    #Window opens when selecting 'render' button
    #Each sequence list has the render layer, start/end frames, renderable, and priority order
    #Render button that starts the sequencer
    #
    
class AutofillInformation(object):
    
    def __init__(self, subfolder):
        
        self.subfolder = subfolder
        
        if mc.objExists("Scene_Sequence_Timing"):
            try:
                mc.deleteUI(setupWin, window = True)
            except:
                pass
            self.windowName = windowName = "%s_Autofill_Directory" % (subfolder)

            if(mc.window(windowName, exists=True)):
                mc.deleteUI(windowName)
           
            self.setupWin = mc.window(self.windowName, title= self.windowName, w =510, h = 85, mnb = True, mxb = True, s = 0, rtf = True)
            mainLayout = mc.columnLayout(w = 510, h= 85)
        
    
    def AutofillDirectory(self, subfolder):
        if mc.objExists("Scene_Sequence_Timing"):
        
            mc.text('')
            
            mc.setParent("..")
            ###_Image_Directory_###
            self.directoryText = str(mc.workspace(q=True, rd = True)) + subfolder.lower()
            
            rowColumnLayout = mc.rowColumnLayout(nc = 5, cw = [(1,140), (2,315), (3,30), (4,40), (5,60)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4), (5, "both",5)])
            mc.text(label = "%s Directory : " % (subfolder), align = "right") 
            self.inputDirectory = mc.textField(pht='C:\Users\Joe_Danger\Documents\maya\projects\default', w = 285 , text = self.directoryText) 
            icon = mc.internalVar(upd = True) + "/icons/folder.png"
            self.inputBrowseButton = mc.symbolButton(w = 20, h = 20, image = icon, ebg = True, c = lambda *_: self.BrowseFilePath(3, None, self.inputDirectory)) 
            mc.text('')
            
            mc.setParent("..")
            mc.text (label = "") 

            autoButtons = mc.rowColumnLayout(nc = 4, cw = [(1,140), (2,100), (3,20), (4,100)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4)])
            mc.text (label = "") 
            mc.button(label = "Apply", command = lambda *_: self.ApplyGlobalDirectory(subfolder))
            mc.text (label = "") 
            mc.button(label = "Cancel", command = lambda *_: self.DeleteUI())

            
            mc.showWindow()
        else:
            mc.warning("Sequence node does not exist. Unable to open up Autofill directory window")

    def BrowseFilePath(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputDirectory, edit = True, text = returnPath)
        self.directoryText = returnPath
        print ("Directory found :   " + str(self.inputDirectory))
        return returnPath
        
    def DeleteUI(self):
        if(mc.window(str("%s_Autofill_Directory" % (self.subfolder)), exists=True)):
            mc.deleteUI(str("%s_Autofill_Directory" % (self.subfolder)))
            
            
    def ApplyGlobalDirectory(self, subfolder):
        rendSelect = mc.ls("Scene_Sequence_Timing", r= True)
        #Find child nodes, store in list
        #renderNodes = mc.listRelatives(rendSelect, c = True, shapes=True)
        renderNodes = mc.container("Scene_Sequence_Timing",q = True, nl = True)
        
        for i in renderNodes:
            if subfolder == "Images":
                mc.setAttr(i + ".FileDirectory", self.directoryText , type = "string")
            elif subfolder == "RenderLogs":
                mc.setAttr(i + ".RenderStatsDir", self.directoryText , type = "string")
        print 'Applied new directory. Closing window.'
        self.DeleteUI()
            
            
            
            
            
            
            
            
            
            
            
            
            
