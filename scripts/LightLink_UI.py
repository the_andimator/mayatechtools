from __future__ import division
import sys
import os
import maya.cmds as mc
import pymel.core as pm

python = "/Anaconda2\envs\Python27/"
sitepackages = "Lib\site-packages"
lib = "Lib"
uifile = "\maya/2018/ui\LightLinker\LightGroups.ui"

sys.path.append("C:\Users/" + os.environ['USER'] + python + sitepackages)
sys.path.append("C:\Users/" + os.environ['USER'] + python + lib)
sys.path.append("C:\Users/" + os.environ['USER'] + python)
sys.path.append("C:\Users/" + os.environ['USER'] + "/Documents/Qt.py")
ui_filename =  os.path.expanduser('~') + uifile


from Qt import QtWidgets, QtCore, QtGui
from Qt.QtWidgets import QWidget, QApplication
from Qt import QtCompat
from Qt.QtCore import QMimeData
import sip
import maya.cmds as mc
import pymel.core as pm
from pymel import *
from shiboken2 import wrapInstance
import shiboken2


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent = None):
        super(MainWindow, self).__init__()
        global main_widget 
        main_widget = QtCompat.loadUi(ui_filename)
        self.setCentralWidget(main_widget)

        self.setObjectName('myWindow')
        self.setWindowTitle("Light Groups")
        self.setWindowFlags(QtCore.Qt.BypassGraphicsProxyWidget|
                    QtCore.Qt.WindowCloseButtonHint|
                    QtCore.Qt.Window)

        self.form_widget = FormWidget(self)
        self.form_widget.RefreshUI()


class FormWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super(FormWidget, self).__init__(parent)

    def RefreshUI(self):
        items = mc.ls(sl=True)
        sceneLights = mc.ls(type = "light")

        main_widget.scroll_HDRI_Lights.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        main_widget.scroll_HDRI_Lights.setDefaultDropAction(QtCore.Qt.MoveAction)
        main_widget.scroll_HDRI_Lights.setSortingEnabled(True)
        main_widget.button_apply_changes.clicked.connect(lambda *_: self.ApplyChangesToScene())

        main_widget.scroll_HDRI_Lights.addItems(self.OrganizeLightsInScene("HDRI", sceneLights))
        main_widget.scroll_Misc_Lights.addItems(self.OrganizeLightsInScene("Misc", sceneLights))
        main_widget.scroll_Default_Lights.addItems(self.OrganizeLightsInScene("Default", sceneLights))
        main_widget.scroll_Unassigned_Lights.addItems(self.OrganizeLightsInScene("Unassigned",sceneLights))

    def ApplyChangesToScene(self):
        HDRI = []
        Misc = []
        Default = []
        Unassigned = []

        HDRI = self.GetLists(main_widget.scroll_HDRI_Lights)
        Misc = self.GetLists(main_widget.scroll_Misc_Lights)
        Default = self.GetLists(main_widget.scroll_Default_Lights)
        Unassigned = self.GetLists(main_widget.scroll_Unassigned_Lights)

        print HDRI
        print Misc
        print Default
        print Unassigned

        self.SetValues(Unassigned, 0)
        self.SetValues(HDRI, 1)
        self.SetValues(Default, 2)
        self.SetValues(Misc, 3)

    def GetLists(self, item):
        list = []
        for x in range(item.count()):
            list.append(QtWidgets.QListWidgetItem.text(item.item(x)))
        return list

    def SetValues(self, list, num):
        for x in list:
            mc.setAttr( str(x + '.Light_Group_Assigned'), num)

    def OrganizeLightsInScene(self, TypeofLight, sceneLights):
        returnLights = []

        for x in sceneLights:
            attrExist = pm.attributeQuery("Light_Group_Assigned" , node = x, exists = True)
            if attrExist == False:
                try:
                    pm.addAttr(x, ln = 'Light_Group_Assigned', at = 'enum', en = 'Unassigned:HDRI:Default:Misc', r = True)
                except:
                    mc.warning("Error when attempting to add attribute to object:  " + x)

            val = mc.getAttr( str(x + '.Light_Group_Assigned'), asString=True)
            if val == TypeofLight:
                returnLights.append(x)

        if len(returnLights) == 0: 
            mc.warning("No types found for " + TypeofLight)
        else:
            mc.warning("Found {0} lights categorized under '{1}' type" .format(len(returnLights), TypeofLight))

        return returnLights


def main():
    import LightLink_UI as LLui
    reload(LLui)

    global LL_window
    try:
        LL_window.close()
    except:
        pass

    app = QtWidgets.QApplication.instance()
    LL_window = LLui.MainWindow()
    LL_window.show()
    #sys.exit(app.exec_())


if __name__ == "__main__":
    main()



