import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
import maya.OpenMaya as OpenMaya

global highlightingIsActive 
highlightingIsActive = False

class DisplayMetalObjects(object):

    def __init__(self, isActivated):
        LocateHDRI = pm.ls('HDRI_SkydomeLight.', r = True)

        if LocateHDRI:
            if LocateHDRI[0]:
                lightName = str(LocateHDRI[0])
                self.lightName = lightName
        else:
            self.lightName = None

        active = isActivated
        self.active = active

    def HighlightMetalObjects(self):
        curObj = mc.lightlink(q = True, light = self.lightName, t = 0, shapes = 1, set = 0)
        meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True)
        unaffectedMesh = self.UnaffectedObjectList(meshes, curObj)

        for x in curObj:
            item = x + ".displayVertices"
            if mc.objExists(item):
                mc.setAttr(item, 1)
                mc.setAttr(x + ".vertexSize", 1.5)
                mc.setAttr(x + ".overrideEnabled", 1)
                mc.setAttr(x + ".overrideColor", 9)

        for i in unaffectedMesh:
            mesh = i + ".displayVertices"
            if mc.objExists(mesh):
                mc.setAttr(mesh, 0)
                mc.setAttr(i + ".vertexSize", 3)
                mc.setAttr(i + ".overrideEnabled", 0)
                mc.setAttr(i + ".overrideColor", 0)    

    def RemoveHighlighting(self):
        meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True)
        for i in meshes:
            mesh = i + ".displayVertices"
            if mc.objExists(mesh):
                mc.setAttr(mesh, 0)
                mc.setAttr(i + ".vertexSize", 0)
                mc.setAttr(i + ".overrideEnabled", 0)
                mc.setAttr(i + ".overrideColor", 0)

    def UnaffectedObjectList(self, meshes, curObj):
        DisableThese = list(set(meshes) - set(curObj))
        return DisableThese

    def ReinitializeHDRILight(self):
        LocateHDRI = pm.ls('HDRI_SkydomeLight.', r = True)
        if LocateHDRI:
            if LocateHDRI[0]:
                lightName = str(LocateHDRI[0])
        else:
            lightName = None

        return lightName

    def DecisionMaker(self):
        #Attempt to find the HDRI again
        if self.lightName == None:
            mc.warning("Attempting to find HDRI")
            self.lightName = self.ReinitializeHDRILight()

            if self.lightName != None:
                mc.warning("Found HDRI map")
            else:
                mc.warning("Unable to find HDRI, aborting process..")

        if self.lightName != None:
            if self.active == True:
                self.HighlightMetalObjects()
            else:
                self.RemoveHighlighting()


#import DisplayMetalObjects_singular as DMO
#reload (DMO)