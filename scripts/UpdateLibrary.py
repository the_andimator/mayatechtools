# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UpdateLibrary.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtWidgets, QtGui

class Ui_UpdateLibrary(object):
    def setupUi(self, UpdateLibrary):
        UpdateLibrary.setObjectName("UpdateLibrary")
        UpdateLibrary.resize(333, 233)
        self.centralwidget = QtWidgets.QWidget(UpdateLibrary)
        self.centralwidget.setObjectName("centralwidget")
        self.pullLatest = QtWidgets.QPushButton(self.centralwidget)
        self.pullLatest.setGeometry(QtCore.QRect(20, 90, 141, 31))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 255, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        self.pullLatest.setPalette(palette)
        self.pullLatest.setAutoFillBackground(True)
        self.pullLatest.setStyleSheet("border-color: rgb(0, 255, 0);")
        self.pullLatest.setObjectName("pullLatest")
        self.pushLatest = QtWidgets.QPushButton(self.centralwidget)
        self.pushLatest.setGeometry(QtCore.QRect(20, 130, 141, 31))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(197, 85, 87))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(197, 85, 87))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(197, 85, 87))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        self.pushLatest.setPalette(palette)
        self.pushLatest.setAutoFillBackground(True)
        self.pushLatest.setObjectName("pushLatest")
        self.newJson = QtWidgets.QCheckBox(self.centralwidget)
        self.newJson.setGeometry(QtCore.QRect(20, 40, 151, 17))
        self.newJson.setObjectName("newJson")
        self.newBackup = QtWidgets.QCheckBox(self.centralwidget)
        self.newBackup.setGeometry(QtCore.QRect(20, 50, 191, 31))
        self.newBackup.setObjectName("newBackup")
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setGeometry(QtCore.QRect(20, 195, 291, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 170, 291, 21))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.rebuild = QtWidgets.QCheckBox(self.centralwidget)
        self.rebuild.setGeometry(QtCore.QRect(20, 15, 191, 31))
        self.rebuild.setObjectName("rebuild")
        UpdateLibrary.setCentralWidget(self.centralwidget)

        self.retranslateUi(UpdateLibrary)
        QtCore.QMetaObject.connectSlotsByName(UpdateLibrary)

    def retranslateUi(self, UpdateLibrary):
        _translate = QtCore.QCoreApplication.translate
        UpdateLibrary.setWindowTitle(_translate("UpdateLibrary", "MainWindow"))
        self.pullLatest.setText(_translate("UpdateLibrary", "Pull Latest From MVT"))
        self.pushLatest.setText(_translate("UpdateLibrary", "Push Changes to MVT"))
        self.newJson.setText(_translate("UpdateLibrary", "Create new favs.json"))
        self.newBackup.setText(_translate("UpdateLibrary", " Backup MVT Library (Promax)"))
        self.label.setText(_translate("UpdateLibrary", "Progress"))
        self.rebuild.setText(_translate("UpdateLibrary", "Rebuild Local Library"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    UpdateLibrary = QtWidgets.QMainWindow()
    ui = Ui_UpdateLibrary()
    ui.setupUi(UpdateLibrary)
    UpdateLibrary.show()
    sys.exit(app.exec_())

