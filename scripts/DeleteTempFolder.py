#Purge the temp folder from user/maya/projects/images/tmp
#import necessary plugins
import os
import maya.cmds as mc
import shutil as shutil
from os import listdir
import os.path


class DeleteTemp(object):
    
    def RunDialog(self):
        projectRoot = mc.workspace(q=1,rd=1)
        src = projectRoot + "images/tmp/"
        python = "/Anaconda2\envs\Python27/"
        sitepackages = "Lib\site-packages"
        lib = "Lib"
    
        result = mc.confirmDialog(
        title="Attemping to permanently delete tmp folder",
        icon = "warning",
        message="Would you like to delete the temp folder at this directory: \n %s" % (src),
        messageAlign = 'left',
        button=['Delete', "Cancel"],
        defaultButton="Cancel",
        cancelButton='Cancel',
        dismissString='Cancel',
        bgc = [1.0,.4,.4])

        if result == 'Delete':
            for the_file in os.listdir(src):
                file_path = os.path.join(src, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path): shutil.rmtree(file_path)
                except Exception as e:
                    print(e)    