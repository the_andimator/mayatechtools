import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
###########################

#Create a button using this bit of code:

#import maya.cmds as mc
#import maya.mel as mel
#import ReattachBodyMaterials as RBM
#reload(RBM)
#RBM.ReapplyMaterials("Human_Anatomy_Rig_test:")
#mc.select (cl = True)


###########################
               
def ReapplyMaterials():
    matA = "matA"
    matB = "matB"
    matC = "matC"
    matD = "matD"
    skel = "skelMat"
    
    QuickSelect = ["matA", "matB", "matC", "matD", "skelMat"]
    Materials = ["Mat_MuscleTone_A", "Mat_MuscleTone_B", "Mat_MuscleTone_C", "Mat_MuscleTone_D", "Skeleton_A1"]
    m = 0
    
    for i in QuickSelect:
        i = ConfirmExistence(i)
        mat = ConfirmExistence(Materials[m])
        mc.select(i)
        mc.hyperShade(assign = mat)
        
        m = m + 1

def ConfirmExistence(obj):
    if mc.objExists(obj):
        print "This object exists already. Returning default value"
        return obj
    else:
       newObj = FindNamespace(obj)

       if mc.objExists(newObj):
           return newObj
       else:
           return obj

           
def FindNamespace(obj):
   #list namespaces, iterate until objexists
   curNmspce = mc.namespaceInfo(lon = True)
   newObj = ""
   for i in curNmspce:
       newObj = i + ":" + obj
       if mc.objExists(newObj):
           print ("Found newObj:   %s" % (newObj))
           return newObj
       else:
           mc.warning("No obj found named %s. Returning original input value of %s" % (newObj, obj))
           return obj