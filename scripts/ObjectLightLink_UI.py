from __future__ import division
import sys
import os
import maya.cmds as mc
import pymel.core as pm

python = "/Anaconda2\envs\Python27/"
sitepackages = "Lib\site-packages"
lib = "Lib"
uifile = "\maya/2017/ui\LightLinker\ActiveLightLinking.ui"
ui_filename =  os.path.expanduser('~') + uifile
sys.path.append("C:\Users/" + os.environ['USER'] + python + sitepackages)
sys.path.append("C:\Users/" + os.environ['USER'] + python + lib)
sys.path.append("C:\Users/" + os.environ['USER'] + python)
sys.path.append("C:\Users/" + os.environ['USER'] + "/Documents/Qt.py")
ui_filename =  os.path.expanduser('~') + uifile

import ObjectLightLink_UI as OLLui
from Qt import QtWidgets, QtCore, QtGui
from Qt.QtWidgets import QWidget, QApplication
from Qt import QtCompat
from Qt.QtCore import QMimeData
from Qt.QtGui import QCursor 
import Qt
import sip
from pymel import *
from shiboken2 import wrapInstance
import shiboken2
import ActiveLightLinking as customUI
import math
from decimal import *

import sys, pprint
from PyQt5 import uic
from pysideuic import compileUi
import maya.OpenMayaUI as omui
import maya.OpenMaya as OpenMaya

def maya_main_window():
    main_window_ptr = omui.MQtUtil.mainWindow()
    return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, parent = None):
        super(MainWindow, self).__init__()
        self.setWindowFlags(QtCore.Qt.Tool)
        global main_widget
        global OLL_window
        global WindRef
        WindRef = self
        main_widget = self.ui =  customUI.Ui_MainWindow()
        main_widget.setupUi(self)
        self.setObjectName('BP Mesh Light Linking')
        self.setWindowTitle("BP Mesh Light Linking")
        self.setWindowFlags(QtCore.Qt.BypassGraphicsProxyWidget|
                            QtCore.Qt.WindowCloseButtonHint|
                            QtCore.Qt.Window)
        self.form_widget = MeshLightLinking(self)
        self.form_widget.GenerateObjList()


###################################################################
###############______MESH LIGHT LINKING CLASS___###################
###################################################################
class MeshLightLinking(QtWidgets.QWidget):

    def __init__(self, parent):
        super(MeshLightLinking, self).__init__(parent)

    def GenerateObjList(self):
        attributes = ["HDRI", "Default", "Misc", "All"]
        #this collects the scene meshes and organizes them into their corresponding boxes (hdri, default, etc) 
        HDRI = []
        Default = []
        Misc = []
        All = []
        Unassigned = []

        #grabs all geometry and checks the attached "Affected_By" attr. If attr does not exist, the  script adds it with a default value.
        grabAllGeometry = mc.ls(geometry = True, dag = True)

        for mesh in grabAllGeometry:
            for attr in attributes:
                for attr in attributes:
                    group = self.OrganizeObjectsInScene(mesh, attr)
                    value = mc.getAttr( str(mesh + "." + attr))
                    if value == True:
                        if attr == "HDRI":
                            HDRI.append(mesh)
                        if attr == "Default":
                            Default.append(mesh)
                        if attr == "Misc":
                            Misc.append(mesh)
                        if attr == "All":
                            All.append(mesh)

                if value == False:
                    cHDRI = mc.getAttr( str(mesh + ".HDRI"))
                    cDefault = mc.getAttr( str(mesh + ".Default"))
                    cMisc = mc.getAttr( str(mesh + ".Misc"))
                    cAll = mc.getAttr( str(mesh + ".All"))

                    if (cHDRI == False and
                        cDefault == False and
                        cMisc == False and
                        cAll == False):
                        Unassigned.append(mesh)

        #Remove Duplicates
        hdri_ = self.RemoveDuplicatesFromLists(main_widget.scroll_HDRILink, HDRI)
        misc_ = self.RemoveDuplicatesFromLists(main_widget.scroll_MiscLink, Misc)
        default_ = self.RemoveDuplicatesFromLists(main_widget.scroll_DefaultLink, Default)
        all_ = self.RemoveDuplicatesFromLists(main_widget.scroll_AllObjLink, All)
        unassigned_ = self.RemoveDuplicatesFromLists(main_widget.scroll_Unassigned_Objects, Unassigned)

        #scroll lists
        main_widget.scroll_HDRILink.addItems(hdri_)
        main_widget.scroll_MiscLink.addItems(misc_)
        main_widget.scroll_DefaultLink.addItems(default_)
        main_widget.scroll_AllObjLink.addItems(all_)
        main_widget.scroll_Unassigned_Objects.addItems(unassigned_)
        
        #material scroll list
        main_widget.scroll_Materials.addItems(self.FindMaterialList(False))

        #Apply buttons
        main_widget.butto_Applychanges.clicked.connect(lambda *_: self.ApplyChangesToScene())

        #Add buttons
        main_widget.button_AddAll.clicked.connect(lambda *_: self.AddObjToList(mc.ls(sl =True), "All"))
        main_widget.button_AddDefault.clicked.connect(lambda *_: self.AddObjToList(mc.ls(sl =True), "Default"))
        main_widget.button_AddHDRI.clicked.connect(lambda *_: self.AddObjToList(mc.ls(sl =True), "HDRI"))
        main_widget.button_AddMisc.clicked.connect(lambda *_: self.AddObjToList(mc.ls(sl =True), "Misc"))
        main_widget.button_AddUnaffected.clicked.connect(lambda *_: self.AddObjToList(mc.ls(sl =True), "Unaffected"))

        #color swatches on buttons
        main_widget.color_HDRI.clicked.connect(lambda *_: self.SelectColor(main_widget.color_HDRI))
        main_widget.color_Default.clicked.connect(lambda *_: self.SelectColor(main_widget.color_Default))
        main_widget.color_Misc.clicked.connect(lambda *_: self.SelectColor(main_widget.color_Misc))
        main_widget.color_All.clicked.connect(lambda *_: self.SelectColor(main_widget.color_All))

        #highlighting for vertex buttons
        main_widget.button_highlight_HDRI.clicked.connect(lambda *_: self.ActivateVertColors(main_widget.button_highlight_HDRI, main_widget.scroll_HDRILink, True))
        main_widget.button_highlight_Default.clicked.connect(lambda *_: self.ActivateVertColors(main_widget.button_highlight_Default, main_widget.scroll_DefaultLink, True))
        main_widget.button_highlight_misc_group.clicked.connect(lambda *_: self.ActivateVertColors(main_widget.button_highlight_misc_group, main_widget.scroll_MiscLink, True))
        main_widget.button_highlight_Shared.clicked.connect(lambda *_: self.ActivateVertColors(main_widget.button_highlight_Shared, main_widget.scroll_AllObjLink, True))

        #select highlighting vertex buttons
        ##############____highlight buttons only make the verts visible, nothing else____#################
        main_widget.check_HDRI.stateChanged.connect(lambda *_: self.SetStateChange(main_widget.check_HDRI))
        main_widget.check_default.stateChanged.connect(lambda *_: self.SetStateChange(main_widget.check_default))
        main_widget.check_shared_misc_group.stateChanged.connect(lambda *_: self.SetStateChange(main_widget.check_shared_misc_group))
        main_widget.check_shared.stateChanged.connect(lambda *_: self.SetStateChange(main_widget.check_shared))

        #Buttons for selecting meshes 
        main_widget.button_highlight_select_hdri.clicked.connect(lambda *_: self.SelectObjectsInList(main_widget.scroll_HDRILink))
        main_widget.button_highlight_select_default.clicked.connect(lambda *_:self.SelectObjectsInList(main_widget.scroll_DefaultLink))
        main_widget.button_highlight_select_misc.clicked.connect(lambda *_:self.SelectObjectsInList(main_widget.scroll_MiscLink))
        main_widget.button_highlight_select_shared.clicked.connect(lambda *_:self.SelectObjectsInList(main_widget.scroll_AllObjLink))
        
        #Refresh button
        main_widget.butto_Refresh.clicked.connect(lambda *_:self.RefreshUI())
        main_widget.button_highlight_Reimport.clicked.connect(lambda *_:self.ReimportUI())

        main_widget.butto_Refresh_3.clicked.connect(lambda *_:self.SelectHighlightedItems())
        main_widget.button_materials_Refresh.clicked.connect(lambda *_:self.FindMaterialList(True))
        
        main_widget.button_materials_SelectObj.clicked.connect(lambda *_:self.SelectObjectByMaterial())
        
        main_widget.button_materials_Rename.clicked.connect(lambda *_:self.RenameShaderGroups())

    def SelectObjectByMaterial(self):
        selectedMaterials = []
        grabAllGeometry = []
        items = main_widget.scroll_Materials.selectedItems()
        for x in items:
            selectedMaterials.append(x.text())
        for i in selectedMaterials:
            shadingGrp = pm.listConnections(i, type='shadingEngine')
            for test in shadingGrp:
                item = pm.listConnections(test, type='mesh', scn = True)
                for object in item:
                    grabAllGeometry.append(object)
        mc.select(grabAllGeometry)

    def RenameShaderGroups(self):
        #Find selections from list
        selectedMaterials = []

        items = main_widget.scroll_Materials.selectedItems()
        for x in items:
            selectedMaterials.append(x.text())

        for i in selectedMaterials:
            oldName = "".join(i)
            result = mc.promptDialog(
            title='',
            message='Rename Shader Group',
            text = oldName,
            button=["Rename", "Generate from Material",'Cancel'],
            defaultButton="Create",
            cancelButton='Cancel',
            dismissString='Cancel')

            if result == "Rename":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.rename(oldName, newName)
                self.FindMaterialList(True)
            if result == "Generate from Material":
                newName = self.GenerateNameForMat(oldName)
                mc.rename(oldName, newName)
                self.FindMaterialList(True)

    def GenerateNameForMat(self, oldName):
        MAT = mc.listConnections(oldName)
        mc.warning(MAT)
        newName = ""
        for x in MAT:
            searchFor = x + '.outColor'
            if mc.objExists(searchFor):
                newName = x + "SG"
        print newName
        return newName

    def FindMaterialList(self, clear):
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)   
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')
        optimizedGrp = []
        refresh = shadingGrps

        if shadingGrps != None:
            for x in shadingGrps:
                if optimizedGrp != []:
                    if x not in optimizedGrp:
                        print ("%s appended to optimized list" % (x))
                        optimizedGrp.append(x)  
                else:
                    optimizedGrp.append(x)
        shaders = mc.ls(mc.listConnections(optimizedGrp), materials= True)
        mc.warning(shaders)

        if clear == True:
            main_widget.scroll_Materials.clear()
            main_widget.scroll_Materials.addItems(shaders)
            return ''
        return shaders

    def SelectHighlightedItems(self):
        GUI_List = [main_widget.scroll_DefaultLink, 
                    main_widget.scroll_HDRILink, 
                    main_widget.scroll_MiscLink, 
                    main_widget.scroll_Unassigned_Objects,
                    main_widget.scroll_AllObjLink]
        pm.select(r= True)
        
        itemsFromList = []

        for i in GUI_List:
            items = i.selectedItems()
            
            for x in items:
                itemsFromList.append(x.text())
        
        pm.select(itemsFromList, r =True)

    def RefreshUI(self):
        attributes = ["HDRI", "Default", "Misc", "All"]
        #this collects the scene meshes and organizes them into their corresponding boxes (hdri, default, etc) 
        HDRI = []
        Default = []
        Misc = []
        All = []
        Unassigned = []

        #grabs all geometry and checks the attached "Affected_By" attr. If attr does not exist, the  script adds it with a default value.
        grabAllGeometry = mc.ls(geometry = True, dag = True)

        for mesh in grabAllGeometry:
            for attr in attributes:
                for attr in attributes:
                    group = self.OrganizeObjectsInScene(mesh, attr)
                    value = mc.getAttr( str(mesh + "." + attr))
                    if value == True:
                        if attr == "HDRI":
                            HDRI.append(mesh)
                        if attr == "Default":
                            Default.append(mesh)
                        if attr == "Misc":
                            Misc.append(mesh)
                        if attr == "All":
                            All.append(mesh)
                if value == False:
                    cHDRI = mc.getAttr( str(mesh + ".HDRI"))
                    cDefault = mc.getAttr( str(mesh + ".Default"))
                    cMisc = mc.getAttr( str(mesh + ".Misc"))
                    cAll = mc.getAttr( str(mesh + ".All"))
                    if (cHDRI == False and
                        cDefault == False and
                        cMisc == False and
                        cAll == False):
                        Unassigned.append(mesh)

        #Remove Duplicates
        hdri_ = self.RemoveDuplicatesFromLists(main_widget.scroll_HDRILink, HDRI)
        misc_ = self.RemoveDuplicatesFromLists(main_widget.scroll_MiscLink, Misc)
        default_ = self.RemoveDuplicatesFromLists(main_widget.scroll_DefaultLink, Default)
        all_ = self.RemoveDuplicatesFromLists(main_widget.scroll_AllObjLink, All)
        unassigned_ = self.RemoveDuplicatesFromLists(main_widget.scroll_Unassigned_Objects, Unassigned)

        #scroll lists
        main_widget.scroll_HDRILink.addItems(hdri_)
        main_widget.scroll_MiscLink.addItems(misc_)
        main_widget.scroll_DefaultLink.addItems(default_)
        main_widget.scroll_AllObjLink.addItems(all_)
        main_widget.scroll_Unassigned_Objects.addItems(unassigned_)

    def SelectObjectsInList(self, list):
        items = self.GetLists(list)
        mc.select(items)

    def ReimportUI(self):
        mc.warning(WindRef)
        WindRef.close()
        import ObjectLightLink_UI as OLLui
        reload(OLLui)

        global OLL_window
        try:
            OLL_window.close()
        except:
            pass
        app = QtWidgets.QApplication.instance()
        OLL_window = OLLui.MainWindow(parent=maya_main_window())
        OLL_window.show()

    #Creates a color picking window, applies color to button, returns the rgb value of the chosen color
    def SelectColor(self, button):
        values = None
        mc.colorEditor()
        if mc.colorEditor(query=True, result=True):
            values = mc.colorEditor(query=True, rgb=True)
            print 'RGB = ' + str(values)
        else:
            print 'Editor was dismissed'
        if values != None:
            button.setStyleSheet("background-color: rgb(%s,%s,%s)" % (values[0]*255,values[1]*255,values[2]*255))
            return values

    def SetStateChange(self, button):
        _hdri = main_widget.check_HDRI
        _default = main_widget.check_default
        _miscgroup = main_widget.check_shared_misc_group
        _shared = main_widget.check_shared

        if button == _hdri:
            listOfMeshes = self.GetLists(main_widget.scroll_HDRILink)
        elif button == _default:
            listOfMeshes = self.GetLists(main_widget.scroll_DefaultLink)
        elif button == _miscgroup:
            listOfMeshes = self.GetLists(main_widget.scroll_MiscLink)
        elif button == _shared:
            listOfMeshes = self.GetLists(main_widget.scroll_AllObjLink)

        try:
            DMO
        except NameError:
            import DisplayMetalObjects as DMO
            reload (DMO)
        if DMO:
            DMO.highlightingIsActive = True
            build = DMO.DisplayMetalObjects()
            #send over this information:  objects, color, visibility, vert size
            #mc.warning("color for vert colors   %s" % (str(color)))
            if button.isChecked():
                build.SetHighlighting(listOfMeshes, True)
                mc.warning("set checked is TRUE")
            else:
                build.SetHighlighting(listOfMeshes, False)

    #Turns the color linking on/off. Overides are the scroll list (to cross ref with) and the correpsonding button.color to query the current color
    def ActivateVertColors(self, button, scroll, active):
        default_ = main_widget.scroll_DefaultLink
        hdri_ = main_widget.scroll_HDRILink
        misc_ = main_widget.scroll_MiscLink
        unassigned_ = main_widget.scroll_Unassigned_Objects
        allobj_ = main_widget.scroll_AllObjLink

        hdri_color = main_widget.color_HDRI.palette().color(QtGui.QPalette.Background)
        misc_color = main_widget.color_Misc.palette().color(QtGui.QPalette.Background)
        default_color = main_widget.color_Default.palette().color(QtGui.QPalette.Background)
        all_color = main_widget.color_All.palette().color(QtGui.QPalette.Background)

        skip = None
        color1 = None
        color2 = None
        mixedColor = []
        associatedLists = []
        color = []

        if button == main_widget.button_highlight_HDRI:
            color1 = hdri_color
            skip = "HDRI"
            active = main_widget.check_HDRI
        if button == main_widget.button_highlight_Default:
            color1 = default_color
            skip = "Default"
            active = main_widget.check_default
        if button == main_widget.button_highlight_misc_group:
            color1 = misc_color
            skip = "Misc"
            active = main_widget.check_shared_misc_group
        if button == main_widget.button_highlight_Shared:
            color1 = all_color
            skip = "All"
            active = main_widget.check_shared

        #use button to get lists of meshes
        listOfMeshes = self.GetLists(scroll)
        #check to see if this list shares any mmshes with other lists
        associatedLists = self.CrossCheckLists(listOfMeshes)

        #check the length of associated lists. If greater than 1, find out which additional color needs blending. Store colors lists in list
        if len(associatedLists) == 1:
            print ''
            color = self.ColorBlender(color1, color1, color1.alpha(), color1.alpha(),0)
        elif len(associatedLists) > 1:
            for category in associatedLists:
                #mc.warning("Category:   %s" % (category))
                #mc.warning("Skip:   %s" % (skip))
                if category != skip:
                    if category == "HDRI":
                        color2 = hdri_color
                    if category == "Default":
                        color2 = default_color
                    if category == "Misc":
                        color2 = misc_color
                    if category == "All":
                        color2 = all_color

            color = self.ColorBlender(color1, color2, color1.alpha(), color2.alpha(),0)
            mc.warning ("mixed color:    %s" % (color))
            #mc.warning ("background-color: rgb(%s,%s,%s)" % (color[0], color[1], color[2]))
            #color1.setStyleSheet("background-color: rgb(%s,%s,%s)" % (mixedColor[0], mixedColor[1], mixedColor[2]))
            #mc.warning(associatedLists)
            #mc.warning(listOfMeshes)

        elif associatedLists:
            if associatedLists[0] == "Unassigned":
                print 'apply vert settings for unassigned object'
            #Find out which color should be blended

        #load script that actually performs the vertex highlighting, provide necessary overrides
        try:
            DMO
        except NameError:
            import DisplayMetalObjects as DMO
            reload (DMO)
        if DMO:
            DMO.highlightingIsActive = True
            build = DMO.DisplayMetalObjects()
            #send over this information:  objects, color, visibility, vert size
            #mc.warning("color for vert colors   %s" % (str(color)))
            active.setChecked(True)
            build.AssignVertColors(listOfMeshes, color, True, 3)

    def ColorBlender(self, color1, color2, a1, a2, t):
        c1 = [color1.red(), color1.green(), color1.blue()]
        c2 = [color2.red(), color2.green(), color2.blue()]
        mc.warning("color1  =  %s" %(c1))
        mc.warning("color2  =  %s" %(c2))
        ret = []
        #for each color value in c1 and c2, blend the values
        iterator = [0,1,2]
        for i in iterator:
            ret.append(self.BlendColorValue(c1[i], c2[i], t))
        ret.append(self.BlendAlphaValue(a1, a2, t))
        return ret

    def BlendAlphaValue(self, a, b, t):
        value = a+b
        return max(min(1, value), 0)
    
    def BlendColorValue(self, a, b, t):
        mc.warning("b =   %s" % (a/255))
        mc.warning("a =   %s" % (b/255))
        
        value = ((a/255) + (b/255))/2
        mc.warning("value =   %s" % (value))
        return value

    def OrganizeObjectsInScene(self, mesh, attr):
        returnObj = None
        attrExist = pm.attributeQuery(attr , node = mesh, exists = True)
        if attrExist == False:
            try:
                pm.addAttr(mesh, ln = attr, at = 'bool', dv = False, r = True)
            except:
                mc.warning("Error when attempting to add attribute to object:  " + mesh)
        returnObj = mc.getAttr( str(mesh + "." + attr))
        return returnObj

    def SortIntoList(self, item):
        list = []
        for x in range(item.count()):
            list.append(QtWidgets.QListWidgetItem.text(item.item(x)))
        return list

    def AddObjToList(self, item, list):
        compiledList = None
        if list == "All":
            widgetList = self.GetLists(main_widget.scroll_AllObjLink)
            compiledList = self.VerifyForDuplicates(item, widgetList)
            if compiledList:
                main_widget.scroll_AllObjLink.addItems(self.RemoveDuplicatesFromLists(main_widget.scroll_AllObjLink, compiledList))
        elif list == "Default":
            widgetList = self.GetLists(main_widget.scroll_DefaultLink)
            compiledList = self.VerifyForDuplicates(item, widgetList)
            if compiledList:
                main_widget.scroll_DefaultLink.addItems(self.RemoveDuplicatesFromLists(main_widget.scroll_DefaultLink, compiledList))
        elif list == "HDRI":
            widgetList = self.GetLists(main_widget.scroll_HDRILink)
            compiledList = self.VerifyForDuplicates(item, widgetList)
            if compiledList:
                main_widget.scroll_HDRILink.addItems(self.RemoveDuplicatesFromLists(main_widget.scroll_HDRILink, compiledList))
        elif list == "Misc":
            widgetList = self.GetLists(main_widget.scroll_MiscLink)
            compiledList = self.VerifyForDuplicates(item, widgetList)
            if compiledList:
                main_widget.scroll_MiscLink.addItems(self.RemoveDuplicatesFromLists(main_widget.scroll_MiscLink, compiledList))
        elif list == "Unaffected":
            widgetList = self.GetLists(main_widget.scroll_Unassigned_Objects)
            compiledList = self.VerifyForDuplicates(item, widgetList)
            if compiledList:
                main_widget.scroll_Unassigned_Objects.addItems(self.RemoveDuplicatesFromLists(main_widget.scroll_Unassigned_Objects, compiledList))

    def RemoveDuplicatesFromLists(self, Link, compiledList):
        if compiledList:
            if compiledList[0] != '':
                Link.addItems(compiledList)
        OldList = self.GetLists(Link)
        Link.clear()
        return list(set(OldList))

    def VerifyForDuplicates(self, items, widgetList):
        shapeList = []
        updatedList = []
        updatedList[:] = []
        for item in items:
            defObj = mc.listConnections(item, t = 'mesh')
            #sort through list to make sure only meshes are selected
            #getShape
            itemShape = mc.listRelatives(item, shapes = True)
            if itemShape != None:
                for i in itemShape:
                    visPrimAttrTrans = i + '.primaryVisibility'
                    if mc.objExists(visPrimAttrTrans):
                        shapeList.append(i)
        if len(widgetList) == 0:
            return shapeList
        else:
            if len(shapeList) == 1:
                for i in widgetList:
                    if i != shapeList[0]:
                        updatedList = shapeList
            else:
                updatedList = set(shapeList) - set(widgetList)

        if len(updatedList) == 0:
            return ['']
        else:
            return list(updatedList)

    def GetLists(self, item):
        list = []
        for x in range(item.count()):
            list.append(QtWidgets.QListWidgetItem.text(item.item(x)))
        return list

    def ApplyChangesToScene(self):
        GUI_List = [main_widget.scroll_DefaultLink, 
                    main_widget.scroll_HDRILink, 
                    main_widget.scroll_MiscLink, 
                    main_widget.scroll_Unassigned_Objects,
                    main_widget.scroll_AllObjLink]

        masterObjList = []
        all = self.GetLists(main_widget.scroll_AllObjLink)
        unassigned = self.GetLists(main_widget.scroll_Unassigned_Objects)
        skippable = list([])
        listName = ''

        for comparison in GUI_List:
            a = self.GetLists(comparison)
            if comparison == main_widget.scroll_DefaultLink:
                listName = "Default"
            if comparison == main_widget.scroll_HDRILink:
                listName = "HDRI"
            if comparison == main_widget.scroll_MiscLink:
                listName = "Misc"
            if comparison == main_widget.scroll_Unassigned_Objects:
                listName = "Unassigned"            
            if comparison != main_widget.scroll_AllObjLink:
                sharedwithAll = list(set(a)&set(all))
                if len(sharedwithAll) > 0:
                    for shareAll in sharedwithAll:
                        skippable.append(shareAll)
                    self.ResolveDuplicateItem(sharedwithAll, "All", listName)

            if comparison != main_widget.scroll_Unassigned_Objects and comparison != main_widget.scroll_AllObjLink:
                if comparison == main_widget.scroll_AllObjLink:
                    listName = "All"
                sharedwithUnassigned = list(set(a)&set(unassigned))
                if sharedwithUnassigned:
                    for un in sharedwithUnassigned:
                        skippable.append(un)
                    self.ResolveDuplicateItem(sharedwithUnassigned, "Unassigned", listName)

        for glist in GUI_List:
            meshList = self.GetLists(glist)
            for x in meshList:
                masterObjList.append(x)

        for mesh in masterObjList:
            if mesh in skippable or mesh in sharedwithUnassigned:
                print 'mesh found in skippable list. Ignoring.    mesh:  %s' % (mesh)
            else:
                attr = mesh + '.HDRI'
                if mc.objExists(attr):
                    for x in GUI_List:
                        meshList = set(self.GetLists(x))
                        if mesh in meshList:
                            if x == main_widget.scroll_AllObjLink:
                                mc.setAttr( str(mesh + '.All'), True)
                                mc.setAttr( str(mesh + '.Default'), False)
                                mc.setAttr( str(mesh + '.HDRI'), False)
                                mc.setAttr( str(mesh + '.Misc'), False)
                            elif x == main_widget.scroll_Unassigned_Objects:
                                mc.setAttr( str(mesh + '.All'), False)
                                mc.setAttr( str(mesh + '.Default'), False)
                                mc.setAttr( str(mesh + '.HDRI'), False)
                                mc.setAttr( str(mesh + '.Misc'), False)
                            else:
                                if x == main_widget.scroll_DefaultLink:
                                    mc.setAttr( str(mesh + '.Default'), True)
                                if x == main_widget.scroll_HDRILink:
                                    mc.setAttr( str(mesh + '.HDRI'), True)
                                if x == main_widget.scroll_MiscLink:
                                    mc.setAttr( str(mesh + '.Misc'), True)
                        else:
                            if x == main_widget.scroll_AllObjLink:
                                mc.setAttr( str(mesh + '.All'), False)
                            if x == main_widget.scroll_DefaultLink:
                                mc.setAttr( str(mesh + '.Default'), False)
                            if x == main_widget.scroll_HDRILink:
                                mc.setAttr( str(mesh + '.HDRI'), False)
                            if x == main_widget.scroll_MiscLink:
                                mc.setAttr( str(mesh + '.Misc'), False)

        self.ApplyChangesToLightLinking()

        self.ActivateVertColors(main_widget.button_highlight_HDRI, main_widget.scroll_HDRILink, True)
        self.ActivateVertColors(main_widget.button_highlight_Default, main_widget.scroll_DefaultLink, True)
        self.ActivateVertColors(main_widget.button_highlight_misc_group, main_widget.scroll_MiscLink, True)
        self.ActivateVertColors(main_widget.button_highlight_Shared, main_widget.scroll_AllObjLink, True)
        #Refresh UI in all windows
        
        
            


    def OverrideChangesToLights(self, isAll, changeList, sharedName):
        if isAll == True:
            mc.warning("isAll == True")
            if sharedName == "All":
                for mesh in changeList:
                    attr = mesh + '.HDRI'
                    if mc.objExists(attr):
                        mc.setAttr( str(mesh + '.All'), True)
                        mc.setAttr( str(mesh + '.Default'), False)
                        mc.setAttr( str(mesh + '.HDRI'), False)
                        mc.setAttr( str(mesh + '.Misc'), False)
            elif sharedName == "Unassigned":
                for mesh in changeList:
                    attr = mesh + '.HDRI'
                    if mc.objExists(attr):
                        mc.setAttr( str(mesh + '.All'), False)
                        mc.setAttr( str(mesh + '.Default'), False)
                        mc.setAttr( str(mesh + '.HDRI'), False)
                        mc.setAttr( str(mesh + '.Misc'), False)
        elif isAll == False:
            hdri = self.GetLists(main_widget.scroll_HDRILink)
            default = self.GetLists(main_widget.scroll_DefaultLink)
            misc = self.GetLists(main_widget.scroll_MiscLink)
            for mesh in changeList:
                attr = mesh + '.HDRI'
                if mc.objExists(attr):
                    mc.warning("isAll == False")
                    mc.warning(mesh)
                    mc.setAttr( str(mesh + '.All'), False)

                    if mesh in default:
                        mc.setAttr( str(mesh + '.Default'), True)
                    else:
                        mc.setAttr( str(mesh + '.Default'), False)
                    if mesh in hdri:
                        mc.setAttr( str(mesh + '.HDRI'), True)
                    else:
                        mc.setAttr( str(mesh + '.HDRI'), False)
                    if mesh in misc:
                        mc.setAttr( str(mesh + '.Misc'), True)
                    else:
                        mc.setAttr( str(mesh + '.Misc'), False)


    def ResolveDuplicateItem(self, common, sharedName, listName):
        result = mc.confirmDialog(
        title="Duplicate Items Found",
        icon = "warning",
        message="Items are being shared between '%s' and '%s'. Where would you like this assignment?" % (sharedName, listName),
        messageAlign = 'left',
        button=['Keep Light Linking to "%s" category' %(sharedName), 'Assign Meshes to "%s" category' % (listName), "Cancel"],
        defaultButton="Items are being shared between '%s' and '%s'. Where would you like this assignment?" % (sharedName, listName),
        cancelButton='Cancel',
        dismissString='Cancel',
        bgc = [1.0,.4,.4])

        if result == 'Keep Light Linking to "%s" category' % (sharedName):
            print 'Mesh Light Linking' #mel.eval('MLdeleteUnused')
            self.MoveLightLinking(True, common, sharedName)
        elif result == 'Assign Meshes to "%s" category' % (listName):
            print 'Move meshes out of "All" category'
            print listName
            self.MoveLightLinking(False, common, listName)

    def MoveLightLinking(self, val, common, sharedName):
        if val == True:
            self.OverrideChangesToLights(val, common, sharedName)
        elif val == False:
            self.OverrideChangesToLights(val, common, sharedName)

    def ApplyChangesToLightLinking(self):
        #Store light s in individual lists
        hdriLights = self.FindLightLinking("HDRI")
        defaultLights = self.FindLightLinking("Default")
        miscLights = self.FindLightLinking("Misc")
        Unassigned = self.FindLightLinking("Unassigned")
        allLights = []
        
        for i in hdriLights:
            allLights.append(i)
        for x in defaultLights:
            allLights.append(x)  
        for m in miscLights:
            allLights.append(m)  
        
        #store GUI lists into new lists 
        hdriList = self.GetLists(main_widget.scroll_HDRILink)
        defaultList = self.GetLists(main_widget.scroll_DefaultLink)
        miscList = self.GetLists(main_widget.scroll_MiscLink)
        allList = self.GetLists(main_widget.scroll_AllObjLink)
        unassignedList = self.GetLists(main_widget.scroll_Unassigned_Objects)

        mc.warning(hdriLights)
        mc.warning(defaultLights)
        mc.warning(miscLights)
        mc.warning(Unassigned)
        mc.warning(allLights)
        mc.warning("ALL LIST ^^^^^")

        MasterAssetList = [hdriList, defaultList, miscList, allList, unassignedList]

        #iterate through mesh list and create/break light links according to the asset query.
        for list in MasterAssetList:
            #get asset lists
            #break asset lists into individual lists
            #iterate through lists and check to see checkbox is enabled
            #if checkbox is enabled, create a link
            for asset in list:
                cHDRI = mc.getAttr( str(asset + ".HDRI"))
                cDefault = mc.getAttr( str(asset + ".Default"))
                cMisc = mc.getAttr( str(asset + ".Misc"))
                cAll = mc.getAttr( str(asset + ".All"))

                if cHDRI == True:
                    for hdr in hdriLights:
                        mc.lightlink(b = 0, light = hdr, object = asset)
                elif cHDRI == False:
                    for hdr in hdriLights:
                        mc.lightlink(b = 1, light = hdr, object = asset)
                        
                if cDefault == True:
                    for defa in defaultLights:
                        mc.lightlink(b = 0, light = defa, object = asset)
                elif cDefault == False:
                    for defa in defaultLights:
                        mc.lightlink(b = 1, light = defa, object = asset)
                    
                if cMisc == True:
                    for misc in miscLights:
                        mc.lightlink(b = 0, light = misc, object = asset)
                elif cMisc == False:
                    for misc in miscLights:
                        mc.lightlink(b = 1, light = misc, object = asset)
                
                if cAll == True:
                    for all in allLights:
                        mc.lightlink(b = 0, light = all, object = asset)


    def FindLightLinking(self, TypeofLight):
        sceneLights = mc.ls(type = "light")
        returnLights = []

        for x in sceneLights:
            attrExist = pm.attributeQuery("Light_Group_Assigned" , node = x, exists = True)
            if attrExist == False:
                try:
                    pm.addAttr(x, ln = 'Light_Group_Assigned', at = 'enum', en = 'Unassigned:HDRI:Default:Misc', r = True)
                except:
                    mc.warning("Error when attempting to add attribute to object:  " + x)

            val = mc.getAttr( str(x + '.Light_Group_Assigned'), asString=True)
            if val == TypeofLight:
                returnLights.append(x)

        if len(returnLights) == 0: 
            mc.warning("No types found for " + TypeofLight)
        else:
            mc.warning("Found {0} lights categorized under '{1}' type" .format(len(returnLights), TypeofLight))

        return returnLights

    
    #This checks to see if any other item is a part of the other GUI lists
    def CrossCheckLists(self, list):
        GUI_List = [main_widget.scroll_DefaultLink, 
                    main_widget.scroll_HDRILink, 
                    main_widget.scroll_MiscLink, 
                    main_widget.scroll_Unassigned_Objects,
                    main_widget.scroll_AllObjLink]
        allList = []
        comp_miscList = []
        comp_hdriList = []
        comp_allList = []
        comp_defaultList = []
        comp_unassignList = []

        for GUI in GUI_List:
            items = self.GetLists(GUI)
            for x in items:
                for y in list:
                    if y == x:
                        if GUI == main_widget.scroll_DefaultLink:
                            comp_defaultList.append(y)
                        if GUI == main_widget.scroll_HDRILink:
                            comp_hdriList.append(y)
                        if GUI == main_widget.scroll_MiscLink:
                            comp_miscList.append(y)
                        if GUI == main_widget.scroll_AllObjLink:
                            comp_allList.append(y)
                        if GUI == main_widget.scroll_Unassigned_Objects:
                            comp_unassignList.append(y)
        #Check to see what lists were appended to
        if len(comp_miscList) > 0:
            allList.append("Misc")
        if len(comp_hdriList) > 0:
            allList.append("HDRI")
        if len(comp_allList) > 0:
            allList.append("All")
        if len(comp_defaultList) > 0:
            allList.append("Default")
        if len(comp_unassignList) > 0:
            allList.append("Unassigned")

        mc.warning("All List:   %s"% (allList))
        return allList

######################################################################################################################################

class VertexHighlighting():
    def __init__():
       print ''

    def ApplyHighlights():
        print ''


class SceneMaterials():
    print ''


def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)

def main():
    import ObjectLightLink_UI as OLLui
    reload(OLLui)

    global OLL_window
    try:
        OLL_window.close()
        OLL_window.deleteLater()
        WindRef.close()
    except:
        pass
    app = QtWidgets.QApplication.instance()
    OLL_window = OLLui.MainWindow(parent=maya_main_window())
    OLL_window.show()
    #sys.exit(app.exec_())


if __name__ == "__main__":
    main()