import maya.cmds as mc
import pymel.core as pm
import os
import os.path
import json


def EnableRenderLog(logName):
    #enable logging
    mc.setAttr("defaultArnoldRenderOptions.stats_enable", 1) #enables ability to write render logs
    mc.setAttr("defaultArnoldRenderOptions.stats_mode", 1) #set the mode to append. 0 == overwrite, 1 == append
    mc.setAttr("defaultArnoldRenderOptions.stats_file", logName, type = "string")
    
def FindJsonFile():
    print ''
    #get workspace path
    
    #find 

def ParseJson(directory):
    
    #check to make sure path is valid 
    with open(directory) as json_file:
        jFile = json.load(json_file)
        
        totalSeconds = []
        absoluteTotal = 0
        
        keylist = jFile.keys()
        keylist.sort()
        #mc.warning (keylist)
        
        for x in keylist:
            totalTime = jFile[x]['frame time microseconds']['total']
        
            totalSeconds.append( GetTotalSeconds( float(totalTime) ))

            #hours, minutes, seconds = ConvertTotalToMinutes( totalSeconds )
            
            #print "test frame:  %s" % (x)
            #print "total:  %s" % (totalSeconds)
            #print "hours:  %s" % (int(hours))
            #print "minutes:  %s" % (int(minutes))
            #print "seconds:  %s" % (int(seconds))
        for i in totalSeconds:
            absoluteTotal += i
            
    return int(absoluteTotal)

def GetTotalSeconds(total):
    #print "total time in seconds:     %s" % (total * .000001)
    seconds = (total * .000001)
    return seconds

def ConvertTotalToMinutes(total):
    hours = 0
    minutes = 0
    seconds = 0

    hours += (total // 60 ) // 60
    
    minutes += (total // 60) % 60
    #if minutes > 60:
        #hours += minutes // 60
       # minutes /= (minutes % 60)
    
    seconds += (total % 60)
    
    return hours, minutes, seconds


#EnableRenderLog("testermcTestFace")
#ParseJson(path)

