import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import glob
import os
import Read_Json as readJson

class SetHeightMap(object):

    def __init__(self, name):
        try:
            mc.deleteUI(name, window = True)
        except:
            pass

        if mc.window(name, exists = True):
            mc.deleteUI(name)

        self.icon = icon = mc.internalVar(upd = True) + "/icons/folder.png"

    def BuildWindow(self, name):
        
        winHeight = 200
        prepWin = mc.window(name, w = 280, h = winHeight, mnb = True, mxb = True, s = 1)
        self.baseColumnLayout = baseColumnLayout = "baseColumn"

        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            pm.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 280, h= winHeight)

        #Create header elements
        pm.text(label="")
        pm.rowColumnLayout("buttonLayout", numberOfColumns=3, columnWidth = [(1,100), (2,10), (3,150)])
        
        pm.text(label="Height:", al = 'right')
        pm.text(label="")
        height = pm.floatField('height', v = self.GetCurrentAttributes("aiDispHeight"))
        
        pm.text(label="Bonus_Padding:", al = 'right')
        pm.text(label="")
        padding = pm.floatField("padding", v = self.GetCurrentAttributes("aiDispPadding"))
        
        pm.text( label="Scalar_Zero_Value:", al = 'right')
        pm.text(label="")
        scalar = pm.floatField("scalar", v = self.GetCurrentAttributes("aiDispZeroValue"))        
                
        pm.text(label="")
        pm.text(label="")
        autoBump = pm.checkBox("autoBump", v = self.GetCurrentAttributes("aiDispAutobump"), label = "Auto Bump")       
        
        mc.text(label = 'Iterations:', align = 'right')
        pm.text(label="")  
        interations = pm.intSliderGrp("interations",field = True, v = self.GetCurrentAttributes("aiSubdivIterations"), fmn = 0, fmx = 10, min = 1, max = 10)    
        
        
        pm.text(label="")
        pm.text(label="")
        subDivType = pm.optionMenu("subDivType", label = "Subdiv Type")
        pm.menuItem(label = "none")
        pm.menuItem(label = "catclark")
        pm.menuItem(label = "linear")
        
        pm.optionMenu("subDivType", e = True, sl = self.GetCurrentAttributes("aiSubdivType"))
        pm.setParent("..")
          
        pm.rowColumnLayout("bottomLayout", numberOfColumns=3, columnWidth = [(1,10), (2,250), (3,10)])
        pm.text(label="")
        pm.text(label="")
        pm.text(label="")

        pm.text(label="")
        pm.button(al = "center", w= 230,label = "Apply Height To Selected Objects", command = lambda *_:self.ApplySettingsToSelected())
        pm.text(label="")

        mc.showWindow()
        
    def ApplySettingsToSelected(self):
        height = pm.floatField("height", q = True, value = True)
        padding = pm.floatField("padding", q = True, value = True)
        scalar = pm.floatField("scalar", q = True, value = True)
        autoBump = pm.checkBox("autoBump", q = True, value = True)
        interations = pm.intSliderGrp("interations", q = True, value = True)
        subDivType = pm.optionMenu("subDivType", q = True, value = True)
        
        selected = pm.ls(sl= True)
        

        for x in selected:
            shape = x.getShape()
            
            if shape.type() == "mesh":
                mc.setAttr(x+ ".aiDispHeight", height)
                mc.setAttr(x+ ".aiDispPadding", padding)
                mc.setAttr(x+ ".aiDispZeroValue", scalar)
                mc.setAttr(x+ ".aiDispAutobump", autoBump)
                mc.setAttr(x+ ".aiSubdivIterations", interations)
                
                if subDivType == 'none':
                    mc.setAttr(x + ".aiSubdivType", 0)            
                elif subDivType == 'catclark':
                    mc.setAttr(x + ".aiSubdivType", 1)   
                elif subDivType == 'linear':
                    mc.setAttr(x + ".aiSubdivType", 2)
            else:
                print "%s is not a mesh, skipping." % (x)

        
    def GetCurrentAttributes(self, attr):
        selected = pm.ls(sl= True)

        for x in selected:
            shape = x.getShape()
            
            if shape.type() == "mesh":
                val = mc.getAttr("%s.%s" % (selected[0], attr))

                if val != None:
                    if attr == "aiSubdivType":
                        return val + 1 #subDiv array starts at 1 instead of 0  
                        
                    return val
                    
            if attr == "aiSubdivType":
                return 1.0
            
        return 0.0