class EditQuickSelectSets(object):

    def FindQuickSelect(self):
        qSet = []
        setObj = mc.ls(exactType = "objectSet")
        for set in setObj:
            if mc.sets(set, q = True, text = True) == "gCharacterSet":
                qSet.append(set)
        return qSet
    
    def RenameQuickSel(self, qss):
        if qss.getSelectItem() != None:
            oldName = "".join(qss.getSelectItem())
            result = mc.promptDialog(
            title='',
            message='Rename Set',
            text = oldName,
            button=["Rename", 'Cancel'],
            defaultButton="Create",
            cancelButton='Cancel',
            dismissString='Cancel')

            if result == "Rename":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.rename(oldName, newName)
                self.UpdateQuickSelLayers(qss)
        else:
            mc.warning("Nothing is selected, unable to rename quick select set.")  
               
    def DeleteQuickSel(self, qss):
        if qss.getSelectItem() != None:
            setToDel = "".join(qss.getSelectItem())
            result = mc.confirmDialog(
            title='',
            message='Are you sure you want to delete "' + setToDel + '"',
            button=["Yes. Get it outta here.", 'Nevermind'],
            defaultButton="Nevermind",
            cancelButton='Nevermind',
            dismissString='Nevermind')

            if result == "Yes. Get it outta here.":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.delete(setToDel)
                self.UpdateQuickSelLayers(qss)
        else:
            mc.warning("Nothing is selected, unable to delete quick select set.")
            
    def SelectQuickSet(self, qss):
        set = "".join(qss.getSelectItem())
        mc.select(set, r = True)
        
    def CreateQuickSelect(self, qsName):
        mc.sets(name = qsName, text = "gCharacterSet" )
        
    def CreateQuickSelWindow(self, sl):
        result = mc.promptDialog(
		title='',
		message='Add Set',
		text = 'newSet_01',
		button=["Create Set using Selected", 'Cancel'],
		defaultButton="Create",
		cancelButton='Cancel',
		dismissString='Cancel')

        if result == "Create Set using Selected":
            newLayer = mc.promptDialog(query=True, text=True)
            self.CreateQuickSelect(newLayer)
            self.UpdateQuickSelLayers(sl)
            
    def AddToQuickSelectSet(self, qss):
        if qss.getSelectItem() != None:
            quickSet = "".join(qss.getSelectItem())
            selected = mc.ls(sl= True)
            
            print selected
            
            if selected == [] or selected == None:
                mc.warning("No meshes were selected, nothing was added to " + quickSet)
            else:
                for i in selected:
                    mc.sets(selected, add = quickSet)
        else:
            mc.warning("Nothing is selected, unable to add to quick select set.")

    def UpdateQuickSelLayers(self, sl):
        list = self.FindQuickSelect()
        sl.removeAll()
        pm.iconTextScrollList(sl, edit = True, allowMultiSelection=True)
        for x in list:
            sl.append(x)
        