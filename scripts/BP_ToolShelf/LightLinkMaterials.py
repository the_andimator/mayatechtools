class LightLinkMaterials(object):
    
    def __init__(self):
        CONST_METAL_GOLD = 'MAT_Inlay_Gold_01SG1'
        CONST_METAL_SILVER = 'MAT_Inlay_Silver_01SG'
        CONST_METAL_PORCELAIN = 'MAT_Inlay_Porcelain_01SG'
        CONST_HDRI_MAP = "HDRI_Refl_LightShape"

        self.CONST_METAL_GOLD = CONST_METAL_GOLD
        self.CONST_METAL_SILVER = CONST_METAL_SILVER
        self.CONST_METAL_PORCELAIN = CONST_METAL_PORCELAIN
        self.CONST_HDRI_MAP = CONST_HDRI_MAP
        

    def FindMaterialList(self):
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)   
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')
        optimizedGrp = []
        refresh = shadingGrps
        
        if shadingGrps != None:
            for x in shadingGrps:
                if optimizedGrp != []:
                    if x not in optimizedGrp:
                        print ("%s appended to optimized list" % (x))
                        optimizedGrp.append(x)  
                else:
                    optimizedGrp.append(x)
      
        return optimizedGrp
            
    def GenerateMaterialList(self, llmm):
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)    
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')
        #return shadingGrps
        llmm.removeAll()
        pm.iconTextScrollList(llmm, edit = True, allowMultiSelection=True)
        
        optimizedGrp = []
        refresh = shadingGrps

        if shadingGrps != None:
            for x in shadingGrps:
                if optimizedGrp != []:
                    if x not in optimizedGrp:
                        optimizedGrp.append(x)
                        llmm.append(x)  
                else:
                    optimizedGrp.append(x)
                    llmm.append(x)
        else:
            mc.warning("No shading groups in scene that are attached to objects. Ignoring shading group refresh.")
                
    def CreateHDRILightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        #HDRI = self.FindLights("HDRI")
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        
        selectedMat = materials.getSelectItem()
        hdrObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        if hdrObj != None:
            for sel in hdrObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 0, light = hdr, object = sel)
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 1, light = lights, object = sel)

        
        
    def CreateDefaultLightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        
        selectedMat = materials.getSelectItem()
        defObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        
        
        if defObj != None:
            for sel in defObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 1, light = hdr, object = sel)
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 0, light = lights, object = sel)
        else:
            mc.warning("No lights were found in scene. Unable to Light Link")

    def ConnectAllLightLinks(self, materials, HDRI_Input):
        #light link the objects the the HDRI image
        #HDRI = self.FindLights("HDRI")
        HDRI = mc.textField(HDRI_Input, q = True, text = True)
        Default = self.FindLights("Default")
        print "Searching for HDRI Map:  %s" % HDRI
        HDRIList = []
        HDRIList.append(HDRI)
        selectedMat = materials.getSelectItem()
        defObj = mc.listConnections(selectedMat, t = 'mesh',scn = True)
        if defObj != None:
            for sel in defObj:
                #Establishes connections for HDRI light(s)
                for hdr in HDRIList:
                    mc.lightlink( b = 0, light = hdr, object = sel)
                
                #break connections for default lights
                for lights in Default:
                    mc.lightlink( b = 0, light = lights, object = sel)
        else:
            mc.warning("No lights were found in scene. Unable to Light Link")        
                
    def FindLights(self, sendBack):
        print ''
        #create array of all lights from scene
        grabAllLights = mc.ls(type = 'light', dag = True)
        grabAllGeometry = mc.ls(type = 'geometry', dag = True)
        
        #Returnable Variables
        sceneLights = []
        HDRI = []
        AllLights = []
        
        metallicShaders = []
        nonMetallicShaders = []
        nonMetalObjects = []
   
        #sort lights into separate arrays (HDRI vs everything else)
        for i in grabAllLights:
            if i != self.CONST_HDRI_MAP:
                sceneLights.append(i)
                AllLights.append(i)

        for x in grabAllGeometry:
            if x == self.CONST_HDRI_MAP:
                HDRI.append(x)
                AllLights.append(x)

        grabAllGeometry = mc.ls(type = 'geometry', dag = True)    
        shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')

        #Make list of metallic shaders in specific material
        if shadingGrps != None:
            for mat in shadingGrps:
                if mat == self.CONST_METAL_GOLD or mat == self.CONST_METAL_SILVER or mat == self.CONST_METAL_PORCELAIN:
                    metallicShaders.append(mat)
                else:
                    nonMetallicShaders.append(mat)
        else:
            mc.warning("No HDRI lights were found in scene. Unable to Link HDRI")
        #Make list of objects that have specific material connections

        metalObjects = mc.listConnections(metallicShaders, t = 'mesh', scn = True)
        
        nonMetalObjects = []
        nonMetalObjects = mc.listConnections(nonMetallicShaders, t = 'mesh', scn = True)  

        if sendBack == "HDRI":
            print ''
            return HDRI
        if sendBack == "Default":
            print ''
            return sceneLights
        if sendBack == "All":
            print ''
            return AllLights
            
    def SelectItemsWithMaterial(self, materials):      
        #create array of all lights from scene
        HDRI = []
        metallicShaders = []
        nonMetallicShaders = []
        nonMetalObjects = []
        
        mat =  materials.getSelectItem()
        
        grabAllGeometry = mc.listConnections(mat, type='mesh', scn = True)
        print grabAllGeometry
        mc.select(grabAllGeometry)
    
    def RenameShaderGroups(self, rsg):
        if rsg.getSelectItem() != None:
            oldName = "".join(rsg.getSelectItem())
            result = mc.promptDialog(
            title='',
            message='Rename Shader Group',
            text = oldName,
            button=["Rename", "Generate from Material",'Cancel'],
            defaultButton="Create",
            cancelButton='Cancel',
            dismissString='Cancel')

            if result == "Rename":
                newName = mc.promptDialog(query=True, text=True)
                print newName
                mc.rename(oldName, newName)
                self.GenerateMaterialList(rsg)
                
            if result == "Generate from Material":
                newName = self.GenerateNameForMat(oldName)
                mc.rename(oldName, newName)
                self.GenerateMaterialList(rsg)
        else:
            mc.warning("Nothing is selected, unable to rename quick select set.")  
            
    def GenerateNameForMat(self, oldName):
        MAT = mc.listConnections(oldName)
        for x in MAT:
            searchFor = x + '.outColor'
            if mc.objExists(searchFor):
                newName = x + "SG"

        print newName
        return newName