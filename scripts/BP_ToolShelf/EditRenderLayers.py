def AddToRenderLayers(self, selection):
    col1 = collection.Collection()
    #Get render layer selection from GUI selection
    renderLayerSel = selection.getSelectItem()
    if renderLayerSel != None:
        rlSl = ''.join(renderLayerSel)
        import re
        rlSl = re.sub('rs_', '', rlSl)

        rs = renderSetup.instance()
        rl = rs.getRenderLayer(rlSl)
        clName = rl.getBack()

        if clName == None:
            c1 = rl.createCollection("RL_Built_from_script")
        
        #print firstCollection
        clName = rl.getBack()
        print clName
        #print rl.GetCollections()
        selected = mc.ls(sl=True)
        if selected != []:
            for i in selected:
                print i
                clName.getSelector().staticSelection.add(selected)
        else:
            mc.warning("No objects selected. Please selected something.")
    else:
        mc.warning("No layers are selected. Please select a render layer")

def RemoveFromRenderLayers(self, selection):
    col1 = collection.Collection()
    #Get render layer selection from GUI selection
    renderLayerSel = selection.getSelectItem()
    if renderLayerSel != None:
        rlSl = ''.join(renderLayerSel)
        import re
        rlSl = re.sub('rs_', '', rlSl)
        
        print rlSl #store render layer into variable
        rs = renderSetup.instance()
        rl = rs.getRenderLayer(rlSl)
        clName = rl.getBack()

        if clName == None:
            print ''
            #c1 = rl.createCollection("RL_Built_from_script")
        else:
            selected = mc.ls(sl=True)
            if selected != []:
                for i in selected:
                    print i
                    clName.getSelector().staticSelection.remove(selected) 
            else:
                mc.warning("No objects selected. Please selected something.")
    else:
        mc.warning("No layers are selected. Please select a render layer")   
                  
def IsolateRL(self, selection):
    renderLayerSel = selection.getSelectItem()
    rlSl = ''.join(renderLayerSel)
    import re
    rlSl = re.sub('rs_', '', rlSl)
    
    rs = renderSetup.instance()
    rl = rs.getRenderLayer(rlSl)
    rs.switchToLayer(rl)
            
def FindRenderlayers(self):
    render_layer_list = mc.ls(type="renderLayer")
    defaultRenderLayer = "defaultRenderLayer"
    refDefaultRenderLayer = ":defaultRenderLayer"
    renderList = []
    print render_layer_list
    for i in render_layer_list:
        if i == defaultRenderLayer:
            continue
        elif defaultRenderLayer in i: 
            continue
        elif refDefaultRenderLayer in i: 
            continue
        else:
            renderList.append(i)
    
    return renderList

def FindCollections(self, selection):
    print ''
    #renderLayerSel = pm.textScrollList(selection, q = True, edit = True)
    #renderLayerSel = ""
    #if renderLayerSel != None:
        #rlSl = ''.join(renderLayerSel)
        #import re
       # rlSl = re.sub('rs_', '', rlSl)

       # rs = renderSetup.instance()
       # rl = rs.getRenderLayer(rlSl)
       # clName = rl.getBack()
       # print ''
    #else:
      #  mc.warning("There is nothing selected")

    
def CreateLayerWindow(self, sl):
    result = mc.promptDialog(
    title='',
    message='Add Layer',
    text = 'new render layer',
    button=["Create Layer", 'Cancel'],
    defaultButton="Rename",
    cancelButton='Cancel',
    dismissString='Cancel')

    if result == "Create Layer":
        newLayer = mc.promptDialog(query=True, text=True)
        self.CreateRenderLayers(sl, newLayer)
        self.UpdateRenderLayers(sl)
        
def CreateRenderLayers(self, sl, layerName):
    layerExists = False
    render_layer_list = mc.ls(type="renderLayer")
    folderRL = layerName
    collectionName = layerName + "_collection"
    if "rs_" in layerName:
        print"'rs_' found in layer already. Nice."
    else:
        newLayerName = "rs_" + layerName
        print ("'rs_' not found in render layer, added rs_ to front of string")
        print ("new string =    " + layerName)
        
    for i in render_layer_list: 
        if i == newLayerName:
            layerExists = True
            break
    
    if layerExists == False: 
        rs = renderSetup.instance()
        rl = rs.createRenderLayer(layerName)
        #c = rl.createCollection(collectionName)
    
    
def UpdateRenderLayers(self, sl):
    render_layer_list = mc.ls(type="renderLayer")
    defaultRenderLayer = "defaultRenderLayer"
    renderList = []
    curSel= sl.getSelectItem()
    print curSel
    #sl = pm.textScrollList()
    renderList = self.FindRenderlayers()
    sl.removeAll()
    pm.iconTextScrollList(sl, edit = True, allowMultiSelection=True)
    for x in renderList:
        sl.append(x)
    #sl = sl.replace("||", "|")
    #mc.iconTextScrollList(sl, append = True, si = True)
    
def PrepareWindow(self, sl):
    oldVal = ""
    oldVal = "".join(sl.getSelectItem())
    result = mc.promptDialog(
    title='',
    message='Rename layer',
    text = oldVal,
    button=["Rename", 'Cancel'],
    defaultButton="Rename",
    cancelButton='Cancel',
    dismissString='Cancel')

    if result == "Rename":
        newVal = mc.promptDialog(query=True, text=True)
        self.RenameLayer(sl, newVal, oldVal)
        self.UpdateRenderLayers(sl)
   
def RenameLayer(self, sl, newVal, oldVal):
    rs = renderSetup.instance()
    # Get the list of all render layers
    render_layer_list = mc.ls(type="renderLayer")
    if "rs_" in newVal:
        print"'rs_' found in layer already. Nice."
    else:
        #newLayerName = "rs_" + newVal
        print ("'rs_' not found in render layer, added rs_ to front of string")
    self.UpdateRenderLayers(sl)
    import os.path as path
    projectRoot = mc.internalVar(userWorkspaceDir=True)
    src = path.dirname(path.dirname(projectRoot))
    src = src + "/RSTemplates/"
    sceneName = pm.sceneName()
    sceneName = path.basename(sceneName)
    sceneName = path.splitext(sceneName)[0]
    fullpath = src + sceneName + ".json"
    self.exportRenderSetup(fullpath, "DidThisWork")
    #Change text inside document
    self.FindandReplaceTextinJson(newVal, oldVal, fullpath)
    #reimport updated render layer
    self.importRenderSetup(fullpath)
   
def FindandReplaceTextinJson(self, newVal, oldVal, path):
    import fileinput
    print oldVal
    fixedVal = oldVal.replace("rs_","")
    #fixedVal = oldVal.replace("rs_rs_","")
    fixedVal = '"' + fixedVal + '"'
    newVal = '"' + newVal + '"'
    print newVal
    print fixedVal
    print path
    
    print "THESE ARE THE NEW VALUEs"
    with open(path, 'r+') as f:
        content = f.read()
        f.seek(0)
        f.truncate()
        f.write(content.replace(fixedVal, newVal))

def importRenderSetup(self, filename):
    with open(filename, "r") as file:
        renderSetup.instance().decode(json.load(file), renderSetup.DECODE_AND_OVERWRITE, None)
 
def exportRenderSetup(self, filename , note = None):
    with open(filename, "w+") as file:
        json.dump(renderSetup.instance().encode(note), fp=file, indent=2, sort_keys=True)
        
def FindMaterialList(self):
    grabAllGeometry = mc.ls(type = 'geometry', dag = True)   
    shadingGrps = mc.listConnections(grabAllGeometry, type='shadingEngine')
    optimizedGrp = []
    refresh = shadingGrps
    
    if shadingGrps != None:
        for x in shadingGrps:
            if optimizedGrp != []:
                if x not in optimizedGrp:
                    print ("%s appended to optimized list" % (x))
                    optimizedGrp.append(x)  
            else:
                optimizedGrp.append(x)
  
    return optimizedGrp