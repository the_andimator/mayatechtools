class RemoveUnusedMaterials(object):
        
    def AttemptToRemoveUnusedMat(self):
        result = mc.confirmDialog(
		title='Remove Unused Materials in Scene',
		message='                                          Are you sure you want to remove unused materials?',
		messageAlign = 'center',
		button=["Yes. I'm prepared to potentially break my scene.", 'Nope. Definitely Not. Take me back to safety, Captain'],
		defaultButton="Nope. Definitely Not. Take me back to safety, Captain",
		cancelButton='Nope. Definitely Not. Take me back to safety, Captain',
		dismissString='Nope. Definitely Not. Take me back to safety, Captain',
		bgc = [1.0,.4,.4])

        if result == "Yes. I'm prepared to potentially break my scene.":
            mel.eval('MLdeleteUnused')
        
   