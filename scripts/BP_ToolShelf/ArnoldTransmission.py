class ArnoldTransmission(object):

    def __init__(self):
        visDif = False
        visSpec = False
        visDiffTrans = False
        visSpecTrans = False
        visOpaque = False

        self.visDif = visDif
        self.visSpec = visSpec
        self.visSpecTrans = visSpecTrans
        self.visDiffTrans = visDiffTrans
        self.visOpaque = visOpaque

    def SetArnoldAttributes(self, all):
        #Apply Values to variables
        visDif = mc.checkBox(self.visDif, q= True, v=True, changeCommand = True)
        visSpec = mc.checkBox(self.visSpec, q= True, v= True, changeCommand = True)
        visSpecTrans = mc.checkBox(self.visSpecTrans, q= True, v= True, changeCommand = True)
        visDiffTrans = mc.checkBox(self.visDiffTrans, q= True, v= True, changeCommand = True)
        
        if all == True:
            meshes = mc.ls(geometry = False, shapes = True,  type='geometryShape', showType=True)
            for i in meshes:  
                specAttr = i + '.aiVisibleInSpecularReflection'
                diffAttr = i + '.aiVisibleInDiffuseReflection'
                diffAttrTrans = i + '.aiVisibleInDiffuseTransmission'
                specAttrTrans = i + '.aiVisibleInSpecularTransmission'
                if mc.objExists(specAttr):
                    mc.setAttr(diffAttr, visDif)
                    mc.setAttr(specAttr, visSpec)
                    mc.setAttr(diffAttrTrans, visDiffTrans)
                    mc.setAttr(specAttrTrans, visSpecTrans)
                               
        elif all == False:
            selected = mc.ls(sl= True)
            getShape = mc.listRelatives(selected, shapes=True)
            
            if getShape != None:
                for i in getShape:
                    specAttr = i + '.aiVisibleInSpecularReflection'
                    diffAttr = i + '.aiVisibleInDiffuseReflection'
                    diffAttrTrans = i + '.aiVisibleInDiffuseTransmission'
                    specAttrTrans = i + '.aiVisibleInSpecularTransmission'
                    if mc.objExists(specAttr):
                        mc.setAttr(diffAttr, visDif)
                        mc.setAttr(specAttr, visSpec)
                        mc.setAttr(diffAttrTrans, visDiffTrans)
                        mc.setAttr(specAttrTrans, visSpecTrans)
            else:
                mc.warning("No meshes are selected. Unable to apply changes.")
            
        