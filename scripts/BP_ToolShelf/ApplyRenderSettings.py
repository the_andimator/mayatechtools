class ApplyRenderSettings(object):
    threadBox = None
    threadSlider = None
    
    def __init__(self):
        threadAttr = None
        ThreadCount = None

        self.threadAttr = threadAttr
        self.ThreadCount = ThreadCount
    
    def checkbox_toggled(self, *args):
        slider_enabled = pm.checkBox("Thread", q = True, value = True)
        print slider_enabled
        if slider_enabled:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = False)
        else:
            pm.intSliderGrp("ThreadSlider", edit=True, enable = True)
        

    def ApplyRenderSettings(self, camAA, diffuse, specular, transmission, SSS, VolumeIndirect, TransparencyDepth, TransparencyThreshold, BucketSize, lockSample):
        slider_enabled = pm.checkBox("Thread", q = True, value = True)

        
        _camAA = pm.intSliderGrp("camAA", q = True, value = True)
        _diffuse= pm.intSliderGrp('diffuse', q = True, value = True)
        _specular = pm.intSliderGrp('Specular', q = True, value = True)
        _transmission = pm.intSliderGrp('Transmission', q = True, value = True)
        _SSS = pm.intSliderGrp('sss', q = True, value = True)
        _VolumeIndirect = pm.intSliderGrp('VolumeIndirect', q = True, value = True)
        
        _TransparencyDepth = pm.intField(TransparencyDepth, q = True, value = True)
        _TransparencyThreshold = pm.floatField(TransparencyThreshold, q = True, value = True)
        _BucketSize = pm.intField(BucketSize, q = True, value = True)
        _maxThreads = pm.intSliderGrp("ThreadSlider", q = True, value = True)
        
        mc.setAttr("defaultArnoldRenderOptions.threads" , _maxThreads)
        mc.setAttr("defaultArnoldRenderOptions.threads_autodetect", slider_enabled)

        mc.setAttr('defaultArnoldDriver.ai_translator', 'tif', type='string')
        mc.setAttr("defaultArnoldDriver.tiffFormat ", 0)
        
        mc.setAttr("defaultArnoldRenderOptions.AASamples", _camAA)
        mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", _diffuse)
        mc.setAttr("defaultArnoldRenderOptions.GISpecularSamples", _specular)
        mc.setAttr("defaultArnoldRenderOptions.GITransmissionSamples", _transmission)
        mc.setAttr("defaultArnoldRenderOptions.GISssSamples", _SSS)
        mc.setAttr("defaultArnoldRenderOptions.GIVolumeSamples", _VolumeIndirect)
        mc.setAttr("defaultArnoldRenderOptions.lock_sampling_noise", lockSample)
        mc.setAttr("defaultArnoldRenderOptions.bucketSize", _BucketSize)
        
        mc.setAttr("defaultArnoldRenderOptions.autoTransparencyDepth", _TransparencyDepth)
        
        mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", _diffuse)
        mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", _diffuse)
        mc.setAttr("defaultArnoldRenderOptions.GISpecularDepth", _specular)
        mc.setAttr("defaultArnoldRenderOptions.GITransmissionDepth", _transmission)
        mc.setAttr("defaultArnoldRenderOptions.GIVolumeDepth", _VolumeIndirect)
        mc.setAttr("defaultResolution.width", 1920)
        mc.setAttr("defaultResolution.height", 1080)
        mc.setAttr("defaultResolution.dotsPerInch", 300)