class SmoothSceneMeshes(object):

    def SmoothMeshes(self, action):
        meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True) 
        for i in meshes:
            smooth = i + ".displaySmoothMesh"
            if mc.objExists(smooth):
                if action == True:
                    mc.setAttr(smooth, 2)
                if action == False:
                    mc.setAttr(smooth, 0)