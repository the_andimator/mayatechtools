import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm


class DetermineRenderTimes(object):
    global renderLayerWin
    renderLayerWin = ''

    def __init__(self):
        try:
            mc.deleteUI(windowName, window = True)
        except:
            pass

        if mc.window("DetermineRenderTimes", exists = True):
            mc.deleteUI("DetermineRenderTimes")

        self.SequenceAttr = SequenceAttr = []
        self.RenderableSequences = RenderableSequences = []


    def CreateRenderWindow(self):
        try:
            mc.deleteUI("DetermineRenderTimes", window = True)
        except:
            pass

        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "DetermineRenderTimes"

        try:
            mc.deleteUI(WinName)
        except:
            pass

        self.renderNodes = mc.container("Scene_Sequence_Timing",q = True, nl = True)

        winHeight = 275

        prepWin = mc.window("DetermineRenderTimes", w =300, h = winHeight, mnb = True, mxb = True, s = 1)
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 1120, h= winHeight)

        for x in self.renderNodes:
#            ###Create Border and Frames####
            instanceFrameLayout = mc.frameLayout(x, parent = prepWin, label = x, cll = False, bv = False)
            instancerowColumnLayout = mc.rowColumnLayout(x, nc = 12, cw = [(1,100), (2,100), (3,100), (4,100), (5,100), (6,100), (7,100), (8,100), (9,100), (10,100),(11, 200), (12,150)], columnOffset = [(1, "both", 2), (2, "both", 2), (3, "both", 0)])

            ###_Render_Layer_###
            mc.text(label = "Frame 1:  ", align = "right")
            mc.intField(x + "_frame_01", ed= True)

            ###_Render_Layer_###
            mc.text(label = "Frame 2:  ", align = "right")
            mc.intField(x + "_frame_02",ed= True)

            ###_Start_###
            mc.text(label = "Frame 3:  ", align = "right")
            mc.intField(x + "_frame_03",ed = True)

            ###_End_###
            mc.text(label = "Frame 4:  ", align = "right")
            mc.intField(x + "_frame_04",ed = True)            
            
            ###_End_###
            mc.text(label = "Frame 5:  ", align = "right")
            mc.intField(x + "_frame_05",ed = True)
            
            mc.text(label ="Include  ", al = 'right')
            render = mc.checkBox(str(x + "_render"),label = "", v = True, ed= True, al = "center")
            
            mc.text(l = "")

#       #Separate Attributes (Sequence #, render layer, start, end) into individual lists
#       #Add Button for rendering at bottom
        bottom = mc.separator("bottom", style='in', width=1400)
        mc.setParent(top = True)
        
        buttoncolumnLayout = mc.rowColumnLayout('buttonLayout', nc = 1, cw = [(1,1350)])
        mc.text(l="")
        mc.text(l="")
        mc.button(al= 'center', l="Start Render Test", c =  lambda *_: self.StartTest(self.renderNodes, False))
        mc.setParent(top = True)
        
        mc.showWindow()


    def StartTest(self, renderNodes, batch):
        SingleSequence = []
        self.SequenceAttr = []
        self.RenderableSequences = []
        
        statPath = []
        ready = True
        #Changes renderAllSequenceCameras to True in render sequencer options. This is very important, disabling this option breaks camera switching when doing renders.
        mel.eval('optionVar -intValue "renderSequenceAllCameras" 1;')

        #Store values in their own lists
        from mtoa.core import createOptions
        createOptions()
  
        for x in renderNodes:
            renderableFrames = []

            
            if mc.checkBox(str(x + "_render"), q = True, v = True) == True:
                
                if mc.intField(str(x + "_frame_01"), q = True, v = True) > 0:
                    renderableFrames.append(mc.intField(str(x + "_frame_01"), q = True, v = True))
                if mc.intField(str(x + "_frame_02"), q = True, v = True) > 0:
                    renderableFrames.append(mc.intField(str(x + "_frame_02"), q = True, v = True))
                if mc.intField(str(x + "_frame_03"), q = True, v = True) > 0:
                    renderableFrames.append(mc.intField(str(x + "_frame_03"), q = True, v = True))
                if mc.intField(str(x + "_frame_04"), q = True, v = True) > 0:
                    renderableFrames.append(mc.intField(str(x + "_frame_04"), q = True, v = True))
                if mc.intField(str(x + "_frame_05"), q = True, v = True) > 0:
                    renderableFrames.append(mc.intField(str(x + "_frame_05"), q = True, v = True))

                name = self.FillSequenceUI(x, ".CameraName")
                renderLayer = self.FillSequenceUI(x, ".RenderLayer")
                prefix = self.FillSequenceUI(x, ".Prefix")
                directory = self.FillSequenceUI(x, ".FileDirectory")
                logDirectory = self.FillSequenceUI(x, ".RenderStatsDir")
                
                    
                if logDirectory == '' or logDirectory == None:
                    ready = False
                else: 
                    if os.path.exists(logDirectory):
                        print("Directory " , logDirectory ,  " already exists")
                    else:
                        os.mkdir(logDirectory)
                        print("Directory " , logDirectory ,  " Created ")
                        
                #Collate information into a single list
                SingleSequence = SequenceAttributes(x, name, renderLayer, prefix, directory, logDirectory, renderableFrames)
                self.RenderableSequences.append(SingleSequence)

            else:
                mc.warning( "Skipping %s\n" % (x) )

        #verify that a logpath exists
        for i in renderNodes:
            statPath.append( self.FillSequenceUI(i, ".RenderStatsDir") )
            print('\nSequence:         %s\nRenderStatsDir:   %s\n' % (i, self.FillSequenceUI(i, ".RenderStatsDir")) )
        
        if ready == True:
            for x in self.RenderableSequences:
                #Disables all other cams from being renderable
                
                print("Starting Test Renders for %s\n            Frames to test:  %s\n" % (x.name, x.renderableFrames))
                print("Name of Sequence %s\n" % (x.seqName))

                self.SetCamAsRenderable(x.name, 1) #Changes camera to renderable state
                self.LookThroughCam(x.name) #Looks through active camera
                
                if len(x.renderableFrames) == 0:
                    print "No frames to render. Stopping sequence."
                    break
                    
                for i in x.renderableFrames:
        #################__Send Sequences to be Rendered__########
                    self.UpdateRenderGlobals(i, i, x.name, False) #Changes render globals to match start/end frames, image prefix, and whether to skip existing frames
                    self.SetRenderLayer(x.renderLayer)
                    
                    #create log names
                    logName = "%s/%s.json" % (x.logDirectory, x.seqName)
                    
                    #change log
                    import Read_Json as json
                    reload(json)                 
                    json.EnableRenderLog(logName)
                    #json.ParseJson(x.logDirectory)
                    
                    ##Disable persp as renderable camera"
                    mc.setAttr("perspShape.renderable", 0)
                    mc.setAttr("perspShape.visibility", 0)
                    mc.setAttr("persp.visibility", 0)
                    mel.eval("getCurrentCamera()")

                    if(x == 0):
                        print ''
                        mc.RenderSequence( s=i, e=i, RenderAllCameras=False) #Starts Sequence Render
                    else:
                        print ''
                        mc.RenderSequence( s=i, e=i)
                    
                    mc.pause (sec = 2)
                    mc.refresh()
        else:
            mc.warning("Render Stats are missing in Sequence node. Double check to make sure that all fields are properly filled out.")


    def SetCamAsRenderable(self, cam, enable):
        if mc.objExists(cam):
            mc.setAttr("%s.renderable" % cam, enable)
        else:
            mc.warning("SetCamAsRenderable() -- Camera could not be found. Please check spelling of camera component")

    # Looks through specified camera for render
    def LookThroughCam(self, cam):
        if mc.objExists(cam):
            mc.lookThru(cam)
            #mc.lookThru(cam, q=True )
            mel.eval("getCurrentCamera()")
        else:
            mc.warning("LookThroughCam() -- Unable to look through camera that doesn't exist. Please check spelling of camera component")

        # Update render settings before starting another sequence
    def UpdateRenderGlobals(self, start, end, prefix, skip):
        mc.setAttr("defaultRenderGlobals.startFrame", start)
        mc.setAttr("defaultRenderGlobals.endFrame", end)
        mc.setAttr("defaultRenderGlobals.imageFilePrefix", prefix, type = "string")

        # Must run before starting RenderSequence
    def GetCurrent():
        mel.eval("getCurrentCamera()")

    def SetRenderLayer(self, activeLayer):

        foundLayer = False
        defaultRenderLayer = "defaultRenderLayer"
        # Get the list of all render layers
        render_layer_list = mc.ls(type="renderLayer")

        if "rs_" in activeLayer:
            print("rs_ found in render layer")
        else:
            activeLayer = "rs_" + activeLayer
            mc.warning("'rs_' not found in render layer, added rs_ to front of string")
            mc.warning ("new string =    " + activeLayer)

        for i in render_layer_list:
            if i == activeLayer:
                mc.warning("Render Layer found:  " + activeLayer)
                mc.editRenderLayerGlobals (currentRenderLayer = activeLayer)
                foundLayer = True
                return
            else:
                foundLayer = False

        if foundLayer == False:
            for x in render_layer_list:
                if x == defaultRenderLayer:
                    mc.editRenderLayerGlobals (currentRenderLayer = defaultRenderLayer)

    def FindRenderable(self, sequence):
        if sequence.render == True:
            return True
        else:
            return False
            
    def FillSequenceUI(self, seq, attr):
        info = mc.getAttr(seq + attr)
        
        if info == '' or info == None:
            return None
        else:   
            return info


class SequenceAttributes (object):

        def __init__(self, seqName, name, renderLayer, prefix, directory, logDirectory, renderableFrames):
            self.seqName = seqName
            self.name = name
            self.renderLayer = renderLayer
            self.prefix = prefix
            self.directory = directory
            self.renderableFrames = renderableFrames
            self.logDirectory = logDirectory
            
        def ReturnValues(self):
            values = [self.seqName, self.name, self.renderLayer, self.prefix, self.directory, self.renderableFrames, self.logDirectory]
            return values
