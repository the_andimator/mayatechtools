import maya.cmds as mc
import pymel.core as pm
import os
import os.path
from os import listdir
import winsound

def FindMissingTextures():
    
    broken_textures = []
    missingPath = []
    texture_filename = []
    
    # Get list of all textures in scene (including references)
    activeTextures = mc.ls( textures=True )

    # For each file node..
    for f in activeTextures:
        # Get the name of the image attached to it
        checkType = mc.nodeType(f)

        if checkType == "file":
            path = str(mc.getAttr(f + '.fileTextureName'))
            
            if CheckExists(path) == False:
                    texture_filename.append(str(f))
                    missingPath.append(path)
                    broken_textures.append("%s         PATH:     %s" % (str(f), path))
                    

        # Test image directory 
        elif checkType == "layeredTexture":
            layered = mc.listConnections(f, s=True, type= "file")
            
            for i in layered:
                path = str(mc.getAttr(i + '.fileTextureName'))
                if CheckExists(path) == False:
                    texture_filename.append(str(i))
                    missingPath.append(path)
                    broken_textures.append("%s         PATH:     %s" % (str(i), path))

    filename = CheckDuplicate(texture_filename)
    print filename
    missing = CheckDuplicate(missingPath)
    print missing
    broken = CheckDuplicate(broken_textures)
    print broken
    
    mc.select(filename)
    
    return missing, filename

def CheckExists(file):
    if os.path.exists(file) == False:
        return False
    else:
        return True
        
def CheckDuplicate(list):
    seen = set()
    seen_add = seen.add
    no_duplicate = [x for x in list if not (x in seen or seen_add(x))]
    return no_duplicate
    
def StartScript():
    try:
        mc.deleteUI("Missing Textures", window = True)
    except:
        pass
        
    paths, names = FindMissingTextures()
    
    outputText = []
    guiText = []
    for num, path in enumerate(paths, start=0):
        #print "num : {}".format(num)
        print "path : {}".format(path)
        print "name : {}".format(names[num])
        outputText.append("NODE: {}\r\nPATH: {}\r\n".format(names[num], path) )
        guiText.append("NODE: {:<30} PATH: {:<80}".format(names[num], path) )
        #mc.warning("\r\n", "Missing Textures found in file:", "\r\nNODE: {}\r\nPATH: {}\r\n\r\n".format(names[num], path))
        
    mc.warning("\r\n", outputText)
        
    if len(names) > 0:
        soundfile = "Z:\ASSETS/3D MVT ASSETS\MVT_Assets_Library\Audio\sadhorn.wav"
        if os.path.exists(soundfile):
            winsound.PlaySound(soundfile, winsound.SND_ASYNC)
        
        if len(names) == 1:
            # Make a new window
            height = 25
        else:
            height = len(names) * 100
        window = pm.window("Missing Textures", title="Missing Textures", widthHeight=(450, height), bgc = (.2,.2,.2), ds = True, nde=True)
        pm.columnLayout( adjustableColumn = True )
        pm.text( label = "", al = "left", bgc = (.26,.26,.26))
        pm.text( label = "MISSING TEXTURES FOUND!", al = "center", fn = "boldLabelFont", bgc = (.26,.26,.26))
        pm.text( label = "", al = "left", bgc = (.26,.26,.26))
        pm.text( label = "", al = "left")
        
        for i in guiText:
            #mc.warning("\r\n", "Missing Textures found in file:", "NODE:    {}      PATH: {}".format(names[num], path))
            pm.text( label = i, al = "left", bgc = (.26,.26,.26))
            
        pm.showWindow( window )
        
        mc.FilePathEditor()
        
        #mc.warning("\r\n", "Missing Textures found in file:", "NAME:      {}  \r\nPATH: {}" % (names, paths))
        winsound.PlaySound("SystemExit", winsound.SND_ASYNC)
    else:
        print "All textures are connected! Nice!"
        mc.confirmDialog( title='Texture Check', message='Congrats! All textures are connected!   ', button = "  Neato burrito!  ")
