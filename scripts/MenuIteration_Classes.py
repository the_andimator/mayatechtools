import maya.cmds as mc
import os.path
import maya.mel as mel
import sys as sys
from functools import partial as ftool
import shutil as shutil
from os import listdir

########################################
#########__SAVE_THIS_TO_SHELF_##########
########################################
#
#import MenuIteration_Classes as MI
#reload (MI)
#build = MI.SequenceSetup(1)
#build.PrepareWindow()
#
########################################
########################################
class SequenceSetup(object):
    
    
    def __init__(self, amount):
        try:
            mc.deleteUI(setupWin, window = True)
        except:
            pass
        self.amount = amount
        self.windowName = windowName = "Creating Sequencer"
       
        if mc.window(self.windowName, exists = True):
            mc.deleteUI(self.windowName)
        self.setupWin = mc.window(self.windowName, title= self.windowName, w =250, h = 85, mnb = True, mxb = True, s = 1, rtf = True)
        mainLayout = mc.columnLayout(w = 250, h= 50)
        
    def PrepareWindow(self):
        
        result = mc.promptDialog(
		title='Creating Sequence...',
		message='# of Sequences:',
		button=["Honey Batcher don't give a Fu..", 'Cancel'],
		defaultButton="Honey Batcher don't give a Fu..",
		cancelButton='Cancel',
		dismissString='Cancel')

        if result == "Honey Batcher don't give a Fu..":
            text = mc.promptDialog(query=True, text=True)
            self.BuildItBetter(int(text))

    def BuildItBetter(self, amount):
        self.amount = amount
        if amount == "":
            mc.warning("No value given. Defaulting to 1 sequence")
        SequencerWindow = self.CreateWindowInstance()     
        SequencerWindow.BuildUI()
        #overides = number of sequences, camera name (iterates # on subsequent sequences), start frame, end frame, skip existing (boolean), directory, prefix 
        #img_<Scene>_<RenderLayer>
        print self.amount

    
    def CreateWindowInstance(self):
        return Window(self.amount, 'camera', 1, 2, 0, '', 'img_<Scene>_<Camera>_<RenderLayer>_', "<None>", "Scene One")       

        
class Window(object):
    
    global mainWin
    
#######################____Initialize Variables____#######################
    
    def __init__(self, numSequences, name, start, end, skip, directory, prefix, rendLayer, sceneAssignment):
        try:
            mc.deleteUI(mainWin, window = True)
        except:
            pass
        
        self.numSequences = numSequences
        self.name = name
        self.start = start
        self.end = end
        self.skip = skip
        self.directory = directory
        self.prefix = prefix
        self.rendLayer = rendLayer
        self.sceneAssignment = sceneAssignment
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        self.sequenceInputs = sequenceInputs = []
        self.sequenceStorage = sequenceStorage = []
        self.numSequences = numSequences
        self.sizeable = sizeable = 1
        self.windowName = windowName = "Render_Sequencer"
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        
        if mc.window("Render_Sequencer", exists = True):
            mc.deleteUI("Render_Sequencer")
        
        
    def BuildUI(self):
        mainWin = mc.window("Render_Sequencer", title= self.windowName, w =738, h = 550, mnb = True, mxb = True, s=self.sizeable, rtf = True)
        
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            cmds.deleteUI(self.baseColumnLayout)

        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 738, h= 350)

        
        for x in range(self.numSequences):
            tempName = self.name
            
            name = tempName + str(x+1)
            self.sequenceStorage.append (self.MakeNew(name, self.start, self.end, self.skip, self.prefix, self.rendLayer, self.sceneAssignment))
            self.sequenceStorage[x].BuildSequenceUI(self.sequenceStorage[x].name, x, self.sequenceStorage[x].start, self.sequenceStorage[x].end, self.sequenceStorage[x].skip, self.sequenceStorage[x].directory ,self.sequenceStorage[x].prefix, self.sequenceStorage[x].rendLayer, mainWin)

        mc.button(label = "Start Hackish Batch Render", command = lambda *_: self.ApplyCallBack(self.numSequences), w = 738)

        mc.showWindow()
        
    def MakeNew(self, name, start, end, skip, prefix, rendLayer, sceneAssignment):
        return SequenceUI(name, self.numSequences, start, end, skip, "", prefix, rendLayer, sceneAssignment)
        
#######################____APPLY GUI VALUES TO LISTS____#######################
    def ApplyCallBack(self, numSequences):
        seq = 0
        seqAttr = 0
                
        for x in range(numSequences):
            name = mc.textField(self.sequenceStorage[x].name, q=True, text = True)
            start = mc.intField(self.sequenceStorage[x].start, q=True, v = True)
            end = mc.intField(self.sequenceStorage[x].end, q=True, v = True)
            skip = mc.checkBox(self.sequenceStorage[x].inputSkip, q=True, v = True)
            directory = mc.textField(self.sequenceStorage[x].inputDirectory, q=True, text = True)
            prefix = mc.textField(self.sequenceStorage[x].inputPrefix, q=True, text = True)
            rendLayer = mc.textField(self.sequenceStorage[x].inputRendLayer, q = True, text = True)
            sceneAssignment = mc.textField(self.sequenceStorage[x].inputSceneAssignment, q = True, text = True)
            print (name)
            print (start)
            print (end)
            print ("Apply Callback is seeing:            "+ directory)
            print (prefix)

            #Disables all other cams from being renderable
            self.sequenceStorage[x].SetCamAsRenderable(name, 1) #Changes camera to renderable state
            mc.warning("enabled %s before rendering" %(mc.textField(self.sequenceStorage[x].name, q=True, text = True)))
                    
            self.sequenceStorage[x].LookThroughCam(name) #Looks through active camera

############__Send Sequences to be Rendered__########
            self.sequenceStorage[x].UpdateRenderGlobals(start, end, prefix, skip) #Changes render globals to match start/end frames, image prefix, and whether to skip existing frames
            self.sequenceStorage[x].SetDirectory(directory) #changes render directory
            self.sequenceStorage[x].SetRenderLayer(rendLayer)
            
            ##Disable persp as renderable camera"
            mc.setAttr("perspShape.renderable", 0)
            mc.setAttr("perspShape.visibility", 0)
            mc.setAttr("persp.visibility", 0)
            mel.eval("getCurrentCamera()")

            if(x == 0):
                mc.RenderSequence( s=start, e=end, RenderAllCameras=False) #Starts Sequence Render
            else:
                mc.RenderSequence( s=start, e=end)
            
            self.sequenceStorage[x].SetCamAsRenderable(name, 0)
            mc.setAttr("%s.visibility" % name, 0)
            
            projectRoot = mc.workspace(q=1,rd=1)
            src = projectRoot + "images/tmp/"
            dst = directory
            print "SCENE :::   " + sceneAssignment
            
            copyTree(src, dst, sceneAssignment, name, False, None)
      
            #ToDo -- Add some error checking and pausing. If something fails, either abort render or attempt to move on to next loop-item
            
class SequenceUI():
    
    def __init__(self, name, numSequences, start, end, skip, directory, prefix, rendLayer, sceneAssignment):
        self.name = name
        self.start = start
        self.end = end
        self.skip = skip
        self.directory = directory
        self.prefix = prefix
        self.rendLayer = rendLayer
        self.sceneAssignment = sceneAssignment
        
        SequenceTitle = None
        UpperRowName = None
        LowerRowName = None
        inputName = None
        seqCount = int(numSequences)
        
        self.sequences = sequences = []
        self.SequenceTitle = SequenceTitle
        SequenceTitle = "Sequence %s"%(seqCount+1)
        self.UpperRowName = UpperRowName
        UpperRowName = "RowColumnTop %s"%(seqCount+1)
        self.LowerRowName =  LowerRowName
        LowerRowName = "RowColumnTop %s"%(seqCount+1)
        x = int(numSequences)
        self.inputName = inputName
        
        appendedName = name
        tempSequence = []
        tempSequence.append(appendedName)
        tempSequence.append(self.start)
        tempSequence.append(self.end)
        tempSequence.append(self.skip)
        tempSequence.append(self.directory)
        tempSequence.append(self.prefix)

        self.sequences.append(tempSequence)

        ##Input Fields
        iName = ""
        iStart = 0
        iEnd = 10
        iSkip = False
        iDir = ""
        iPrefix = ""
        
        self.iName = iName
        self.iStart = iStart
        self.iEnd = iEnd
        self.iSkip = iSkip
        self.iDir = iDir
        self.iPrefix = iPrefix
        
###################____Create UI based on # of sequences____#######################
    def BuildSequenceUI(self, name, numSequences, start, end, skip, direct, prefix, rendLayer, mainWin):
        seqCount = int(numSequences)
        directoryText = None
        self.SequenceTitle = "Sequence %s"%(seqCount+1)
        self.UpperRowName = "RowColumnTop %s"%(seqCount+1)
        self.LowerRowName = "RowColumnTop %s"%(seqCount+1)
        self.directoryText = directoryText
        x = int(seqCount)
        self.inputName = ""
        
        ###Create Border and Frames####
        instanceFrameLayout = mc.frameLayout(self.SequenceTitle, parent = mainWin, label = self.SequenceTitle, cll = True, bv = True)
        instancerowColumnLayout = mc.rowColumnLayout(self.UpperRowName, nc = 9, cw = [(1,93), (2,250), (3,30), (4,50), (5,30), (6,50), (7,80), (8,50), (9,100)], columnOffset = [(1, "both", 2), (2, "both", 2), (3, "both", 0), (4, "both",1), (5, "both", 5), (6, "both", 1), (7, "both", 2), (8, "both", 2), (9, "both", 2)])
        
        ###_Camera_###
        mc.text(label = "Camera Name : ", align = "right") 
        self.name = mc.textField(text = self.name)

        ###_Start_###
        mc.text(label = "Start : ", align = "left") 
        self.start = mc.intField(v = self.start)
  
        ###_End_###
        mc.text(label = "End : ", align = "left") 
        self.end = mc.intField( v = self.end)
        mc.text (label = "") 
        mc.text (label = "")
        
        ###_Skip_###
        self.inputSkip = mc.checkBox(label= 'Skip Existing') 
        mc.setParent("..")

        ###_Image_Directory_###
        self.directoryText = str(mc.workspace(q=True, rd = True)) + "images"
        
        rowColumnLayout = mc.rowColumnLayout(nc = 6, cw = [(1,93), (2,400), (3,10), (4,20), (4,50)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4)])
        mc.text(label = "Image Directory : ", align = "right") 
        self.inputDirectory = mc.textField(pht='C:\Users\Joe_Danger\Documents\maya\projects\default', w = 400 , text = self.directoryText) 
        icon = mc.internalVar(upd = True) + "/icons/folder.png"
        self.inputBrowseButton = mc.symbolButton(w = 20, h = 20, image = icon, ebg = True, c = lambda *_: self.BrowseFilePath(3, None, self.inputDirectory)) 
        mc.text (label = "") 
        
        ###_Prefix_###
        mc.text(label = "Prefix : ", align = "right") 
        self.inputPrefix = mc.textField(pht='format: img_project_scene_layer', w = 135, text = self.prefix)
        mc.setParent("..")
        
        ##_Render_Layer_###
        rowColumnLayout = mc.rowColumnLayout(nc = 6, cw = [(1,93), (2,300), (3,108), (4,20), (4,50)], columnOffset = [(1, "both", 4), (2, "both", 4), (3, "both", 4), (4, "both",4)])
        mc.text(label = "Render Layer : ", align = "right") 
        self.inputRendLayer = mc.textField(pht='render layer name', w = 300 , text = self.rendLayer) 
        mc.text (label = "")
        mc.text (label = "")
        
        ##_Scene Assignment_##
        mc.text(label = "Scene : ", align = "right") 
        self.inputSceneAssignment = mc.textField(pht='Scene_One, Scene_Two, etc', w = 135)
        mc.setParent("..")
        mc.setParent("..")

        ###_Attach commands to GUI_###
        mc.textField(self.name, edit=True)
        mc.intField(self.start, edit=True)
        mc.intField(self.end, edit=True)
        mc.textField(self.inputDirectory, edit=True)
        mc.textField(self.inputPrefix, edit=True)
        mc.textField(self.inputRendLayer, edit=True)
        mc.textField(self.inputSceneAssignment, edit=True)

###################____RENDER____########################
    #Update render settings before starting another sequence
    def UpdateRenderGlobals(self, start, end, prefix, skip):
        mc.setAttr("defaultRenderGlobals.startFrame", start)
        mc.setAttr("defaultRenderGlobals.endFrame", end)
        mc.setAttr("defaultRenderGlobals.imageFilePrefix", prefix, type = "string")
        mc.setAttr("defaultRenderGlobals.skipExistingFrames", skip)

    #Sets cameras as renderable/non-renderable
    def SetCamAsRenderable(self, cam, enable):
        if mc.objExists(cam):
            mc.setAttr("%s.renderable" % cam, enable)
        else:
            mc.warning("SetCamAsRenderable() -- Camera could not be found. Please check spelling of camera component")

    def DisableCams(self, cam):
        if mc.objExists(cam):
            mc.setAttr("%s.renderable" % cam, 0)
        else:
            mc.warning("SetCamAsRenderable() -- Camera could not be found. Please check spelling of camera component")


    #Looks through specified camera for render
    def LookThroughCam(self, cam):
        if mc.objExists(cam):
            mc.lookThru(cam)
            #mc.lookThru(cam, q=True )
            mel.eval("getCurrentCamera()")
        else:
            mc.warning("LookThroughCam() -- Unable to look through camera that doesn't exist. Please check spelling of camera component")

    #Must run before starting RenderSequence
    def GetCurrent():
        mel.eval("getCurrentCamera()")

    def SetDirectory(self, directory):   
        self.directory = self.inputDirectory
        
    def SetRenderLayer(self, activeLayer):

        foundLayer = False
        defaultRenderLayer = "defaultRenderLayer"
        # Get the list of all render layers
        render_layer_list = mc.ls(type="renderLayer")
        print render_layer_list
        
        if "rs_" in activeLayer:
            print("rs_ found in render layer")
        else:
            activeLayer = "rs_" + activeLayer
            mc.warning("'rs_' not found in render layer, added rs_ to front of string")
            mc.warning ("new string =    " + activeLayer)
            
        for i in render_layer_list:    
            if i == activeLayer:
                mc.warning("Render Layer found:  " + activeLayer)
                mc.editRenderLayerGlobals (currentRenderLayer = activeLayer)
                foundLayer = True
                return
            else:
                print 'render layer match has not been found'
                foundLayer = False
                
        if foundLayer == False:
            for x in render_layer_list:
                if x == defaultRenderLayer:
                    mc.editRenderLayerGlobals (currentRenderLayer = defaultRenderLayer)
        
    def BeginSequenceRender(self, start, end):
        mc.RenderSequence( s=start, e=end, RenderAllCameras=True)



###################____BROWSE FILE PATH____#######################     

    def BrowseFilePath(self, fileMode, fileFilter, textField, *args):
        returnPath = mc.fileDialog2(fm = fileMode, fileFilter = fileFilter, ds = 2)[0]
        mc.textField(self.inputDirectory, edit = True, text = returnPath)
        self.directoryText = returnPath
        print ("Directory found :   " + str(self.inputDirectory))
        return returnPath


###################____"STORE"_FRAMES_FROM_RENDER_TO_SPEFICIC_FOLDER____#######################  
def copyTree(src, dst, folderName, name, symlinks=False, ignore=None):
    #create new folder in dst path, named after respective camera
    dst = dst + "/" + folderName
    if os.path.isdir(dst):
        print ("Huzzah! Destination folder already exists. Congrats.")
    else:
        mc.sysFile(dst, makeDir=True )

    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        d = d + "\frame_one__0001"
        print "SRC ::::  " + s
        print "DST :::: ThisOne " + d
        #d = d + "\frame_one__0001"
        if os.path.isdir(s):
            shutil.copy2(s, d)
        else:
            shutil.copy2(s, d)
        
        if os.path.exists(s):
            if os.path.isfile(d):
                mc.warning ("Confirmed that %s has been successfully copied into target directory. Proceeding to delete tmp file." % s)
                os.remove(s)  #Deletes file in temp folder after it has been successfully copied to other directory
        else:
            mc.warning("%s does not exist. Cannot remove." % d)



#ToDo : Find a way to store values between windows closing. Check 'remember values' box in window to reload those same preferences. Save/read from pref file (?)


###################___Reading_information_from_file_placeholder___######
#        for file in files:
#            if file.rpartition(".")[2] == "mb":
#                
#                #main loop
#                fileName = inputDirectory + "/" + file
#                mc.file(fileName, open = True, force = True, ignoreVersion = True, prompt = False)
#                
#                #do stuff
#                print 'reading the txt  files down here'
#
#   text file should read as follows:
#       amount of sequences 
#           -- per sequence
#       cam name, start, end, skip, image directory, prefix