#Move cluster origin to joint position
import maya.cmds as mc

def MoveOrigin():
    selList = mc.ls(sl = True, transforms = True)

    cluster = mc.listRelatives(selList[0], shapes= True)
    pivotPoint_ws = mc.ls(sl = True, type = "joint")

    #Get world space for joint object
    jointPOS = mc.xform(pivotPoint_ws, piv=True , q=True , ws=True)

    clusterOrigin = mc.getAttr(str(selList[0]) + ".origin")
    mc.setAttr(str(selList[0]) + ".originX", jointPOS[0])
    mc.setAttr(str(selList[0]) + ".originY", jointPOS[1])
    mc.setAttr(str(selList[0]) + ".originZ", jointPOS[2])
    #pos = cmds.getAttr("{0}.origin".format(cluster))[0]

#import moveClusterOrigin as MCO
#reload(MCO)

#MCO.MoveOrigin()