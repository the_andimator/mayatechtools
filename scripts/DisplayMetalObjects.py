import maya.cmds as mc
import maya.mel as mel
import pymel.core as pm
import maya.OpenMaya as OpenMaya

global highlightingIsActive 
highlightingIsActive = False

class DisplayMetalObjects(object):
    #Create light lists using the 4 groups that are assigned
    #Locate and store HDRI image (check light link script to ensure that this map is able to be added to anything)

    #overrides  = list of meshes([]), enable/disable (bool), color(x,x,x), blend color (bool)

    #based on override value:
        #turn on visible verts for provided meshes ()
        #change vert colors
        #change size of visible verts
    def __init__(self):
        hdri_list = self.hdri_list = []
        misc_list = self.misc_list = []
        default_list = self.default_list = []
        all_list = self.all_list = []
        none_list = self.none_list = []

        #possible variables that will be used
        newColor = self.newColor = []
        newAlpha = self.newAlpha = []

#(listOfMeshes, color, True, 3)
    def AssignVertColors(self, objects, color, visible, size):
        for i in objects:
            
            item = pm.listRelatives(i, fullPath=False, parent=True)

            sel = item[0]
            mc.warning("sel:     %s" % (sel))
            mc.warning("color:     %s" % (color))
            
            shape = sel.getShape()

            mobj = shape.__apimobject__()
            meshFn = OpenMaya.MFnMesh(mobj)

            vertexColorList = OpenMaya.MColorArray()
            normalsList = OpenMaya.MFloatVectorArray()

            meshFn.getVertexColors(vertexColorList)
            meshFn.getNormals(normalsList)

            lenVertexList = vertexColorList.length()

            fnComponent = OpenMaya.MFnSingleIndexedComponent()
            fullComponent = fnComponent.create( OpenMaya.MFn.kMeshVertComponent )

            fnComponent.setCompleteData( lenVertexList );

            vertexIndexList = OpenMaya.MIntArray()
            fnComponent.getElements(vertexIndexList)

            for k in range(lenVertexList):
                vertexColorList[k].r = color[0]
                vertexColorList[k].g = color[1]
                vertexColorList[k].b = color[2]

            meshFn.setVertexColors(vertexColorList,vertexIndexList, None)
            objects = i + ".displayVertices"
            if mc.objExists(objects):
                mc.setAttr(objects, 1)
                mc.setAttr(i + ".vertexSize", 6)
            

    def get_mobject(self, node):
        selectionList = OpenMaya.MSelectionList()
        selectionList.add(node)
        oNode = OpenMaya.MObject()
        selectionList.getDependNode(0, oNode)
        return oNode


    def SetHighlighting(self, meshes, state):
        #meshes = mc.ls(geometry = True, shapes = False,  type='geometryShape', showType=True)
        for i in meshes:
            mesh = i + ".displayVertices"
            if mc.objExists(mesh):
                mc.setAttr(mesh, state)
                if state == True:
                    mc.setAttr(i + ".vertexSize", 6)
                else:
                    mc.setAttr(i + ".vertexSize", 3)

                mc.setAttr(i + ".displayVertices", state)



#::::NOTES::::
#https://stackoverflow.com/questions/726549/algorithm-for-additive-color-mixing-for-rgb-values