import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import glob
import os
import Read_Json as readJson

class DeleteJsonFiles(object):

    def __init__(self):
        try:
            mc.deleteUI("Delete_Json", window = True)
        except:
            pass

        if mc.window("Delete_Json", exists = True):
            mc.deleteUI("Delete_Json")

        self.Logs = RenderableSequences = []

    def CreateRenderWindow(self):
        try:
            mc.deleteUI("Delete_Json", window = True)
        except:
            pass

        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "Delete_Json"

        try:
            mc.deleteUI(WinName)
        except:
            pass

#        ##fill UI by running a for loop of Fill Sequence UI
#        #Grab Scene Sequence Timing
#        rendSelect = mc.ls("Scene_Sequence_Timing", r= True)
#        #Find child nodes, store in list
#        #renderNodes = mc.listRelatives(rendSelect, c = True, shapes=True)
        self.renderNodes = mc.container("Scene_Sequence_Timing",q = True, nl = True)

        winHeight = 250

        prepWin = mc.window("Delete_Json", w =435, h = winHeight, mnb = False, mxb = False, rtf = True, s = 0)
        
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 435, h= winHeight)

        ###Create Border and Frames####
        rowColumnParent = pm.rowColumnLayout(numberOfColumns=7, columnWidth = [(1,25), (2,125), (3,25), (4,100), (5,50), (6,75), (7,25)])


        mc.text(label = "", align = "center")
        mc.text(label = "Name of Sequence", align = "center", fn ="boldLabelFont")
        mc.text(label = "", align = "center")
        mc.text(label = "Number of Logs", align = "center", fn ="boldLabelFont")
        mc.text(label = "", align = "center")
        mc.text(label = "Delete Logs", align = "center", fn ="boldLabelFont")
        mc.text(label = "", align = "center")

        for x in self.renderNodes:
            mc.text(label = "", align = "center")
            mc.text(label = "", align = "center")
            mc.text(label = "", align = "center")
            mc.text(label = "", align = "center")
            mc.text(label = "", align = "center")
            mc.text(label = "", align = "center")
            mc.text(label = "", align = "center")
            
            mc.text(label = "", align = "center")
            mc.text(label = x, al = "center")
            mc.text(label = "", align = "right")
            mc.text(label = self.FindJsonFiles(x, False), align = "center")
            mc.text(label = "", align = "right")
            pm.checkBox(str(x + "_render"), l = '', v = True, al = "right")
            mc.text(label = "", align = "center")
      
            #Fill text with estimated render times


#        #Separate Attributes (Sequence #, render layer, start, end) into individual lists
#        #Add Button for rendering at bottom
        mc.text(label = "", align = "center")
        mc.setParent("..")
        mc.text(label = "", align = "center")
        mc.setParent("..")
        ###Create Border and Frames####
        pm.rowColumnLayout( numberOfColumns=1, columnWidth = [(1,444)])
        
        mc.button(l="Delete Logs", w = 435, c =  lambda *_: self.DeleteLogs())

        mc.showWindow()
        

    def FindJsonFiles(self, sequenceNum, path):
        LogNames = []
        RenderDir = self.FillSequenceUI(sequenceNum, ".RenderStatsDir")
        
        if RenderDir != None:
            logName = "%s/%s*.json" % (RenderDir, sequenceNum)
            logs = glob.glob(logName)
            
            if len(logs) > 0:
                for file in logs:
                    LogNames.append(file)
        else:
            if path == True:
                return None
            else:
                return 0
        
        if path == True:
            return LogNames
        else:
            return len(LogNames)
        
    def FillSequenceUI(self, seq, attr):
        info = mc.getAttr(seq + attr)
        return info           

    def DeleteLogs(self):
        
        logsToDelete = []
        
        for x in self.renderNodes:
            print ''

            deleteLog = pm.checkBox(str(x + "_render"), q = True, v = True)
            
            if deleteLog == True:
               logsToDelete.append( self.FindJsonFiles(x, True) )
            
        mc.warning(logsToDelete)
        
        mc.warning("Deleting Logs Now..")  
              
        for seqLog in logsToDelete:
            if seqLog != None:
                for i in seqLog:
                    if i != None:
                        mc.warning("logsToDelete:    "+str(i))
            
                        if os.path.exists(i):
                            os.remove(i)

        self.CreateRenderWindow()
#import Read_Json as readJson
#from functools import partial