import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import sys as sys
from functools import partial as ftool
import shutil as shutil
from os import listdir
import os
 
class Verify(object):
 #This checks inputs of all sequences to make sure they are valid
    def VerifyInputs(self, name, rendLayer, start, end, directory, prefix, sceneAssignment):
        verified = []
        passFail = True
        if name != None:
            print''
            #Check to make sure that the cameras exist
            if mc.objExists(name):
                print ("verified " + name + " exists")
            else:
                print (name + " could not be verified, aborting render process")
                passFail = False
            #Check to make sure the render layers exist
            if self.CheckRenderLayer(rendLayer) == False:
                passFail = False
                mc.warning("Render Layer doesn't exist. Aborting render process")
            #Check to make sure that start/end frames are int values
            if start > end:
                passFail = False
                mc.warning("start frame is larger than end frame. Aborting render process")

            #Check file directory for existing folders and files
            verified.append(passFail)
            for check in verified:
                if check == False:
                    passFail = False
            return passFail
        else:
            return False
        
    def CheckRenderLayer(self, activeLayer):
        #mc.warning("CUR RENDER LAYER:  " + activeLayer)
        foundLayer = False
        defaultRenderLayer = "defaultRenderLayer"
        render_layer_list = mc.ls(type="renderLayer")
        
        if "rs_" in activeLayer:
            print("")
        else:
            activeLayer = "rs_" + activeLayer
 
        for i in render_layer_list:  
            if i == activeLayer:
                foundLayer = True
                break
            else:
                foundLayer = False
            
        return foundLayer

    def CheckDirectory(self, directory, renderLayer, prefix, sceneAssignment):
        #Get folder name
        import CopyToLocation as CL
        reload (CL)
        
        # Get the list of all render layers. Find render layer to append to find folder.
        render_layer_list = mc.ls(type="renderLayer")
        folderRL = renderLayer

        #Get Target directory
        #Append folder name to target directory
        targetDir = directory + "/" + sceneAssignment + "/" + folderRL
        
        #Get tmp directory
        projectRoot = mc.workspace(q=1,rd=1)
        tmpRoot = projectRoot + "images/tmp/"
        destination = tmpRoot + folderRL
        


        #confirm if it exists
        #if it exists, copy target to tmp directory
        mc.warning (targetDir + "  TARGET DIR")
        mc.warning (destination + "  DESTINATION DIR")
        mc.warning (tmpRoot + "  tmpRoot DIR")
        if os.path.isdir(targetDir):
            print ("Destination folder exists: %s" % targetDir)
            CL.DeleteTemp(destination) #purges any files currently in temp. This shouldn't backfire at all..
            
            if os.path.isdir(destination):
                print "Do nothing"
            else:
                CL.CopyExisting(targetDir, destination)
            #copyTree(targetDir, destination, sceneAssignment, name, rendLayer, False, None)
        else:
            mc.warning("UNABLE TO FIND THIS DIRECTORY:  " + targetDir)

            desktopPass = "\Desktop"
            folderName = "\StoredRenders"
            homepath = os.environ['HOMEPATH']
            backupDestination = homepath + desktopPass + folderName
            #print backupDestination

            try:
                os.path.exists(backupDestination)
                print "path exists, awesome"
            except:
                #os.makedirs(backupDestination)
                CL.CopyExisting(targetDir, backupDestination)
                mc.warning("Unable to find originally directory. Images have been copied to the desktop.")
                mc.warning('new location:     ' + backupDestination)