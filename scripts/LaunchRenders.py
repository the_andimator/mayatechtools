import maya.cmds as mc
import os.path
import maya.mel as mel
import pymel.core as pm
import glob
import os
import os.path as path
import sys
from os.path import expanduser
import Read_Json as readJson
from functools import partial

class LaunchRenders(object):
    global renderLayerWin
    renderLayerWin = ''

    toolsetVer = "01.20"

    def __init__(self):
        try:
            mc.deleteUI(windowName, window = True)
        except:
            pass

        if mc.window("Sequencer", exists = True):
            mc.deleteUI("Sequencer")

        self.SequenceAttr = SequenceAttr = []
        self.RenderableSequences = RenderableSequences = []
        
        user = (os.path.abspath(os.path.join(expanduser("~"), '..' )))
        conda = "Anaconda2\envs\Python27\Lib\site-packages"
        pythonPath = os.path.join(user, conda)
        sys.path.append(pythonPath)

    def CreateRenderWindow(self):
        try:
            mc.deleteUI("Sequencer", window = True)
        except:
            pass

        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        WinName = "Sequencer"

        try:
            mc.deleteUI(WinName)
        except:
            pass

#        ##fill UI by running a for loop of Fill Sequence UI
#        #Grab Scene Sequence Timing
#        rendSelect = mc.ls("Scene_Sequence_Timing", r= True)
#        #Find child nodes, store in list
#        #renderNodes = mc.listRelatives(rendSelect, c = True, shapes=True)
        self.renderNodes = mc.container("Scene_Sequence_Timing",q = True, nl = True)

        winHeight = 275

        prepWin = mc.window("Sequencer", w =300, h = winHeight, mnb = True, mxb = True, s = 1)
        if(mc.frameLayout(self.baseColumnLayout, exists=True)):
            mc.deleteUI(self.baseColumnLayout)
        mainLayout = mc.scrollLayout(self.baseColumnLayout, w = 1120, h= winHeight)

        #Create menu items
        menuBarLayout = mc.menuBarLayout()
        mc.menu( label='File' )
        mc.menuItem( label='Generate Batch File', i = "sortByDate.bmp", c = lambda *_: self.GenerateBatchFile())
        mc.menuItem( label='Launch Legacy Sequencer', i = "icon_rendersequence.png", c = lambda *_: self.LaunchLegacy())
        mc.menuItem( label='Re-Calculate Render Time', i = "icon_clock.png", c = lambda *_: self.FindRenderTimes())
        mc.menuItem( label='Delete Log Files...', i = "DeleteLogs.png", c = lambda *_: self.DeleteLogFiles())
        ###Create Border and Frames####
        top = mc.separator("top",style='in', width=1120)
        pm.rowColumnLayout( numberOfColumns=6, columnWidth = [(1,685), (2,45), (3,75), (4,43), (5,80), (6,180)])

        mc.text(label = "", align = "center")
        mc.text(label = "Render", align = "left")
        mc.text(label = "Skip Existing", align = "center")
        mc.text(label = "Priority", align = "right")
        mc.text(label = "Directory", align = "right")
        mc.text(label = "  Render Estimate", align = "center")


        for x in self.renderNodes:
            cam = self.FillSequenceUI(x, ".CameraName")
            rl = self.FillSequenceUI(x, ".RenderLayer")
            start = self.FillSequenceUI(x, ".Start")
            end = self.FillSequenceUI(x, ".End")
            skip = self.FillSequenceUI(x, ".SkipExisting")
            directory = self.FillSequenceUI(x, ".FileDirectory")

#            ###Create Border and Frames####
            instanceFrameLayout = mc.frameLayout(x, parent = prepWin, label = x, cll = False, bv = False)
            instancerowColumnLayout = mc.rowColumnLayout(x, nc = 14, cw = [(1,100), (2,165), (3,85), (4,175), (5,40), (6,50), (7,40), (8,50), (9,60), (10,50), (11,40), (12,30), (13,45), (14,180)], columnOffset = [(1, "both", 2), (2, "both", 2), (3, "both", 0)])

            ###_Render_Layer_###
            mc.text(label = "Camera : ", align = "right")
            mc.textField(text = cam, ed= False)

            ###_Render_Layer_###
            mc.text(label = "Render Layer : ", align = "right")
            mc.textField(text = rl, ed= False)

            ###_Start_###
            mc.text(label = "Start : ", align = "right")
            mc.intField(v = start, ed = False)

            ###_End_###
            mc.text(label = "End : ", align = "right")
            mc.intField( v = end,  ed = False)

            render = pm.checkBox(str(x + "_render"), label = "", v = True, ed= True, al = "center", changeCommand = partial (self.UpdateTotalEstimate))
            skip = mc.checkBox(label = "", v = True, ed= skip, al = "center")
            mc.intField(str(x + "_priority"), ed= True)
            mc.text(l="")
            
            #check to see if directory is valid
            
            directoryValid = mc.textField(bgc = self.CheckDirectory(directory, False), ed= False, text = self.CheckDirectory(directory, True))
            
            #Fill text with estimated render times
            renderEstimate = mc.text(label = self.GetEstimateRenderTime(x, False), al = "center")

#        #Separate Attributes (Sequence #, render layer, start, end) into individual lists
#        #Add Button for rendering at bottom
        bottom = mc.separator("bottom", style='in', width=1110)
        pm.rowColumnLayout(numberOfColumns=3, columnWidth = [(1,400), (2,50), (3,100)])
        mc.setParent("..")
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        mc.text(l="")
        mc.setParent("..")
        
        instancerowColumnLayout = mc.rowColumnLayout('bottomRow', nc = 2, cw = [(1,967),(14,180)], columnOffset = [(1, "both", 2), (2, "both", 2)])
        mc.text(l=" Total Time:   ", al = 'right', fn ="boldLabelFont")
        mc.text("Total_Time",l=self.GetTotalTime(), al = "left", fn ="boldLabelFont")
        mc.text(l="", al = "center")
        mc.text(l="", al = "center")
        bottom = mc.separator("bottom_sep", style='in', width=1110)
        mc.setParent("..")
        
        mc.button(l="Start Render", w = 100, c =  lambda *_: self.LaunchRender(self.renderNodes, False))

        mc.showWindow()
        
    def CheckDirectory(self, x, rType):  
        if x == None:
            if rType == True:
                return "No Dir"
            else:
                return (.7,0,0)
        elif os.path.exists(x):
            if rType == True:
                return " valid"
            else:
                return (0,1,0)
        else:
            if rType == True:
                return "invalid"
            else:
                return (.9,.6,0)
                
    def GetTotalTime(self):

        TotalTime = []
        TotaledSeconds = 0
        Total_Hours = 0
        Total_Minutes = 0
        Total_Seconds = 0

        for x in self.renderNodes:
            TotalTime.append(self.GetEstimateRenderTime(x, True))

        for i in TotalTime:

            #this checks 
            if isinstance(i, str) == False:
                TotaledSeconds += i

        return "%s hr %s min %s sec" % (readJson.ConvertTotalToMinutes(TotaledSeconds))

    def GetEstimateRenderTime(self, sequenceNum, returnNum):
        reload(readJson)
        totalSeconds = []
        Total_Hours = 0
        Total_Minutes = 0
        Total_Seconds = 0
        Average_Sec = 0
        
        RenderDir = self.FillSequenceUI(sequenceNum, ".RenderStatsDir")
        
        numFrames = (self.FillSequenceUI(sequenceNum, ".End") - self.FillSequenceUI(sequenceNum, ".Start"))
        
        renderable = pm.checkBox(str(sequenceNum + "_render"),v = True, q= True)
        
        
        logName = "%s/%s*.json" % (RenderDir, sequenceNum)
        
        logs = glob.glob(logName)
        
        if len(logs) > 0:
            for file in logs:
                total = readJson.ParseJson(file)
                
                totalSeconds.append(total)
                #Total_Hours.append(hours)
                #Total_Minutes.append(minutes)
                #Total_Seconds.append(seconds)
                
            for i in totalSeconds:
                Total_Seconds += i
            
            Average_Sec = float(Total_Seconds) / float(len(logs))
            #mc.warning("Average Seconds:   %s" % (Average_Sec))
            #mc.warning("Total Seconds:   %s" % (Total_Seconds))
            #mc.warning("Num of Logs:   %s" % (len(logs)))
            
            Total_Hours, Total_Minutes, Total_Seconds = readJson.ConvertTotalToMinutes(int(Average_Sec * numFrames))
            
            
            #mc.warning ("Total:    %s" % (Total_Seconds) )
            #mc.warning ("Total_Hours:    %s" % (Total_Hours) )
            #mc.warning ("Total_Minutes:    %s" % (Total_Minutes) )
            #mc.warning ("Total_Seconds:    %s" % (Total_Seconds) )
            
            #self.GetTotalTime(Total_Hours, Total_Minutes, Total_Seconds)
            
            #mc.warning( 'Name Of Sequence:    %s' % sequenceNum)
            if returnNum == True:
                if renderable == True:
                    return int(Average_Sec * numFrames)
                else:
                    return str(Average_Sec * numFrames)
                #return 
            else:
                return "%s hr %s min %s sec" % (Total_Hours, Total_Minutes, Total_Seconds)
        else:
            return "No Render Logs"
            
        
    def FillSequenceUI(self, seq, attr):
        info = mc.getAttr(seq + attr)
        return info

    def GenerateBatchFile(self):
        self.LaunchRender(self.renderNodes, True)

    def LaunchLegacy(self):
        from mtoa.core import createOptions
        createOptions()
        mel.eval('optionVar -intValue "renderSequenceAllCameras" 1;')

        import Render_Sequencer as MI
        reload (MI)
        build = MI.SequenceSetup(1)
        build.PrepareWindow()
        
    def FindRenderTimes(self):
        print 'Calculating Render Time'


    def LaunchRender(self, renderNodes, batch):
        #mc.warning("Render is launched")
        seq = 0
        seqAttr = 0
        verified = []
        result = True
        SingleSequence = []
        self.SequenceAttr = []
        self.RenderableSequences = []

        #Changes renderAllSequenceCameras to True in render sequencer options. This is very important, disabling this option breaks camera switching when doing renders.
        mel.eval('optionVar -intValue "renderSequenceAllCameras" 1;')


        #Store values in their own lists
        from mtoa.core import createOptions
        createOptions()

        import CopyToLocation as CL
        reload (CL)

        #Find how many sequences there are
        for x in renderNodes:

            name = self.FillSequenceUI(x, ".CameraName")
            start = self.FillSequenceUI(x, ".Start")
            end = self.FillSequenceUI(x, ".End")
            directory = self.FillSequenceUI(x, ".FileDirectory")
            prefix = self.FillSequenceUI(x, ".Prefix")
            renderLayer = self.FillSequenceUI(x, ".RenderLayer")
            scene = self.FillSequenceUI(x, ".Scene")
            skip = self.FillSequenceUI(x, ".SkipExisting")

            renderable = mc.checkBox(str(x + "_render"), q = True, v = True)#self.FillSequenceUI(x, ".Render")
            priority = mc.intField(str(x + "_priority"), q = True, v = True)

            #mc.warning(renderable)
            #mc.warning(priority)

            if os.path.isdir(directory):
                #Collate information into a single list
                SingleSequence = SequenceAttributes(name, start, end, renderLayer, prefix, scene, directory, skip, renderable, priority)

                self.SequenceAttr.append(SingleSequence)
            else:
                if directory == "":
                    mc.error("\n\n No directory found for %s. Please input a directory" % (x))
                    break
                else:
                    mc.error("\n\n %s directory is invalid. Input valid directory and try again.\n Invalid Directory: %s" % (x, directory), noContext = False, sl = False)
                    break



        #Filter For Non-Renderable Sequences
        for x in self.SequenceAttr:
            if self.FindRenderable(x) == True:
                self.RenderableSequences.append(x)
            else:
                continue


        #Reorder by priority number (0's are ordered by sequence # and appended to end of list)
        self.RenderableSequences = self.SortRenderLayers(self.RenderableSequences)

        #Verify inputs of renderable sequences
        for x in self.RenderableSequences:
            import Verify as veri
            reload (veri)
            verify = veri.Verify()
            verified.append(verify.VerifyInputs(x.name, x.renderLayer, x.start, x.end, x.directory, x.prefix, x.scene))

        for v in verified:
            if v == False:
                result = False

        if result == True:
            if batch == False:
                for x in self.RenderableSequences:
                    #Disables all other cams from being renderable
                    verify.CheckDirectory(x.directory, x.renderLayer, x.prefix, x.scene)
                    self.SetCamAsRenderable(x.name, 1) #Changes camera to renderable state
                    self.LookThroughCam(x.name) #Looks through active camera

        #################__Send Sequences to be Rendered__########
                    self.UpdateRenderGlobals(x.start, x.end, x.prefix, x.skip) #Changes render globals to match start/end frames, image prefix, and whether to skip existing frames
                    #self.SetDirectory(directory) #changes render directory
                    self.SetRenderLayer(x.renderLayer)

                    ##Disable persp as renderable camera"
                    mc.setAttr("perspShape.renderable", 0)
                    mc.setAttr("perspShape.visibility", 0)
                    mc.setAttr("persp.visibility", 0)
                    mel.eval("getCurrentCamera()")

                    if(x == 0):
                        mc.RenderSequence( s=x.start, e=x.end, RenderAllCameras=False) #Starts Sequence Render
                    else:
                        mc.RenderSequence( s=x.start, e=x.end)

                    self.SetCamAsRenderable(x.name, 0)
                    mc.setAttr("%s.visibility" % x.name, 0)

                    projectRoot = mc.workspace(q=1,rd=1)
                    src = projectRoot + "images/tmp/"
                    dst = x.directory
                    print "SCENE :::   " + x.scene

                    CL.copyTree(src, dst, x.scene, x.name, x.renderLayer, False, None)
                    mc.pause (sec = 2)
                    mc.refresh()

            elif batch == True:
                SequenceList = []
                #Iterate through sequences
                for i in self.RenderableSequences:
                    SequenceList.append(self.CreateString(i.name, i.start, i.end, i.renderLayer, "tif", i.skip , i.prefix))

                mc.warning(SequenceList)

                import AssGeneration as AG
                reload (AG)
                AG.OpenUI(SequenceList)
                    #each sequence creates a string
                    
                    #each string is appended to a list

        elif result == False:
            mc.warning("Errors found in sequencer input fields. Double check to make sure everything is correct")

    def SetCamAsRenderable(self, cam, enable):
        if mc.objExists(cam):
            mc.setAttr("%s.renderable" % cam, enable)
        else:
            mc.warning("SetCamAsRenderable() -- Camera could not be found. Please check spelling of camera component")

    # Looks through specified camera for render
    def LookThroughCam(self, cam):
        if mc.objExists(cam):
            mc.lookThru(cam)
            #mc.lookThru(cam, q=True )
            mel.eval("getCurrentCamera()")
        else:
            mc.warning("LookThroughCam() -- Unable to look through camera that doesn't exist. Please check spelling of camera component")

        # Update render settings before starting another sequence
    def UpdateRenderGlobals(self, start, end, prefix, skip):
        mc.setAttr("defaultRenderGlobals.startFrame", start)
        mc.setAttr("defaultRenderGlobals.endFrame", end)
        mc.setAttr("defaultRenderGlobals.imageFilePrefix", prefix, type = "string")
        mc.setAttr("defaultRenderGlobals.skipExistingFrames", skip)

        # Must run before starting RenderSequence
    def GetCurrent():
        mel.eval("getCurrentCamera()")

    def SetRenderLayer(self, activeLayer):

        foundLayer = False
        defaultRenderLayer = "defaultRenderLayer"
        # Get the list of all render layers
        render_layer_list = mc.ls(type="renderLayer")

        if "rs_" in activeLayer:
            print("rs_ found in render layer")
        else:
            activeLayer = "rs_" + activeLayer
            mc.warning("'rs_' not found in render layer, added rs_ to front of string")
            mc.warning ("new string =    " + activeLayer)

        for i in render_layer_list:
            if i == activeLayer:
                mc.warning("Render Layer found:  " + activeLayer)
                mc.editRenderLayerGlobals (currentRenderLayer = activeLayer)
                foundLayer = True
                return
            else:
                foundLayer = False

        if foundLayer == False:
            for x in render_layer_list:
                if x == defaultRenderLayer:
                    mc.editRenderLayerGlobals (currentRenderLayer = defaultRenderLayer)

    def FindRenderable(self, sequence):
        if sequence.render == True:
            return True
        else:
            return False

    def SortRenderLayers(self, sequence):
        zero = []
        priority = []
        SortedArray = []
        #Split priority #'s into two groups, one for 0's and the other for everything else
        for x in sequence:
            if self.GetKey(x) == 0:
                zero.append(x)
                print ''
            else:
                priority.append(x)
                print ''

        sortPriority = sorted(priority, key = self.GetKey)

        for i in sortPriority:
            SortedArray.append(i)
        for z in zero:
            SortedArray.append(z)
        for sort in SortedArray:
            mc.warning(sort .name)

        return SortedArray

    def GetKey(self, num):
        return num.priority

    def CreateString(self, name, start, end, renderlayer, format, skip, prefix):
        sequenceString = "-cam %s -s %s -e %s -rl %s -of %s -skipExistingFrames %s -im %s" % (name, start, end, renderlayer, format, True, prefix)
        return sequenceString
        
    def UpdateTotalEstimate(self, *args):
        pm.text("Total_Time", l=self.GetTotalTime(), e= True, fn ="boldLabelFont")

    def DeleteLogFiles(self):
        #find list of logs
        print 'bring up window to delete log files'
        
        import DeleteJsonLogs as DJL
        reload (DJL)
        load = DJL.DeleteJsonFiles()
        load.CreateRenderWindow()

class SequenceAttributes (object):

        def __init__(self, name, start, end, renderLayer, prefix, scene, directory, skip, render, priority):
            self.name = name
            self.start = start
            self.end = end
            self.renderLayer = renderLayer
            self.prefix = prefix
            self.scene = scene
            self.directory = directory
            self.skip = skip
            self.render = render
            self.priority = priority

        def ReturnValues(self):
            values = [self.name, self.start, self.end, self.renderLayer, self.prefix, self.scene, self.directory, self.skip, self.render, self.priority]
            return values

