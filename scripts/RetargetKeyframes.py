import maya.cmds as mc
import pymel.core as pm

class RetargetKeyframes(object):
    
    def __init__(self):
        try:
            mc.deleteUI(windowName, window = True)
        except:
            pass
        
        useHierarchy = 'useHierarchy'
        cutKeys = 'cutKeys'
        #Class variables
        self.useHierarchy = useHierarchy
        self.cutKeys = cutKeys
                
    def BuildUI(self, windowName):

        WinName = windowName
        
        try:
             mc.deleteUI(WinName, window = True)
        except:
            pass
            
        selection = mc.ls(sl=True)
        
        if (len(selection) < 2):
            mc.error("Select two objects")

        source, target = selection[0], selection[1]
        
        print source, target
        
        self.baseColumnLayout = baseColumnLayout = "base_ColumLayout"
        
        dWin = mc.window(windowName, title = windowName, wh=(325,256), sizeable = False, rtf = True)
        mc.columnLayout(cat=('both', 0), rowSpacing=5, columnWidth=275, cal = "left", co = ("left",100))
        mc.text(label = "Copy Keyframes:")

        self.sourceField = mc.textFieldGrp("Source", label='Source_Skeleton', text = source, w = 256)
        self.targetField = mc.textFieldGrp("Target", label='Target_Skeleton', text = target)
        mc.checkBox(self.useHierarchy, label = "Hierarchy", v = True)
        mc.checkBox(self.cutKeys, label = "Delete Keyframes from Target First", v = True)
        pm.button(label = "Copy Keyframes", command = lambda *args: self.CopyKeys(source, target))
        mc.showWindow(dWin)
     
    def CopyKeys(self, src, target):
        hierarchy = mc.checkBox(self.useHierarchy, q= True, v= True)
        print hierarchy
        
        cutkeys = mc.checkBox(self.cutKeys, q= True, v= True)
        print cutkeys
        
        if cutkeys == True:
            if hierarchy == True:
                target_children = mc.listRelatives(target, allDescendents=True, type='joint')
                all_target = target_children + [target]
                
                print "all_target:    " + str(all_target)
                for joint in all_target:
                    print "joint:   " + str(joint)
                    mc.cutKey(joint, s=True)
            else:
                mc.cutKey(target, s=True)
        
        if hierarchy == True:
            src_children = mc.listRelatives(src, allDescendents=True, type='joint')
            target_children = mc.listRelatives(target, allDescendents=True, type='joint')
            
            all_src = src_children + [src]
            all_target = target_children + [target]
            
            print all_src
            print all_target

            counter = 0
            for src_joint in all_src:
                print "src_joint:  " + str(src_joint)
                print "target:  " + str(all_target[counter])
                self.CopyKeyframes(src_joint, all_target[counter])
                counter += 1
        else:
            self.CopyKeyframes(src, target)
            
        
    def CopyKeyframes(self, src, target):
        
        animAttr = mc.listAnimatable(src)

        for attribute in animAttr:
            numKeyframes = mc.keyframe(attribute, query=True, keyframeCount=True)

            if (numKeyframes > 0):
                
                mc.copyKey(attribute)
                
                print "keyattr:   " + target
                print "attribute:   " + attribute
                mc.pasteKey(target, attribute=self.getAttName(attribute), option="replace")
        
        print ''
            
    def getAttName(self, fullname):
        parts = fullname.split('.')
        return parts[-1]


#import RetargetKeyframes as RK
#reload (RK)

#WinName = "Retarget Keyframes"

#try:
#	mc.deleteUI(WinName, window = True)
#except:
#	pass

#build = RK.RetargetKeyframes()
#build.BuildUI(WinName)