import maya.cmds as mc
import pymel.core as pm
import maya.OpenMaya as om
    
def FindSelectedCamera(selectedObj):
    shapeNode = mc.listRelatives(selectedObj, shapes=True)
    nodeType = mc.nodeType(shapeNode)

    if nodeType == "camera":
        return True
    else:
        return False

def CreateCtrl(name, material, height, camera, camPos, camRot, offset):
    print ''
    ctrl = mc.circle(n = name, nr=(0, 0, 1), c=(0, 0, 0), sw=180, r=6 )
    
    mc.setAttr(name + ".rotateX", 90)
    mc.setAttr(name + ".rotateY", 90)
    mc.move(0, camPos[1], 0, name)
    
def CreatePlane(name, material, height, camera, camPos, camRot, offset):
    if mc.objExists(name) == False:
        plane = mc.polyPlane(n = name, w = 12, h = height, sx = 3, sy = 1, ax = (0,10,0), cuv = 2, ch = 1)
    else:
        plane = name
        print "%s already exists.." % (name)
        
    mc.setAttr(str(name) + ".rotateX", 90)
    
    temp = getSGfromShader(material)
    shaderGroup = mc.rename(temp, material + "SG")
    mc.sets(plane, e=True, forceElement = shaderGroup)
    mc.move(0, camPos[1], 0, name)
    
    if offset == True:
        mc.setAttr(str(name) + ".translateZ", .1)
        
    SetNonRenderable(name)

def GetCameraPos(camera):
    X = mc.getAttr(camera + ".translateX")
    Y = mc.getAttr(camera + ".translateY")
    Z = mc.getAttr(camera + ".translateZ")
    return [X, Y, Z]


def GetCameraRot(camera):
    X = mc.getAttr(camera + ".rotateX")
    Y = mc.getAttr(camera + ".rotateY")
    Z = mc.getAttr(camera + ".rotateZ")
    return [X, Y, X]


def CreatePolyMove(name):
    center = name + ".vtx[1:2]", name + ".vtx[5:6]"
    right = name + ".vtx[7]", name + ".vtx[3]"
    left = name + ".vtx[4]", name + ".vtx[0]"

    tempEdge = pm.polyMoveVertex(left, right)
    edgeVerts = mc.rename(tempEdge, "%s_edge_verts_LOCKED" % (name))

    tempCenter = pm.polyMoveVertex(center)
    centerVerts = mc.rename(tempCenter, "%s_center_verts_LOCKED" % (name))

    mc.setAttr(name + ".inMesh", lock = True)

    
def LockMesh(name, state):
    mc.setAttr(name + ".inMesh", lock = state)


def EnableDOF(cam, state):
    mc.setAttr(cam + ".aiEnableDOF", state)

def CreateShaders(shaderName, rgb, trans):
    if mc.objExists(shaderName):
        print 'material already exists. Nice.'
    else:
        mc.shadingNode('lambert', name = shaderName, asShader=True)
        #check if shader exists, if false, create new shader
        mc.setAttr (shaderName + ".color", rgb[0], rgb[1], rgb[2])
        mc.setAttr (shaderName + ".transparency", trans, trans, trans) 
        
        SG = mc.sets(renderable=1, noSurfaceShader=1, empty=1, name= shaderName + 'SG')
        mc.connectAttr((shaderName +'.outColor'),(SG +'.surfaceShader'),f=1)

def getSGfromShader(shader=None):
    if shader:
        if mc.objExists(shader):
            sgq = mc.listConnections(shader, d=True, et=True, t='shadingEngine')
            if sgq: 
                return sgq[0]

    return None

def CreateHierarchy(dof, aperture, cam, ctrl):
    groupName = "%s_dof_grp" % (cam)

    mc.parent(aperture, dof)
    mc.setAttr(dof + ".translateX", 0)
    mc.setAttr(dof + ".translateY", 0)
    mc.setAttr(dof + ".translateZ", 0)
    mc.setAttr(dof + ".rotateY", 180)
    mc.setAttr(dof + ".rotateZ", 180)
    
    mc.group(dof, name = groupName)
    mc.parent(ctrl, groupName, r = True)
    
    distance = mc.getAttr(cam + '.aiFocusDistance')
    
    mc.setAttr(ctrl + ".translateX", 0)
    mc.setAttr(ctrl + ".translateY", 0)
    mc.setAttr(ctrl + ".translateZ", distance)
    mc.setAttr(ctrl + ".rotateX", 90)
    mc.setAttr(ctrl + ".rotateY", -90)
    mc.setAttr(ctrl + ".rotateZ", 0)
    
    mc.makeIdentity(ctrl, apply=True, r=1, s=1, n=0)
    
    mc.parent(dof, ctrl)
    mc.setAttr(groupName + ".translateX", 0)
    mc.setAttr(groupName + ".translateY", 0)
    mc.setAttr(groupName + ".translateZ", 0)
    mc.setAttr(groupName + ".rotateY", 180)
    
    mc.parent(groupName, cam, r = True)
    #create group, parent plane to group, parent aperture to plane
    

def LockNodes(dof, aperture, cam, ctrl, hide, lock):
    groupName = "%s_dof_grp" % (cam)
    #Lock/hide nodes
    mc.setAttr(ctrl + '.translateX', k=False,l=lock, channelBox = hide)
    mc.setAttr(ctrl + '.translateY', k=False,l=lock, channelBox = hide)
    mc.setAttr(ctrl + '.rotateX', k=False,l=lock, channelBox = hide)
    mc.setAttr(ctrl + '.rotateY', k=False,l=lock, channelBox = hide)
    mc.setAttr(ctrl + '.rotateZ', k=False,l=lock, channelBox = hide)
    mc.setAttr(ctrl + '.scaleX', k=False,l=lock, channelBox = hide)
    mc.setAttr(ctrl + '.scaleY', k=False,l=lock, channelBox = hide)
    mc.setAttr(ctrl + '.scaleZ', k=False,l=lock, channelBox = hide)
    #mc.setAttr(ctrl + '.v', k=False,l=lock, channelBox = hide)
    
    mc.setAttr(dof + '.translateX', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.translateY', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.translateZ', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.rotateX', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.rotateY', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.rotateZ', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.scaleX', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.scaleY', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.scaleZ', k=False,l=lock, channelBox = hide)
    mc.setAttr(dof + '.v', k=False,l=lock, channelBox = hide)
    
    mc.setAttr(groupName + '.translateX', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.translateY', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.translateZ', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.rotateX', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.rotateY', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.rotateZ', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.scaleX', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.scaleY', k=False,l=lock, channelBox = hide)
    mc.setAttr(groupName + '.scaleZ', k=False,l=lock, channelBox = hide)
    #mc.setAttr(groupName + '.v', k=False,l=lock, channelBox = hide)
    
    mc.transformLimits(ctrl, tz = (0,1), etz = (1,0))
    
def SetNonRenderable(obj):
    print obj
    mc.setAttr(obj + ".primaryVisibility", 0)
    mc.setAttr(obj + ".castsShadows", 0)
    mc.setAttr(obj + ".aiVisibleInDiffuseReflection", 0)
    mc.setAttr(obj + ".aiVisibleInSpecularReflection", 0)
    mc.setAttr(obj + ".aiVisibleInDiffuseTransmission", 0)
    mc.setAttr(obj + ".aiVisibleInSpecularTransmission", 0)
    mc.setAttr(obj + ".aiVisibleInVolume", 0)
    mc.setAttr(obj + ".aiSelfShadows", 0)
    mc.setAttr(obj + ".castsShadows", 0)
    mc.setAttr(obj + ".receiveShadows", 0)
    mc.setAttr(obj + ".primaryVisibility", 0)
    mc.setAttr(obj + ".smoothShading", 0)
    mc.setAttr(obj + ".visibleInReflections", 0)
    mc.setAttr(obj + ".visibleInRefractions", 0)
    mc.setAttr(obj + ".doubleSided", 0)
    
def ConnectAttributesToCamera(dof, aperture, camera, ctrl):
    #connects dof translate Z to focus distance
    #mc.connectAttr(camera + ".aiFocusDistance", dof + ".translateZ")
    
    #distance = mc.getAttr(camera + ".aiFocusDistance")
    mc.setAttr(dof + ".translateZ", 0)
    
    
    #create multiplyDivide node and connect camera attr to it
    multiplyDiv = mc.shadingNode('multiplyDivide', asUtility=True, name = '%s_apertureMultiply' % (aperture))
    setRange = mc.shadingNode('setRange', asUtility=True, name = '%s_apertureSetRange' % (aperture))
    reverse = mc.shadingNode('reverse', asUtility=True, name = '%s_reverse' % (aperture))
    
    mc.setAttr(multiplyDiv + ".input2X", 5)
    mc.setAttr(multiplyDiv + ".input2Y", -.2)
    
    mc.setAttr(setRange + ".oldMaxZ ", 1.050)
    
    mc.setAttr(setRange + ".oldMaxX ", 20)
    mc.setAttr(setRange + ".oldMinX ", -20)
    
    mc.setAttr(setRange + ".oldMinY", -1)
    mc.setAttr(setRange + ".oldMaxY", 1.65)
    mc.setAttr(setRange + ".maxY", -.9)
    mc.setAttr(setRange + ".maxY", 1.550)
    
    mc.setAttr(setRange + ".maxZ", 1.0)
    mc.setAttr(setRange + ".maxX", 1)
    mc.setAttr(setRange + ".minX", .1)
    
    mc.connectAttr(ctrl + ".translateZ", camera + ".aiFocusDistance")
    
    
    #mc.connectAttr(camera + ".aiApertureSize", multiplyDiv + ".input1X")
    
    mc.connectAttr(camera + ".aiApertureSize", multiplyDiv + ".input1X")
    mc.connectAttr(camera + ".aiApertureSize", multiplyDiv + ".input1Y")
    
    mc.connectAttr(camera + ".aiApertureSize", setRange + ".valueZ")
    
    mc.connectAttr(multiplyDiv + ".input1X", setRange + ".valueX")
    mc.connectAttr(multiplyDiv + ".input1Y", setRange + ".valueY")    
    
    mc.connectAttr(multiplyDiv + ".outputX", "%s_edge_verts_LOCKED" % (aperture) + ".translateZ")
    
    mc.connectAttr(setRange + ".outValueY", reverse + ".inputX")
    mc.connectAttr(setRange + ".outValueY", reverse + ".inputY")
    
    mc.connectAttr(setRange + ".outValueZ", reverse + ".inputZ")
    
    mc.connectAttr(reverse + ".outputZ  ", aperture + ".scaleX")
    mc.connectAttr(reverse + ".outputZ  ", aperture + ".scaleY")

    mc.addAttr(ctrl, ln = 'Aperture_Size', sn = 'ApertureSize', at = 'float', dv = mc.getAttr(camera + ".aiApertureSize"), min = 0, max = 1, hnv = True, hxv = False, s = True, h = False, k = True)
    mc.connectAttr(ctrl + ".Aperture_Size", camera + ".aiApertureSize")

##########

def StartBuildingCamera():
    selected = mc.ls(sl=True)

    if mc.nodeType(selected) != None:
        camera = selected[0]

        if FindSelectedCamera(camera):
            
            children = mc.listRelatives(camera, c = True)
            
            cont = True
            checkName = "%s_dof_grp" % (camera)
            
            for x in children:
                if x == checkName:
                    mc.warning( "There is already a node named %s. Nothing has been added." % (checkName))
                    cont = False
                    
            if cont == True:
                DofMark = "%s_Cam_DOF_Marker_01" % (str(camera))
                ApertureMark = "%s_Cam_DOF_ApertureMarker" % (str(camera))
                DofCtrl = "%s_DOF_ApertureMarker_Ctrl" % (str(camera))
                #Get selected and make sure it is a camera
                trans = .50
                rgbAperture = [1,0,0]
                rgbDOF = [0,1,1]

                MatApertureName = "MAT_DOF_ApertureMarker"
                MatDofMarker = "MAT_Cam_DOF_Marker_01"

                CreateShaders(MatApertureName, rgbAperture, trans)
                CreateShaders(MatDofMarker, rgbDOF, trans)

                camPos = GetCameraPos(camera)
                camRot = GetCameraRot(camera)

                CreatePlane(DofMark, MatDofMarker, 4, str(camera), camPos, camRot, False)
                LockMesh(DofMark, True)
                
                CreatePlane(ApertureMark, MatApertureName, 1, str(camera), camPos, camRot, True)   
                CreateCtrl(DofCtrl, MatApertureName, 1, str(camera), camPos, camRot, True)
                CreatePolyMove(ApertureMark)
                LockMesh(ApertureMark, True)
                
                CreateHierarchy(DofMark, ApertureMark, camera, DofCtrl)
                ConnectAttributesToCamera(DofMark, ApertureMark, camera, DofCtrl)
                EnableDOF(camera, True)
                
                
                LockNodes(DofMark, ApertureMark, camera, DofCtrl, False, True)

        else:
            print "\r\rcamera is not selected"
    else:
        mc.warning("Nothing is selected")